﻿                                                                                                                                                                                    using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy1
{
    class Program
    {
        //    一、统计下面一段文字中“类”字和“码”的个数。
        //与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。
        //        封装：就是将代码看作一个整体，例如使用类、方法、接口等。
        //              在使用定义好的类、 方法、接口等对象时不必考虑其细节，
        //              只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。
        //        继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。
        //        多态：不仅体现了代码的重用性，也体现了代码的灵活性，
        //              它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。
        //1、使用循环遍历的方法来实现。
        //2、使用Replace方法来实现。
        //3、使用Split()方法来实现。
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，" +
                "即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。" +
                "在使用定义好的类、 方法、接口等对象时不必考虑其细节，" +
                "只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。" +
                "继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。" +
                "多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，" +
                "让类或接口中的成员表现出不同的作用。";
            int sum = 0;
            int num = 0;
            for (int i=0;i<str.Length;i++)
            {
                if (str[i]=='类' )
                {
                    sum++;
                }
                if (str[i] == '码')
                {
                    num++;
                }
            }
            Console.WriteLine("第1种方法");
            Console.WriteLine("类个数："+sum);
            Console.WriteLine("码个数："+num);


            string str2=str.Replace("类","  ");
            string str3 = str.Replace("码", "  ");
            int a = str2.Length - str.Length;
            int b = str3.Length - str.Length;
            Console.WriteLine("第2种方法");
            Console.WriteLine("类个数：" + a);
            Console.WriteLine("码个数：" + b);

            Console.WriteLine("第3种方法");
            string [] str4=str.Split('类');
            int m=str4.Length-1;

            string[] str5 = str.Split('码');
            int n = str5.Length-1;
            Console.WriteLine("类个数：" + m);
            Console.WriteLine("码个数：" + n);
            Console.ReadKey();
        }
    }
}
