﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            string str1 = " 与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C#多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            //使用循环遍历的方法来实现
                int a=0;  int b = 0;
            for (int i = 0; i < str1.Length; i++)
            {
               
                if (str1[i].Equals('类'))
                {
                    a = a + 1;
                }
                if (str1[i].Equals('码'))
                {
                    b = b + 1;
                }
            }
            Console.WriteLine("出现类的个数："+a);
            Console.WriteLine("出现码的个数："+b);
            //使用Replace方法来实现
            string str2 =str1.Replace("类", "");
            string str3 = str1.Replace("码", "");
            Console.WriteLine("码的个数："+(str1.Length-str3.Length));
            Console.WriteLine("类的个数："+(str1.Length-str2.Length));



            //使用Split()方法来实现
            string[] array = str1.Split('类');
            Console.WriteLine("类的字数："+(array.Length-1));
            array = str1.Split('码');
            Console.WriteLine("码的字数："+(array.Length-1));

                 
            Console.ReadKey();



        }
    }
}
