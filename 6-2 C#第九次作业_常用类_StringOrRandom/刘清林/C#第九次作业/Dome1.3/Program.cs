﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1._3
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向" +
                   "对象语言的基本特征，即封装、继承、 多态。封装：就是" +
                   "将代码看作一个整体，例如使用类、方法、接口等。在使用" +
                   "定义好的类、 方法、接口等对象时不必考虑其细节，只需要" +
                   "知道其对象名以及所需要的参数即可，也是一种提升代码安" +
                   "全性的方法。继承：是一种体现代码重用性的特性，减少代码" +
                   "的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码" +
                   "的重用性，也体现了代码的灵活性，它主要通过继承和实现接" +
                   "口的方式，让类或接口中的成员表现出不同的作用。";
            char[] A = new char[] { '类'};
            string[] B = str.Split(A);
            Console.WriteLine("类的个数："+(B.Length-1));
            char[] C = new char[] { '类' };
            string[] D = str.Split(C);
            Console.WriteLine("码的个数：" + (A.Length - 1));
        }
    }
}
