﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1._2
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，" +
                "即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。" +
                "在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要" +
                "的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，" +
                "减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，" +
                "也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";

            string A = str.Replace("类", "");
            string B = str.Replace("码","");
            
            Console.WriteLine("类的个数为：" + (str.Length-A.Length));
            Console.WriteLine("码的个数为：" + (str.Length-B.Length));
            Console.ReadKey();
        }
    }
}
