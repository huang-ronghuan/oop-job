﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arr = new int[10];
            Random r = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                int temp = r.Next(1, 10);
                while (true)
                {
                    if (Array.IndexOf(arr, temp) == -1)
                    {
                        arr[i] = temp;
                        break;
                    }
                    else
                    {
                        temp = r.Next(1, 10);
                    }
                }
                Console.WriteLine(arr[i]);
            }
            Console.ReadKey();
        }
    }
}
