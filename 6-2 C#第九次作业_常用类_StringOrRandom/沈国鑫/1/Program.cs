﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
//    一、统计下面一段文字中“类”字和“码”的个数。

//        与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，
//        即封装、继承、 多态。封装：就是将代码看作一个整体，
//        例如使用类、方法、接口等。在使用定义好的类、 方法、接口
//        等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，
//        也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，
//        减少代码的冗余，但在 C# 语言中仅支持单继承。
//        多态：不仅体现了代码的重用性，也体现了代码的灵活性，
//        它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。

//      1、使用循环遍历的方法来实现。
//      2、使用Replace方法来实现。
//      3、使用Split()方法来实现。
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("使用循环遍历的方法来实现。");
            int a = 0, b = 0;
            string str1 = "与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。";
            for (int i = 0; i < str1.Length; i++)
            {
                if (str1[i]=='类')
                {
                    a++;
                }
                if (str1[i]=='码')
                {
                    b++;
                }
            }
            Console.WriteLine("类的个数为：{0}，码的个数为：{1}",a,b);
            Console.WriteLine();

            Console.WriteLine("使用Replace方法来实现。");
            Console.WriteLine();

            string str2 = str1.Replace("类", "");
            Console.WriteLine("类的个数="+(str1.Length-str2.Length));
            Console.WriteLine();

            string str3 = str1.Replace("码", "");
            Console.WriteLine("码的个数=" + (str1.Length - str3.Length));

            Console.WriteLine();
            Console.WriteLine("使用Split()方法来实现。");
            string[] s1 = str1.Split('类');
            string[] s2 = str1.Split('码');
            Console.WriteLine("类的个数为：{0}，码的个数为：{1}",s1.Length-1,s2.Length-1);

            Console.ReadKey();
        }
    }
}
