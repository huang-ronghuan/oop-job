﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {   
        //1、使用循环遍历的方法来实现。
        //2、使用Replace方法来实现。
        //3、使用Split()方法来实现。


        static void Main(string[] args)
        {
            string line = "与其他面向对象语言一样，" +
                "C# 语言也具有面向对象语言的基本特征，" +
                "即封装、继承、 多态。封装：就是将代码看作一个整体，" +
                "例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，" +
                "只需要知道其对象名以及所需要的参数即可，" +
                "也是一种提升代码安全性的方法。" +
                "继承：是一种体现代码重用性的特性，减少代码的冗余，" +
                "但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，" +
                "也体现了代码的灵活性，它主要通过继承和实现接口的方式，" +
                "让类或接口中的成员表现出不同的作用。";

            for(int i = 0;i < line.Length;i++)
            {
                Console.Write(line[i]);
            }

            Console.WriteLine();


            string line1 = line.Replace("类","");
            Console.WriteLine("类有的字数为：" + (line.Length-line1.Length));
            string line2 = line.Replace("码", "");
            Console.WriteLine("码有的字数为："+(line.Length - line2.Length));


            string line3 = ("与其他面向对象语言一样，C# 语言也具有面向对象语言的基本特征，即封装、继承、 多态。封装：就是将代码看作一个整体，例如使用类、方法、接口等。在使用定义好的类、 方法、接口等对象时不必考虑其细节，只需要知道其对象名以及所需要的参数即可，也是一种提升代码安全性的方法。继承：是一种体现代码重用性的特性，减少代码的冗余，但在 C# 语言中仅支持单继承。多态：不仅体现了代码的重用性，也体现了代码的灵活性，它主要通过继承和实现接口的方式，让类或接口中的成员表现出不同的作用。");
            string[] array = line3.Split(new char[1] { '类' });

            Console.WriteLine("类的个数:" + (array.Length - 1 ));

            string[] array2 = line3.Split(new char[1] { '码' });

            Console.WriteLine("码的个数：" + (array2.Length - 1));
            


            Console.ReadKey();
        }
    }
}
