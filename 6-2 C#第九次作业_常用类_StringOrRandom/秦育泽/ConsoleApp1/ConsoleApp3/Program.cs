﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Random ra = new Random();
            int[] arrNum = new int[10];
            int tmp = 0;
            int minValue = 1;
            int maxValue = 10;
            for (int i = 0; i < 10; i++)
            {
                tmp = ra.Next(minValue, maxValue);
                arrNum[i] = getNum(arrNum, tmp, minValue, maxValue, ra);
            }
            for(int i = 0;i < arrNum.Length;i++)
            {
                Console.WriteLine(arrNum[i]);
            }
            Console.ReadKey();
        }

        public static int getNum(int[] arrNum,int tmp,int minValue,int maxValue,Random ra)
        {
            int n = 0;
            while(n <= arrNum.Length - 1)
            {
                if(arrNum[n] == tmp)
                {
                    tmp = ra.Next(minValue, maxValue);
                    getNum(arrNum, tmp, minValue, maxValue, ra);
                }
                n++;
            }
            return tmp;
        }

       
    }
}
