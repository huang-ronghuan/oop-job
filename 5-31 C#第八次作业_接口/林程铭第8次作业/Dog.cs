﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Dog:Eat1,ISwim,IClimb
    {
        private string swim;

        public string Swim1 { get => swim; set => swim = value; }

        public Dog()
        {
        
        }
        public Dog(string swim ,string eat):base(eat) 
        {
            this.swim = swim;
        }
        public void Swim()
        {
            Console.WriteLine("狗会游泳");
        }
        public void Climb() 
        {
            Console.WriteLine("狗不会爬树");
        }
    }

        
}
