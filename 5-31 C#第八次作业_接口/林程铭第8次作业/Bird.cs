﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Bird:Animal,IFlyable
    {
        private string  LayEggs;
        
        public string LayEggs1 { get => LayEggs; set => LayEggs = value; }

     
        public Bird(string layeggs,string eat):base(eat)
        {
            this.LayEggs = layeggs;
        }
        public void TakeOff() 
        {
            Console.WriteLine("鸟开始飞");
        }
        public void Fly() 
        {
            Console.WriteLine("鸟会飞");
        }
        public void Land() 
        {
            Console.WriteLine("鸟不飞了");
        }
       

    }
}
