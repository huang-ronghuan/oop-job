﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Superman:Animal,IFlyable
    {
        private string kill;

        public string Kill { get => kill; set => kill = value; }

        public Superman(string kill,string eat):base(eat)
        {
            this.kill = kill;
        }
        public void TakeOff() 
        {
            Console.WriteLine("超人起飞");
        }
        public void Fly()
        {
            Console.WriteLine("超人会飞");
        }
        public void Land() 
        {
            Console.WriteLine("超人不飞了");
        }
    }
}
