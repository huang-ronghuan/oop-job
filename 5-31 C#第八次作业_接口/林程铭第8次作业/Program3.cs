﻿using System;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane p = new Plane("100个人","载人飞机");
            p.TakeOff();
            p.Fly();
            p.Land();
            Bird b = new Bird("下蛋", "吃稻谷");
            b.TakeOff();
            b.Fly();
            b.Land();
            Superman s = new Superman("打怪兽","吃饭");
            s.TakeOff();
            s.Fly();
            s.Land();
            Console.ReadKey();


        }
    }
}
