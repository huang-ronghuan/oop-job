﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Monkey:Eat1,ISwim,IClimb
    {
        private string Climb1;

        public string Climb11 { get => Climb1; set => Climb1 = value; }

        public Monkey() 
        {
        
        }

        public Monkey(string Climb1,string eat):base(eat) 
        {
            this.Climb1 = Climb1;
        }

        public void Swim() 
        {
            Console.WriteLine("猴子不会游泳");
        }
        public void Climb() 
        {
            Console.WriteLine("猴子会爬树");
        }
    }
}
