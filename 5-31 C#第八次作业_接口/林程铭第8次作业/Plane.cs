﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Plane:Vehicle,IFlyable
    {
       
        private string CarryPassange;


        public string CarryPassange1 { get => CarryPassange; set => CarryPassange = value; }


        public Plane(string carrypassange,string vehicle):base(vehicle)
        {
           
            this.CarryPassange = carrypassange;
        }
        public void TakeOff()
        {
            Console.WriteLine("飞机起飞");
        }
        public void Fly() 
        {
            Console.WriteLine("飞机会飞");
        }
        public void Land() 
        {
            Console.WriteLine("飞机降落");
        }

    }
}
