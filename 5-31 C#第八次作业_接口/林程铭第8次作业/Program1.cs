﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Cat cat = new Cat();
            cat.Swim();
            cat.Climb();
            Dog dog = new Dog();
            dog.Swim();
            dog.Climb();
            Monkey monkey = new Monkey();
            monkey.Swim();
            monkey.Climb();
            Duck duck = new Duck();
            duck.Swim();
            duck.Climb();
            Console.ReadKey();


        }
    }
}
