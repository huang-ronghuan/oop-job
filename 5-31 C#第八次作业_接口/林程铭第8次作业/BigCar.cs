﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class BigCar
    {
        private string Brand;
        private string Run;

        public string Brand1 { get => Brand; set => Brand = value; }
        public string Run1 { get => Run; set => Run = value; }

        public BigCar()
        {
        
        }

        public BigCar(string brand, string run)
        {
            this.Brand = brand;
            this.Run = run;
        }
    }
}
