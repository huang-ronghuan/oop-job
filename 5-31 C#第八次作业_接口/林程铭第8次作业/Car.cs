﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Car:BigCar,IFly
    {
        private string Brand;
        private string Run;

        public Car() 
        {
        
        }
        public Car(string brand,string run):base(brand,run) 
        {
            this.Brand =brand;
            this.Run = run;
        }
        public void Fly() 
        {
            Console.WriteLine("该车为蝙蝠车,是一辆会飞的车,时速有100km/h");
        }

    }
}
