﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            cat cat = new cat();
            cat.climb();

            dog dog = new dog();
            dog.swim();

            duck duck = new duck();
            duck.swim();

            monkey monkey = new monkey();
            monkey.climb();

            Console.ReadKey();
        }
    }
}
