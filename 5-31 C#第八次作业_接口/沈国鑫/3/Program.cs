﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            bird bird = new bird();
            bird.Eat();
            bird.TakeOff();
            bird.Fly();
            bird.Land();
            bird.LayEggs();
            Console.WriteLine();
            plane plane = new plane();
            plane.TakeOff();
            plane.Fly();
            plane.Land();
            Console.WriteLine();
            Superman superman = new Superman();
            superman.Eat();
            superman.TakeOff();
            superman.Fly();
            superman.Land();

            Console.ReadKey();
        }
    }
}
