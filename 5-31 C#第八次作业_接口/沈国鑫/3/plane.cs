﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class plane:IFlyable
    {
        public void TakeOff()
        {
            Console.WriteLine("飞机起飞");
        }
        public void Fly()
        {
            Console.WriteLine("飞行中");
        }
        public void Land()
        {
            Console.WriteLine("着陆");
        }
    }
}
