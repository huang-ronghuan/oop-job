﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1、猫、狗、鸭、猴，（吃、游泳、爬树）
            //所有动物都有吃的方法
            //狗和鸭会游泳，不会爬树
            //猫和猴不会游泳会爬树
            //将吃的方法定义在父类方法中，将游泳和爬树的技能定义为接口
            //所有子类继承父类后，再去继承相应的接口实现技能
            Cat cat = new Cat("小猫","吃");
            cat.Print();
            Monkey monkey = new Monkey("小猴", "吃");
            monkey.Print();
            Dark dark = new Dark("小鸭", "吃");
            dark.Print();
            Dog dog = new Dog("小狗", "吃");
            dog.Print();


            Console.ReadKey();
        }
    }
}
