﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //            1.飞机是交通工具类，有运输载人的功能；
            //2.小鸟和超人都是动物类，都有吃的方法；小鸟有自己的特有方法，下蛋；
            //3.超人、小鸟、飞机都有飞的功能，可以定义飞的接口;
            //            飞的接口有起飞、飞行中、着陆的方法；
            //4.超人、小鸟、飞机除了继承各自的父类后，还要继承飞的接口，实现飞的接口的方法；


            Plane plane = new Plane("飞机");
            plane.TakeOff();
            plane.Fly();
            plane.Land();
            plane.CarryPassange();
            Bird bird = new Bird("小鸟");
            bird.TakeOff();
            bird.Fly();
            bird.Land();
            bird.LayEggs();
            bird.Eat();
            Superman superman = new Superman("超人");
            superman.Eat();
            superman.TakeOff();
            superman.Fly();
            superman.Land();



            Console.ReadKey();
        }
    }
}
