﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Bird : Animal, IFlyable
    {
        private string name;
        public string Name { get => name; set => name = value; }
        public Bird()
        {

        }
        public Bird(string name)
        {
            this.name = name;
        }
        public new void Eat()
        {
            Console.WriteLine("{0}要吃东西", this.name);
        }
        public void LayEggs()
        {
            Console.WriteLine("{0}会下蛋", this.name);
        }
        public void TakeOff()
        {
            Console.WriteLine("{0}起飞要动翅膀", this.name);
        }

        public void Fly()
        {
            Console.WriteLine("{0}在飞行中要不停煽动翅膀", this.name);
        }

        public void Land()
        {
            Console.WriteLine("{0}着陆时要慢慢停止翅膀", this.name);
        }
    }
}
