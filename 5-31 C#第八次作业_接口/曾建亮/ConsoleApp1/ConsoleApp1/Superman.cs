﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Superman : Animal, IFlyable
    {
        private string name;
        public string Name { get => name; set => name = value; }
        public Superman()
        {

        }
        public Superman(string name)
        {
            this.name = name;
        }
        public void Eat()
        {
            Console.WriteLine("{0}需要吃东西", this.name);
        }
        public void TakeOff()
        {
            Console.WriteLine("{0}起飞", this.name);
        }

        public void Fly()
        {
            Console.WriteLine("{0}在飞", this.name);
        }

        public void Land()
        {
            Console.WriteLine("{0}停止", this.name);
        }
    }
}
