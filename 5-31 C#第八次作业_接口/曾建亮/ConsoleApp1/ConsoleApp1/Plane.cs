﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Plane : Vechicle, IFlyable
    {
        private string name;
        public string Name { get => name; set => name = value; }
        public Plane()
        {

        }
        public Plane(string name)
        {
            this.Name = name;
        }
        public new void CarryPassange()
        {
            Console.WriteLine("{0}可以载人", this.name);
        }
        public void TakeOff()
        {
            Console.WriteLine("{0}收起滚轮", this.name);
        }

        public void Fly()
        {
            Console.WriteLine("{0}飞行中消耗油量", this.name);
        }

        public void Land()
        {
            Console.WriteLine("{0}着陆时放下滚轮,放慢速度", this.name);
        }
    }
}
