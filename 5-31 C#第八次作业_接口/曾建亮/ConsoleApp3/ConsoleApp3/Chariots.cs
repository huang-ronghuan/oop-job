﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Chariots:Common,IFly
    {
        private string name;
        public string Name { get => name; set => name = value; }
        public Chariots()
        {

        }
        public Chariots(string name, string Brand) : base(Brand)
        {
            this.name = name;
            this.Brand1 = Brand;
        }
        public void Fly()
        {
             Console.WriteLine("{0}也可以在天上飞", this.name);
        }
        public new void Run()
        {
            Console.WriteLine("{0}可以在地上跑", this.name);
        }
    }
}
