﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Common
    {
        //定义一个父类表示车的共同属性特征行为：品牌名Brand，会跑Run方法
        private string Brand;
        public string Brand1 { get => Brand; set => Brand = value; }
        public Common()
        {
            
        }
        public Common(string Brand, string Run)
        {
            this.Brand = Brand;
            
        }

        public Common(string brand)
        {
            Brand = brand;
        }

        public void Run() { }
    }
}
