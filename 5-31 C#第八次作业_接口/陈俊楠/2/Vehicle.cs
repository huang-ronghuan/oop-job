﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    abstract class Vehicle
    {
        private string brand;

        public string Brand { get => brand; set => brand = value; }

        public Vehicle(string brand) {
            this.brand = brand;
        }

        public abstract void run();
    }
}
