﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Plane : Vehicle, IFlyable
    {
        public Plane(string name) : base(name) { }
        public void fly()
        {
            Console.WriteLine(Name + "飞机飞行中");
        }

        public void land()
        {
           
            Console.WriteLine(Name + "飞机着路了");
        }

        public void takeoff()
        {
            Console.WriteLine(Name + "飞机起飞了");
        }

        public void carryPassange() {
            Console.WriteLine(Name + "飞机带着一顿大麻");
        }
    }
}
