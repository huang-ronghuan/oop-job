﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
   abstract class Animal
    {
        private string name;

        public Animal(string name) {
            this.name = name;
        }

        public abstract void Eat();

        public string Name { get => name; set => name = value; }
    }
}
