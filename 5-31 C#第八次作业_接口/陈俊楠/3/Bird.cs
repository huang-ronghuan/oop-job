﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Bird : Animal, IFlyable
    {
        public Bird(string name) : base(name) { }
        public override void Eat()
        {
            Console.WriteLine(Name + "小鸟吃虫子");
        }

        public void fly()
        {
            Console.WriteLine(Name + "小鸟飞行中");
        }

        public void land()
        {
            Console.WriteLine(Name + "小鸟着路了");
        }

        public void takeoff()
        {
            Console.WriteLine(Name + "小鸟起飞了");
        }

        public void layEggs() {
            Console.WriteLine(Name + "小鸟的小秘密");
        }
    }
}
