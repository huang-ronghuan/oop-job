﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Vehicle
    {
        private string name;

        public Vehicle(string name) {
            this.name = name;
        }

        public string Name { get => name; set => name = value; }
    }
}
