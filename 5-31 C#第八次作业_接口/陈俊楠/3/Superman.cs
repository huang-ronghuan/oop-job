﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Superman : Animal, IFlyable
    {
        public Superman(string name) : base(name) { }
        public override void Eat()
        {
            Console.WriteLine(Name + "超人喜欢吃西瓜");
        }

        public void fly()
        {
            Console.WriteLine(Name + "超人飞行中");
        }

        public void land()
        {
            Console.WriteLine(Name + "超人着路了");
        }

        public void takeoff()
        {
            Console.WriteLine(Name + "超人起飞了");
        }

        
    }
}
