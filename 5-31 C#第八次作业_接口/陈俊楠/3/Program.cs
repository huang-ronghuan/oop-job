﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            Plane p1 = new Plane("打飞机");
            p1.takeoff();
            p1.fly();
            p1.land();
            p1.carryPassange();
            Bird b1 = new Bird("小鸟");
            b1.takeoff();
            b1.fly();
            b1.land();
            b1.layEggs();
            b1.Eat();
            Superman s1 = new Superman("萝卜");
            s1.Eat();
            s1.takeoff();
            s1.fly();
            s1.land();
            Console.ReadKey();
        }
    }
}
