﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class cat : Animal, Iclimb
    {
        public cat(string name) : base(name) { }
        public void climb()
        {
            Console.WriteLine(Name+"特别的会上树");
        }

        public override void Eat()
        {
            Console.WriteLine(Name + "喜欢吃鱼");
        }
    }
}
