﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            cat c1 = new cat("猫猫");
            c1.Eat();
            c1.climb();
            dog d1 = new dog("狗狗");
            d1.Eat();
            d1.swim();
            duck d2 = new duck("嘎嘎");
            d2.Eat();
            d2.swim();
            monkey m1 = new monkey("吉吉");
            m1.Eat();
            m1.climb();
            Console.ReadKey();
        }
    }
}
