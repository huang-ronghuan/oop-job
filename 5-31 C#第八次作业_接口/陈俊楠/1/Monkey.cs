﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class monkey : Animal, Iclimb
    {
        public monkey(string name) : base(name) { }
        public void climb()
        {
            Console.WriteLine(Name + "上树和回家一样");
        }

        public override void Eat()
        {
            Console.WriteLine(Name + "最爱吃香蕉");
        }
    }
}
