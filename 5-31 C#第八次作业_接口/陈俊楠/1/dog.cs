﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class dog : Animal, Iswim
    {
        public dog(string name) : base(name) { }
        public override void Eat()
        {
            Console.WriteLine(Name+"最喜欢吃大便");
        }

        public void swim()
        {
            Console.WriteLine(Name + "最擅长狗刨");
        }
    }
}
