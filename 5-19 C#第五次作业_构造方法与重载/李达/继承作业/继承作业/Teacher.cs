﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 继承作业
{
    class Teacher:Person
    {

        public string title;
        public int wageno;

        public new void Print()
        {
            base.Print();

            Console.WriteLine("职称：" + title);
            Console.WriteLine("工资号：" + wageno);
        }
        public Teacher()
        {

        }

        public Teacher (int ID, string Name, string Sex, string CardID, string TEL, string Title,int Wageno) : base(ID, Name, Sex, CardID, TEL)
        {
            this.title = Title;
            this.wageno = Wageno;
        }
    }
}
