﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 继承作业
{
    class Person
    {
        public int id;
        public string name;
        public string sex;
        public string cardid;
        public string tel;

        public void Print()
        {
            Console.WriteLine("编号：" + id);
            Console.WriteLine("姓名：" + name);
            Console.WriteLine("性别：" + sex);
            Console.WriteLine("身份证号：" + cardid);
            Console.WriteLine("联系方式：" + tel);
        }

        public Person()
        {

        }

        public Person(int ID,string Name,string Sex,string CardID,string TEL)
        {
            this.id = ID;
            this.name = Name;
            this.sex = Sex;
            this.cardid = CardID;
            this.tel = TEL;
        }
    }
}
