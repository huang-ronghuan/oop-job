﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 继承作业
{
    class Student:Person
    {
        public string major;
        public int grade;

        public new void Print()
        {
            base.Print();

            Console.WriteLine("专业：" + major);
            Console.WriteLine("年级：" + grade);
        }
        public Student()
        {

        }

        public Student(int ID, string Name, string Sex, string CardID, string TEL,string Major, int Grade) :base(ID,Name,Sex,CardID,TEL)
        {
            this.major = Major;
            this.grade = Grade;
        }
    }
}
