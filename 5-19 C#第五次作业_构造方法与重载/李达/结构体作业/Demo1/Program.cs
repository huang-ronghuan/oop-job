﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    struct PersonStruct
    {
        public string name;
        public string tel;
        public string address;
        public void p()
        {
            Console.WriteLine("姓名" + name);
            Console.WriteLine("电话" + tel);
            Console.WriteLine("地址" + address);
        }
    }
    class PersonClass
    {
        public string name;
        public string tel;
        public string address;
        public void p()
        {
            Console.WriteLine("姓名" + name);
            Console.WriteLine("电话" + tel);
            Console.WriteLine("地址" + address);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            PersonStruct p1;
            p1.name = "张三";
            p1.tel = "12345678910";
            p1.address = "福建龙岩";

            PersonStruct p2;
            p2 = p1;
            p2.name = "李四";
            p1.p();

            PersonClass p3 = new PersonClass();
            p3.name = "张三";
            p3.tel = "12345678910";
            p3.address = "福建龙岩";

            PersonClass p4 = new PersonClass();
            p4 = p3;
            p4.name = "李四";
            p3.p();
            Console.ReadKey();

            //结构体是值类型，类是引用类型。p1传到p2的是值，p2改变的是自身的值，不影响p1；p3传到p4的是引用的空间，p4改变了引用的这个空间，从而影响p3
        }
    }
}
