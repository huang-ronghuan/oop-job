﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ref和out作业
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[5];
            for (int i = 0; i < num.Length; i++)
            {
                Console.WriteLine("请输入第{0}个数字",i+1);
                num[i] = int.Parse(Console.ReadLine());
            }
            int max=num[0], min=num[0], sum, avg=0;

            sum=SumUtil(ref max,ref min,ref avg,num);

            Console.WriteLine("最大值=" + max);
            Console.WriteLine("最小值=" + min);
            Console.WriteLine("和=" + sum);
            Console.WriteLine("平均数=" + avg);

            Console.ReadKey();
        }

        public static int SumUtil(ref int max,ref int min,ref int avg,int [] num)
        {
            int sum=0;
            for (int i = 0; i < num.Length; i++)
            {
                if (max < num[i])
                {
                    max = num[i];
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                if (min > num[i])
                {
                    min = num[i];
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                sum += num[i];
            }
            avg = sum / num.Length;
            return sum;
        }
    }
}
