﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace @out
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[5];
            for (int i = 0; i < num.Length; i++)
            {
                Console.WriteLine("请输入第{0}个数字", i + 1);
                num[i] = int.Parse(Console.ReadLine());
            }
            int max, min, sum, avg;

            sum = SumUtil(out max, out min, out avg, num);

            Console.WriteLine("最大值=" + max);
            Console.WriteLine("最小值=" + min);
            Console.WriteLine("和=" + sum);
            Console.WriteLine("平均数=" + avg);

            Console.ReadKey();
        }

        public static int SumUtil(out int max, out int min, out int avg, int[] num)
        {
            int sum = 0;
            max = num[0];
            min = num[0];
            for (int i = 0; i < num.Length; i++)
            {
                if (max < num[i])
                {
                    max = num[i];
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                if (min > num[i])
                {
                    min = num[i];
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                sum += num[i];
            }
            avg = sum / num.Length;
            return sum;
        }
    }
    //ref在引用前一定要赋值，out可以不赋值直接引用，但在引用完成之前一定要给其赋值
}
