﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Worker
    {
        public int ID;//工号
        public string Name;//姓名
        public string Sex;//性别
        public string Education;//学历
        public string Message;//部门信息

        public Worker()
        {
            //无参构造函数
            this.Education = "专科";
        }

        public Worker(int id,string name,string sex,string education,string message)
        {
            //有参构造函数
            this.ID = id;
            this.Name = name;
            this.Sex = sex;
            this.Education = education;
            this.Message = message;
        }

        public void print()
        {
            Console.WriteLine("工号：" + ID);
            Console.WriteLine("姓名：" + Name);
            Console.WriteLine("性别：" + Sex);
            Console.WriteLine("学历：" + Education);
            Console.WriteLine("部门信息：" + Message);
            Console.ReadKey();
        }
    }
}
