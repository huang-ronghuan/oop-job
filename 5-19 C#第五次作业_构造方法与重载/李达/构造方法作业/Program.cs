﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    //构造方法作业
    class Program
    {
        static void Main(string[] args)
        {
            Worker worker = new Worker();
            worker.print();
            Worker worker1 = new Worker(1,"张三","男","本科","研发部");
            worker1.print();
        }
    }
}
