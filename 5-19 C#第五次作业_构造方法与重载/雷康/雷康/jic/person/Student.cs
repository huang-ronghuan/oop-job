﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace person
{
    class Student
    {
        private string major;
        private string grade;

        public string Major { get => major; set => major = value; }
        public string Grade { get => grade; set => grade = value; }

        public Student(string major,string grade)
        {
            this.major = major;
            this.grade = grade;
        }
        public void print()
        {
            Console.WriteLine("major:"+major);
            Console.WriteLine("grade:"+grade);
        }
    }
}
