﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace person
{
    class Person
    {
        private int id;
        private string name;
        private string sex;
        private int number;
        private int phonenumber;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Sex { get => sex; set => sex = value; }
        public int Number { get => number; set => number = value; }
        public int Phonenumber { get => phonenumber; set => phonenumber = value; }

        public Person(int id,string name,string sex,int number,int phonenumber)
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.number = number;
            this.phonenumber = phonenumber;
        }
        public void print()
        {
            Console.WriteLine("id:"+id);
            Console.WriteLine("name:" +name);
            Console.WriteLine("sex:" +sex);
            Console.WriteLine("number:" +number);
            Console.WriteLine("phonenumber:" +phonenumber);
        }
    }
}
