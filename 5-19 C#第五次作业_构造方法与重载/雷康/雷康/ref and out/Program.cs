﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ref_and_out
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("a请输入");
            int[] a = new int[5];
            for (int j = 0; j<5; j++)
            {
               a[j] = int.Parse(Console.ReadLine());
            }
            int max;
            int min;
            int avg;
            int sum;
            Math(out max,out min,out avg,out sum,a);

            Console.WriteLine("max:"+max);
            Console.WriteLine("min:"+min);
            Console.WriteLine("sum:"+sum);
            Console.WriteLine("avg:"+avg);

            Console.WriteLine("b请输入");
            int[] b = new int[5];
            for (int j = 0; j < 5; j++)
            {
                b[j] = int.Parse(Console.ReadLine());
            }
            int max1=0;
            int min1=0;
            int avg1=0;
            int sum1=0;
            Math1(ref max1, ref min1, ref avg1, ref sum1, b);
            Console.WriteLine("max1:" + max1);
            Console.WriteLine("min1:" + min1);
            Console.WriteLine("sum1:" + sum1);
            Console.WriteLine("avg1:" + avg1);
            Console.ReadKey();

        } 
        public static void Math1(ref int max1, ref int min1, ref int avg1, ref int sum1, int[] b)
        {

            max1 = b[0];
            for (int i = 1; i < 5; i++)
            {
                if (max1 < b[i])
                {
                    max1 = b[i];
                }
            }
            min1 = b[0];
            for (int i = 1; i < 5; i++)
            {
                if (min1 > b[i])
                {
                    min1 = b[i];
                }
            }

            sum1 = 0;
            for (int i = 0; i < 5; i++)
            {
                sum1 = sum1 + b[0];
            }

            avg1 = sum1 / b.Length;
            
        }
        public static void Math(out int max, out int min, out int avg, out int sum,int []a) {
            max= a[0];
            for (int i = 1; i < 5; i++)
            {
                if (max < a[i]) 
                {
                    max = a[i];
                }
            }
            min = a[0];
            for (int i = 1; i < 5; i++)
            {
                if (min > a[i])
                {
                    min = a[i];
                }
            }

            sum = 0;
            for (int i = 0; i < 5; i++)
            {
                sum = sum + a[0];
            }

            avg = sum / a.Length;


        }


    }
}
