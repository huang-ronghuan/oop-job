﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace account
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 a = new Class1();
            Class1 b = new Class1(2000);
            Console.WriteLine("初始金额：{0}", a.Balance);
            bool s = true;
            while (s)
            {
                Console.WriteLine("输入操作的步骤1.存钱，2.取钱，3.查询余额,4.退出");
                int num = int.Parse(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Console.WriteLine("输入你要存入的金额");
                        int money = int.Parse(Console.ReadLine());
                        a.sum(money);
                        Console.WriteLine("当前金额：{0}", a.Balance);
                        break;
                    case 2:
                        Console.WriteLine("输入你要取走的金额");
                        int money1 = int.Parse(Console.ReadLine());
                        a.delete(money1);
                        break;
                    case 3:
                        a.select();
                        break;
                    case 4:
                        s = false;
                        break;
                    default:
                        break;
                }
            }

            Console.ReadKey();
        }
    }
}
