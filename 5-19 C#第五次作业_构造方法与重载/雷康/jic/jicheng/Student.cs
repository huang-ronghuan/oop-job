﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jicheng
{
    class Student
    {
        private int id;
        private string name;
        private string sex;
        private int number;
        private int phonenumber;
        private string major;
        private string grade;

        public int Id { get => id; set => id = value; }
        public string Name { get => name; set => name = value; }
        public string Sex { get => sex; set => sex = value; }
        public int Number { get => number; set => number = value; }
        public int Phonenumber { get => phonenumber; set => phonenumber = value; }
        public string Major { get => major; set => major = value; }
        public string Grade { get => grade; set => grade = value; }

        public void print()
        {
            Console.WriteLine("id:"+id);
            Console.WriteLine("naem:"+name);
            Console.WriteLine("sex:" +sex);
            Console.WriteLine("number:" +number);
            Console.WriteLine("phonenumber:" +phonenumber);
            Console.WriteLine("major:" +major);
            Console.WriteLine("grade:" +grade);
        }
    }
}
