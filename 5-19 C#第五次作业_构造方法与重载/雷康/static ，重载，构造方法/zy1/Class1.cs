﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy1
{
    //定义一个员工类，存放用户的工号、姓名、性别、学历和部门信息；
//定义两个构造函数:
//一个是无参构造函数，学历默认为专科；
//一个有参构造函数，根据参数对类的属性进行初始化。
    class Class1
    {
        public string number;
        public string name;
        public string sex;
        public string bumen;

        public Class1() { }

        public Class1(string number, string name,
            string sex, string bumen)
        {
            this.number = number;
            this.name = name;
            this.sex = sex;
            this.bumen = bumen;
        }

        public static string xli;
        public static void printxli() {
            Console.WriteLine("学历："+xli);
        }

        public void print() {
            Console.WriteLine("工号："+number);
            Console.WriteLine("名字：" +name);
            Console.WriteLine("性别：" +sex);
            Console.WriteLine("学历：" +xli);
            Console.WriteLine("部门：" +bumen);
        }
    }
}
