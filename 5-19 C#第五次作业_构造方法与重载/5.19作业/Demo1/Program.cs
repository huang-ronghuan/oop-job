﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        private static object name;

        static void Main(string[] args)
        {
            Class1 cc = new Class1();  //实例化对象
            cc.id = "1";
            cc.name = "唐某";
            cc.sex = "女";
            cc.department = "宣传部";
          

            //有参的构造方法对成员进行初始化
            Class1 xx = new Class1("01","杨某","女","生劳部");

            Class1.study = "专科";
            Class1.Student();
            Console.WriteLine("cc数据:");
            cc.Class();
            Console.WriteLine("xx数据:");
            xx.Class();

            Console.ReadKey();
         
        }
    }
}
