﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Class1
    {
        public string id;   //工号
        public string name;  //姓名
        public string sex;//性别
        public string department; //部门

        public static string study;  ////static关键字定义静态成员,静态成员属于整个类的,定义学历


        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        public void Class()
        {
            Console.WriteLine("工号" + id);
            Console.WriteLine("姓名" + name);
            Console.WriteLine("性别" + sex);
            Console.WriteLine("部门" + department);
            Console.WriteLine("学历" + study);
        }

        public static void Student()
        {
            Console.WriteLine("学历" + study);  //静态方法引用静态成员
        }

        //一个有参构造函数，根据参数对类的属性进行初始化
        public Class1(string id, string name, string sex, string department)
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.department = department;
        }
        public Class1()
        {

        }
    }
}

