﻿using System;

namespace Demo9
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[] { 3, 4, 5, 6, 7 };
            int max;
            int min;
            int avg;
            int sum;
            Math(out max, out min, out avg, out sum);
            Console.WriteLine("max:"+max.ToString());
            Console.WriteLine("min:"+min.ToString());
            Console.WriteLine("avg:"+avg.ToString());
            Console.WriteLine("sum:"+sum.ToString());
            Console.ReadKey();
        }

         private static void Math(out int max, out int min, out int avg,out int sum)
        {
            max = 7;
            min = 3;
            avg = (3 + 4 + 5 + 6 + 7) / 5;
            sum = 3 + 4 + 5 + 6 + 7;
        }
    }
}
