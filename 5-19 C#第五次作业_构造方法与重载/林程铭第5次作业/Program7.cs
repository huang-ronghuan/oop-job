﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Hero1
    {
        public string choosehero;
        public string yourname;
        public string introduce;
        public string skills;
        public string ATK;
        public string defensive;
        public string speed;

        public void hero()
        {
            Console.WriteLine("(请按回车键，预览人物属性)\n");
            Console.WriteLine("输入名字：{0}", yourname);
            yourname = Console.ReadLine();
        }
        public void hero1()
        {
            Console.WriteLine("人物：{0}\n 攻击力：{1}", this.choosehero, this.ATK);

        }
        public void hero2()
        {
            Console.WriteLine("人物：{0}\n 防御力：{1}", this.choosehero, this.defensive);

        }
        public void hero3()
        {
            Console.WriteLine("人物：{0}\n 速度：{1}", this.choosehero, this.speed);

        }



    }


    class Hero2
    {
        public string choosehero;
        public string yourname;
        public string introduce;
        public string skills;
        public string ATK;
        public string defensive;
        public string speed;

        public void hero()
        {
            Console.WriteLine("\n(请按回车键，预览人物属性)\n");
            Console.WriteLine("输入名字：{0}", yourname);
            yourname = Console.ReadLine();
        }
        public void hero1()
        {
            Console.WriteLine("人物：{0}\n 攻击力：{1}", this.choosehero, this.ATK);

        }
        public void hero2()
        {
            Console.WriteLine("人物：{0}\n 防御力：{1}", this.choosehero, this.defensive);

        }
        public void hero3()
        {
            Console.WriteLine("人物：{0}\n 速度：{1}", this.choosehero, this.speed);

        }



    }


    class Hero3
    {
        public string choosehero;
        public string yourname;
        public string introduce;
        public string skills;
        public string ATK;
        public string defensive;
        public string speed;

        public void hero()
        {
            Console.WriteLine("\n(请按回车键，预览人物属性)\n");
            Console.WriteLine("输入名字：{0}", yourname);
            yourname = Console.ReadLine();
        }
        public void hero1()
        {
            Console.WriteLine("人物：{0}\n 攻击力：{1}", this.choosehero, this.ATK);

        }
        public void hero2()
        {
            Console.WriteLine("人物：{0}\n 防御力：{1}", this.choosehero, this.defensive);

        }
        public void hero3()
        {
            Console.WriteLine("人物：{0}\n 速度：{1}", this.choosehero, this.speed);

        }



    }


    class Hero4
    {
        public string choosehero;
        public string yourname;
        public string introduce;
        public string skills;
        public string ATK;
        public string defensive;
        public string speed;

        public void hero()
        {
            Console.WriteLine("\n(请按回车键，预览人物属性) \n");
            Console.WriteLine("输入名字：{0}", yourname);
            yourname = Console.ReadLine();
        }
        public void hero1()
        {
            Console.WriteLine("人物：{0}\n 攻击力：{1}", this.choosehero, this.ATK);

        }
        public void hero2()
        {
            Console.WriteLine("人物：{0}\n 防御力：{1}", this.choosehero, this.defensive);

        }
        public void hero3()
        {
            Console.WriteLine("人物：{0}\n 速度：{1}\n", this.choosehero, this.speed);

        }



    }
    class Program
    {
        static void Main(string[] args)
        {
            Hero1 h = new Hero1();
            h.choosehero = "埃洛克";
            h.yourname = "晓";
            h.ATK = "80%";
            h.defensive = "50%";
            h.speed = "20%";
            h.hero();
            h.hero1();
            h.hero2();
            h.hero3();

            Hero2 h1 = new Hero2();
            h1.choosehero = "泰拉";
            h1.yourname = "破";
            h1.ATK = "80%";
            h1.defensive = "60%";
            h1.speed = "50%";
            h1.hero();
            h1.hero1();
            h1.hero2();
            h1.hero3();

            Hero3 h2 = new Hero3();
            h2.choosehero = "卢卡斯";
            h2.yourname = "清";
            h2.ATK = "80%";
            h2.defensive = "50%";
            h2.speed = "55%";
            h2.hero();
            h2.hero1();
            h2.hero2();
            h2.hero3();

            Hero4 h3 = new Hero4();
            h3.choosehero = "洛菲";
            h3.yourname = "空";
            h3.ATK = "75%";
            h3.defensive = "30%";
            h3.speed = "80%";
            h3.hero();
            h3.hero1();
            h3.hero2();
            h3.hero3();



            Console.WriteLine("英雄选择：1.埃洛克 2.泰拉 3.卢卡斯 4.洛菲");
            int choosehero = int.Parse(Console.ReadLine());
            switch (choosehero)
            {
                case 1:
                    Console.WriteLine("人物：埃洛克 \n");
                    Console.WriteLine("角色技能：碎石打击 \n 烈焰锚钩 \n 战斗咆哮 \n");
                    Console.WriteLine("埃洛克是一名来自末日边境的勇士。\n 他是圣约英雄中摩纳哥夫妻是的拳术好手。\n 他用毁灭性的符文魔法和无情的拳术攻击\n消灭敌人。");
                    Console.WriteLine("请继续输入数字，预览您想要的角色：");
                    int a = int.Parse(Console.ReadLine());
                    break;
                case 2:
                    Console.WriteLine("人物：泰拉\n");
                    Console.WriteLine("角色技能：\n巨浪冲击 \n 元素突击 \n 复仇杀戮");
                    Console.WriteLine("泰拉是为复仇而来的勇者。\n她挥舞法杖，将愤怒转化为强大的\n元素魔法和攻击力，因此战无不胜。");
                    break;
                case 3:
                    Console.WriteLine("人物：卢卡斯\n");
                    Console.WriteLine("角色技能：\n减速陷阱 \n 能量浪潮 \n 旋风剑舞");
                    Console.WriteLine("卢卡斯是一名彬彬有礼的剑客，\n能控制源质能量。他一手持剑战斗，\n另一手辅助攻击。");
                    break;
                case 4:
                    Console.WriteLine("人物：洛菲\n");
                    Console.WriteLine("角色技能：\n能量精灵 \n 暗影传送 \n 时空迸裂");
                    Console.WriteLine("洛菲是一名攻击迅猛且擅长\n传送魔法的时空旅行者，\n喜欢利用她的幻想伙伴迷惑、\n吸引并摧毁敌人。");
                    break;
                default:
                    break;
            }
            Console.ReadKey();




        }
    }
}

