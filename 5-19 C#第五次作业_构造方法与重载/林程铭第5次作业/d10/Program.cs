﻿using System;

namespace Demo10
{

    class Program
    {
        static void Main(string[] args)
        {
            Student stu = new Student(001, "小林", "男", "393833833", "1837383993", "软件开发", "大一");
            stu.Study1();
            Teacher tea = new Teacher(002, "小优", "男", "230948309", "10932821", "教授", "23904");
            tea.Study2();
            Person per = new Person(003, "晓", "男", "2349384","2122323232");
            per.Study3();
            Console.ReadKey();
        }
    }
}
