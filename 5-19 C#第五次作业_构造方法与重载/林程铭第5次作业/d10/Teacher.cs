﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo10
{
    class Teacher:Person
    {
        private string title;
        public string Tille
        {
            get {
                return this.title;
            }
            set {
                this.title = value;
            }
        }
        private string wageno;
        public string Wageno
        {
            get {
                return this.wageno;
            }
            set {
                this.wageno = value;
            }
        }
        public Teacher() 
        {
        
        }
        public Teacher(int id, string name, string sex, string cardid, string tel, string title, string wageno):base(id,name,sex,cardid,tel)
        {
            //this.id = id;
            //this.name = name;
            //this.sex = sex;
            //this.cardid = cardid;
            //this.tel = tel;
            this.title = title;
            this.wageno = wageno;
        }

        public void Study2()
        {    
            base.Study3();
            Console.WriteLine("\n教师信息：\n{0}\n{1}",this.title,this.wageno);
            
        }
    

    }
}
