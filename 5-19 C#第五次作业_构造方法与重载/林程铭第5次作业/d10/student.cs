﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo10
{
    class Student : Person
    {
        private string major;
        public string Major {
            get
            {
                return this.major;
            }
            set {
                this.major = value;
            }
        }
        private string grade;
        public string Grade {
            get {
                return this.grade;
            }
            set {
                this.grade = value;
            }
        }
        public Student()
        {

        }
        public Student(int id, string name, string sex, string cardid, string tel, string major, string grade):base(id,name,sex,cardid,tel)
        {
            this.major = major;
            this.grade = grade;
        }
        public void Study1()
         {
            base.Study3();
            Console.WriteLine("\n信息：\n{0}\n{1}", this.major, this.grade);
         }
    }
}
