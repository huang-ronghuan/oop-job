﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo10
{
    class Person
    {
        private int id;
        public int Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
            }
        }
        private string name;
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        private string sex;
        public string Sex
        {
            get
            {
                return this.name;
            }
            set
            {
                this.sex = value;
            }
        }
        private string cardid;
        public string Cardid
        {
            get
            {
                return this.cardid;
            }
            set
            {
                this.cardid = value;
            }
        }
        private string tel;
        public string Tel
        {
            get
            {
                return this.tel;
            }
            set
            {
                this.tel = value;
            }
        }
        public Person() 
        {
        
        }
        public Person(int id, string name, string sex, string cardid, string tel)
        {
            this.id = id;
            this.name = name;
            this.sex = sex;
            this.cardid = cardid;
            this.tel = tel;
        }

        public void Study3() 
        {
            Console.WriteLine("\nPerson信息：\n{0}\n{1}\n{2}\n{3}\n{4}",this.id,this.name,this.sex,this.cardid,this.tel);
        
        }

    }
}
