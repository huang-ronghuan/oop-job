﻿using System;

namespace Demo8
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[] { 4, 5, 3, 6, 8 };
            int max = 0;
            int min = 0;
            int sum = 0;
            float avg = 0;
            Math(ref max, ref min, ref sum, ref avg);
            Console.WriteLine("max是："+max.ToString());
            Console.WriteLine("min是："+min.ToString());
            Console.WriteLine("avg是："+avg.ToString());
            Console.ReadKey();
        }

        private static void Math(ref int max, ref int min, ref int sum, ref float avg)
        {
            max = 8;
            min = 3;
            avg = (4 + 5 + 3 + 6 + 8) / 5;

        }
    }
}
