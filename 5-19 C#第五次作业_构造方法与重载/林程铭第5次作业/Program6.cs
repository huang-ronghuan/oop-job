﻿using System;

namespace Demo6
{
    class Student 
    {
        public string name;
        public static string teacher1;
        public static string teacher2;
  
        public Student(string name) 
        {
            this.name = name;
            
        }

        public void Study ()
        {
            Console.WriteLine("大家好，我叫{0}，俺老师叫{1}",this.name,teacher1);
        }
        public void Study1() 
        {
            Console.WriteLine("大家好，我叫{0},俺老师叫{1}",this.name,teacher2);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Student stu1 = new Student("孙悟空");
            Student stu2 = new Student("猪八戒");
            Student stu3 = new Student("沙和尚");
            Student stu4 = new Student("白龙马");
            Student.teacher1 = "唐僧";
            Student.teacher2 = "嫦娥姐姐";
            stu1.Study();
            stu2.Study();
            stu3.Study();
            stu4.Study();
            stu1.Study1();
            stu2.Study1();
            stu3.Study1();
            stu4.Study1();
            Console.ReadKey();



        }
    }
}
