﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Staff
    {
        public string staffid;
        public string name;
        public char gender;
        public string school;
        public string information;


        public Staff()
        {
            
                  }
        public Staff(string staffid,string name,char gender,string school,string information) 
        {
            this.staffid = staffid;
            this.name = name;
            this.gender = gender;
            this.school = school;
            this.information = information;
        }

     

    }

    class Program
    {
        static void Main(string[] args)
        {
            Staff Sta1 = new Staff();
            Sta1.school = "专科";

            Staff Sta2 = new Staff("001", "小林", '男', "闽西职业技术学院","sfjfsdfd");
            Console.WriteLine("无参数构造函数学历:{0} \n",Sta1.school);
            Console.WriteLine("\n 参数构造函数学历：{0}\n",Sta2.school);
            Console.ReadKey();




        }
    }
}
