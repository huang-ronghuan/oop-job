﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson03
{
    class Program
    {
        static void Main(string[] args)
        {

            Class1 id1 = new Class1(01, "张三", "男", "摄影部");
            Class1 id2 = new Class1(02, "李四", "女", "采编部");
            //默认为专科
            Class1.edeucation = "专科";


            Console.WriteLine("1号的信息");
            id1.Print();
            Console.WriteLine("2号的信息");
            id2.Print();

            Console.ReadKey();
        }
    }
}
