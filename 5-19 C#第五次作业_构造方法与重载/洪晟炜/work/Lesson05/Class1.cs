﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson05
{
    class Class1
    {
        public void number(int a, int b)
        {
            Console.WriteLine("两个整数相加=" + (a + b));
        }
        public void number(double c, double d)
        {
            Console.WriteLine("两个小数相加=" + (c + d));
        }
        public void number(string e, string f)
        {
            Console.WriteLine("两个字符串相加=" + (e + f));
        }
        public void number(int n)
        {
            int sum = 0;
            for (int i = 1; i <= n; i++)
            {
                sum += i;
            }
            Console.WriteLine("1到" + n + "的和=" + sum);
        }
    }
}
