﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson01
{
    class Student
    {
        private string name;

        public string Name { get => name; set => name = value; }
        public static string Teacher { get => teacher; set => teacher = value; }

        private static string teacher;


        public Student(string name)
        {
            this.name = name;
        }


        public void Print()
        {
            Console.WriteLine("大家好，我是{0}，俺老师是{1}", name, teacher);
        }
    }
}
