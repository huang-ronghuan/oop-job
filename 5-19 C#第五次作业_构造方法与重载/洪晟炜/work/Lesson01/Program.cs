﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson01
{
    class Program
    {
        static void Main(string[] args)
        {
            Student student1 = new Student("孙悟空");
            Student student2 = new Student("猪八戒");
            Student student3 = new Student("沙和尚");
            Student student4 = new Student("白龙马");
            Student student5 = new Student("张念山");

            //静态成员赋值
            Student.Teacher = "唐僧";

            student1.Print();
            student2.Print();
            student3.Print();
            student4.Print();
            student5.Print();


            //老师换了，将静态成员Teacher重新赋值
            Student.Teacher = "嫦娥姐姐";

            student1.Print();
            student2.Print();
            student3.Print();
            student4.Print();
            student5.Print();

            Console.ReadKey();



        }
    }
}
