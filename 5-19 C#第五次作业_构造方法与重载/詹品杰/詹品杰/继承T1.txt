using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    //    1. 假设要完成一个学校的校园管理信息系统，在员工管理系统中有不同的人员信息，包括学生信息、教师信息等。
    //学生的字段：编号（Id）、姓名（Name）、性别（Sex）、
    //身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）

    //教师的字段：编号（Id）、姓名（Name）,性别 （Sex）、
    //身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno）

    //为学生信息、教师信息创建两个类，并在两个类中分别定义属性和方法：
    //学生类中定义编号（Id）、姓名（Name）、性别（Sex）、
    //身份证号（Cardid）、联系方式（Tel）、专业（Major）、年级（Grade）7 个属性，
    //并定义一个方法在控制台输出这些属性的值。

    //教师类（Teacher）中定义编号（Id）、姓名（Name）,性别 （Sex）、
    //身份证号（Cardid）、联系方式（Tel）、职称（Title）、工资号（Wageno），
    //并将上述属性输岀到控制台。


    class Program
    {
        //创建 教师类
        class Teacher
        {

            public string Id;       // 编号  1
            public string Name;     // 姓名  1
            public string Sex;      // 性别  1
            public string Cardid;   // 身份证号码 1
            public string Tel;      // 联系方式 1
            private string Title;    // 职称   0
            private string Wageno;   // 工资号 0

            public string TitLE { get; set; }
            public string WageNO { get; set; }

            public Teacher(string TiTLE, string WagEno)
            {
                this.Title = TiTLE;
                this.Wageno = WagEno;
            }

            public Teacher()
            {

            }

            public void TfTEXT()
            {
                this.Id = "2021";
                this.Name = "詹三";
                this.Sex = "男";
                this.Cardid = "2656564166325143741646";
                this.Tel = "1131603335";
                this.TitLE = "讲师";
                this.WageNO = "005";

                Console.WriteLine("教师信息输入如下！");
                Console.WriteLine("===================");
                Console.WriteLine("编号：" + this.Id);
                Console.WriteLine("姓名：" + this.Name);
                Console.WriteLine("性别：" + this.Sex);
                Console.WriteLine("身份证号：" + this.Cardid);
                Console.WriteLine("电话：" + this.Tel);
                Console.WriteLine("职称：" + this.TitLE);
                Console.WriteLine("工资号：" + this.WageNO);
                Console.WriteLine("=====================");
            }


            //public  Teacher(string ID, string NAME, string SEX, string CARDID, string TEL, string TITLE, string WAGENO) 
            //{
            //    this.Id = ID;
            //    this.
            //}

            //创建 学生类
            class Student:Teacher
            {

                private string Major;    // 专业   0
                private string Grade;    // 年纪   0

                public Student()
                {
                }

                public Student(string MajoR, string GradE)
                {
                    this.Major = MajoR;
                    this.Grade = GradE;
                }
                public string MajoR { get; set; }
                public string GradE { get; set; }
                public void StuTEXT()
                {
                    this.Id = "2021";
                    this.Name = "詹三";
                    this.Sex = "男";
                    this.Cardid = "2656564166325143741646";
                    this.Tel = "1131603335";
                    this.MajoR = "计算机软件开发";
                    this.GradE = "软件开发四班";

                    Console.WriteLine("学生信息如下信息输入如下！");
                    Console.WriteLine("===================");
                    Console.WriteLine("编号：" + this.Id);
                    Console.WriteLine("姓名：" + this.Name);
                    Console.WriteLine("性别：" + this.Sex);
                    Console.WriteLine("身份证号：" + this.Cardid);
                    Console.WriteLine("电话：" + this.Tel);
                    Console.WriteLine("专业：" + this.MajoR);
                    Console.WriteLine("年纪：：" + this.GradE);
                    Console.WriteLine("=====================");
                }
            }

            static void Main(string[] args)
            {
                Teacher T1 = new Teacher();
                T1.TfTEXT();
                Student S1 = new Student();
                S1.StuTEXT();
                Console.ReadKey();

            }



        }
    }
}
