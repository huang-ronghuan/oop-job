﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo2.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            姓名：
            <asp:TextBox ID ="name" runat ="server" AutoPostBack="True" OnTextChanged="name_TextChanged"></asp:TextBox>
            <br />
            性别：
            <asp:DropDownList ID="sex" runat="server" OnSelectedIndexChanged="sex_SelectedIndexChanged"></asp:DropDownList>
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text=""></asp:Label> 
        </div>
    </form>
</body>
</html>
