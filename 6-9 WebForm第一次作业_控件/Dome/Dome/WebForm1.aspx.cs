﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Dome
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "顾客信息登记";
        }
        protected void Button1_Click(object sender, EventArgs e)
        {

            this.Label1.Text = "当前时间：" + System.DateTime.Now.ToString();
            this.Label1.Text = "<b>请确认您填写的信息：</b>";
            this.Label1.Text += "<br>" + "姓名：" + this.Name.Text;
            this.Label1.Text += "<br>" + "年龄：" + this.Age.Text;
            this.Label1.Text += "<br>" + "爱好：" + this.Happy.Text;
        }
    }
}