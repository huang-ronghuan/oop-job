﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "顾客信息登记";
            if (!Page.IsPostBack)
            {
                this.Label2.Text = System.DateTime.Now.ToString();
            }
            
            }

        protected void Button1_Click(object sender, EventArgs e)
        {
            this.Label2.Visible = true;
            this.Label2.Text = "<b>请确认您填写的信息：</b>";
            this.Label2.Text += "<br>" + "姓名：" + this.name.Text;
            this.Label2.Text += "<br>" + "年龄：" + this.age.Text;
            this.Label2.Text += "<br>" + "爱好：" + this.hobit.Text;
        }
    }

    
    }
