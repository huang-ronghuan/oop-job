﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = DateTime.Now.ToString();
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        protected void Button1_Click1(object sender, EventArgs e)
        {
            Label2.Text = "请确认您填写的信息：" + "<br/>" +
                 "姓名：" + TextBox1.Text + "<br/>" +
                 "年龄：" + TextBox2.Text + "<br/>" +
                 "爱好：" + TextBox3.Text;
        }
    }
}