﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserInfo.aspx.cs" Inherits="WebApplication1.UserInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 675px;
        }
        .auto-style3 {
            height: 49px;
        }
        .auto-style4 {
            width: 271px;
        }
        .auto-style5 {
            width: 400px;
        }
        .auto-style6 {
            margin-bottom: 0px;
        }
        .auto-style7 {
            width: 400px;
            height: 38px;
        }
        .auto-style8 {
            width: 675px;
            height: 38px;
        }
        .auto-style9 {
            width: 271px;
            height: 38px;
        }
        .auto-style10 {
            width: 400px;
            height: 28px;
        }
        .auto-style11 {
            width: 675px;
            height: 28px;
        }
        .auto-style12 {
            width: 271px;
            height: 28px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 96%; height: 589px;">
                <tr>
                    <td colspan="3" class="auto-style3">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label4" runat="server" Text="顾客信息登记表" Width="400px" Font-Size="20"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5" aria-expanded="undefined" aria-grabbed="false">
                        <asp:Label ID="Label1" runat="server" Text="姓名："></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox1" runat="server" Height="30px" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label2" runat="server" Text="年龄：" ></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox2" runat="server" Height="30px" Width="200px" CssClass="auto-style6"></asp:TextBox>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label3" runat="server" Text="爱好："></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="TextBox3" runat="server" Height="292px" TextMode="MultiLine" Width="769px"></asp:TextBox>
                    </td>
                    <td class="auto-style4">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style7">
                        您好：</td>
                    <td class="auto-style8" vertical-align: rigth;>
                        <asp:TextBox ID="TextBox6" runat="server" Height="24px" Width="769px"></asp:TextBox>
                    </td>
                    <td class="auto-style9">
                        <asp:Button ID="Button1" runat="server" Height="63px" OnClick="Button1_Click1" Text="提交" Width="189px" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        </td>
                    <td class="auto-style11">
                        <asp:TextBox ID="TextBox7" runat="server" Height="264px" Width="758px"></asp:TextBox>
                    </td>
                    <td class="auto-style12">
                        </td>
                </tr>
            </table>
        </div>
        <asp:Label ID="Label5" runat="server" Text="请确定您填写的信息："></asp:Label>
    </form>
</body>
</html>
