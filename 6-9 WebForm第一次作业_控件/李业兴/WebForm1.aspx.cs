﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label4.Text ="您好，当前时间是："+DateTime.Now.ToString();
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label5.Text = "请确认您填写的信息：";
            Label6.Text = this.Label1.Text + this.TextBox1.Text;
            Label7.Text = this.Label2.Text + this.TextBox2.Text;
            Label8.Text = this.Label3.Text + this.TextBox3.Text;
        }
    }
}