﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
            //对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
            //在学生类中定义一个方法输出学生信息。
            //在主方法实例化对象，赋值并输出
            Class1 class1 = new Class1();

            class1.id = 1212213;
            class1.name = "快捷";
            class1.Sex = "男";
            class1.Age = 43;
            class1.message = "jkhdfkjshdkjh";
            class1.PrintInfo();
            Console.ReadKey();
        }
    }
}
