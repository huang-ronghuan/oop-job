﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Class1
    {
        //定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
        //对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
        public int id;
        public string name;
        private string sex;
        private int age;
        public string message;

        public int ID {
            get {
                return id;
            }
        }
        public string Name {
            get
            {
                return name;
            }
        }
        public string Sex
        {
            get
            {
                return sex;
            }
            set
            {
                if (value == "男"|| value == "女")
                {
                    sex = value;
                }
                else
                {
                    sex = null;
                }
            }
        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value >= 0 && value <= 128)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                    Console.WriteLine("是非法值（小于0或者大于128岁），该年龄值为0");
                    
                }
            }
        }
        public string Message
        {
            get
            {
                return message;
            }
        }
        public void PrintInfo()
        {
            //学号、姓名、性别、年龄、专业信息
            Console.WriteLine("学号" + id);
            Console.WriteLine("姓名" + name);
            Console.WriteLine("性别" + sex);
            Console.WriteLine("年龄" + age);
            Console.WriteLine("专业信息" + message);
        }
    }
}
