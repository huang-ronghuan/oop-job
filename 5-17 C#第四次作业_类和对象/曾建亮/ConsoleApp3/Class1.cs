﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Class1
    {
        //定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
        //对价格进行赋值限制，小于0价格，赋值为0
        public int id;
        public string name;
        private int price;
        public string press;
        public string message;

        public int Price
        {
            get
            {
                return price;
            }
            set
            {
                if (value>=0)
                {
                    price = value;
                }
                else
                {
                    price = 0;
                }
            }
        }
        public void PrintInfo()
        {
            //编号、书名、价格、出版社、作者信息
            Console.WriteLine("编号" + id);
            Console.WriteLine("书名" + name);
            Console.WriteLine("价格" + price);
            Console.WriteLine("出版社" + press);
            Console.WriteLine("作者信息" + message);
        }
    }
}
