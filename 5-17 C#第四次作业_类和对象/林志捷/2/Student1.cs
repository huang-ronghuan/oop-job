﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
     internal class Student1
    {
        public string stuid;            //学号、
        private string stuname;         //姓名、
        private char stusex;            //性别、
        private int age;             //年龄、
        private string stumajoe;        //专业信息

        public string Stuname
        {
            get
            {
                return stuname;
            }
            set
            {
                    stuname = value;
            }

        }

        public char Stusex
        {
            get
            {
                return stusex;
            }
            set
            {
                if (stusex == '男' || stusex == '女')
                {
                    stusex = value;
                }
            }

        }

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value >= 0 || value <= 128)
                {
                    age = value;
                }
                else
                {
                    age = 0;
                }
            }

        }
        public string Stumajoe
        {
            get
            {
                return stumajoe;
            }
            set
            {
                stumajoe = value;
            }

        }

        public void bbb()
        {
            Console.WriteLine("学生的学号{0}\n姓名:{1}\n性别:{2}\n年龄:{3}\n专业信息:{4}",stuid,Stuname,Stusex,age,stumajoe);
        }
    }
}
