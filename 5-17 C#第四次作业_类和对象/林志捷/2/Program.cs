﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
            //对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁）
            //，该年龄值为0；
            //在学生类中定义一个方法输出学生信息。
            //在主方法实例化对象，赋值并输出
            Student1 sex2 = new Student1();

            sex2.stuid = "20144010";
            sex2.Stuname = "小小";
            sex2.Stusex = '女';
            sex2.Age = 18;
            sex2.Stumajoe = "教育专业";

            sex2.bbb();
            Console.ReadKey(true);
        }
    }
}
