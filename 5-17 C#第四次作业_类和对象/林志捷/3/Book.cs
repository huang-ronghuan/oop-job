﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome4
{
    internal class Book
    {
        private string npbs;
        private string bookname;
        private double money;
        private string press;
        private string author;

        public string Npbs
        {
            get
            {
                return npbs;
            }
            set
            {
                npbs = value;
            }
        }

        public string Bookname
        {
            get
            {
                return bookname;
            }
            set
            {
                bookname = value;
            }
        }

        public double Money
        {
            get
            {
                return money;
            }
            set
            {
                if (value<0)
                {
                    money = 0;
                }
                else
                {
                    money = value;
                }
            }
        }

        public string Press
        {
            get
            {
                return press;
            }
            set
            {
                press = value;
            }
        
        }

        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }

        public void ccc()
        {
            Console.WriteLine("图书的编号:{0}\n书名:{1}\n价格:{2}\n出版社:{3}\n作者信息:{4}", 
            npbs,bookname,money,press,author);
        }

    }
}
