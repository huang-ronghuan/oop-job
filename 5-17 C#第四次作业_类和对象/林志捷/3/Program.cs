﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome4
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
            //对价格进行赋值限制，小于0价格，赋值为0
            //在图书类中定义一个方法输出图书信息；
            //在主方法实例化对象，赋值并输出
            Book a3 = new Book();

            a3.Npbs = "n7502-k11";
            a3.Bookname = "活着";
            a3.Money = 37.5;
            a3.Press = "北京精英社";
            a3.Author = "徐福";

            a3.ccc();
            Console.ReadKey();
        }
    }
}
