﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 stu = new Class1();
            stu.StuID = 1;
            stu.StuName = "测试一";
            stu.StuSex = "男";
            stu.StuAge = 20;
            stu.Message = "测试";

            stu.PrintInfo();
        }
    }
}
