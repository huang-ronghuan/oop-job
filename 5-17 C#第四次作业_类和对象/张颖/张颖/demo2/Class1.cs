﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Class1
    {

        public int StuID;
        public string StuName;
        public string StuSex;
        public int StuAge;
        public string Message;

        public int age
        {
            get
            {
                return StuAge;
            }
            set
            {
                if (value < 0 || value > 128)
                {
                    StuAge = 0;
                }
                else
                {
                    StuAge = value;
                }
            }
        }
        public void PrintInfo()
        {
            Console.WriteLine("学号：" + StuID);
            Console.WriteLine("姓名：" + StuName);
            Console.WriteLine("性别：" + StuSex);
            Console.WriteLine("年龄：" + StuAge);
            Console.WriteLine("专业信息：" + Message);
            Console.ReadKey();
        }
    }
}