﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 book = new Class1();
            book.BookID = 1;
            book.BookName = "测试书籍";
            book.Price = 54;
            book.chubanshe = "测试出版社";
            book.Author = "测试作者";

            book.PrintInfo();
        }
    }
}
