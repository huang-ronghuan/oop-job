﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu = new Student();
            stu.StuID = 1;
            stu.StuName = "测试一";
            stu.StuSex = "男";
            stu.StuAge = 18;
            stu.Message = "测试";

            stu.PrintInfo();
        }
    }
}
