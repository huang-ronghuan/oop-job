﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book = new Book();
            book.BookID = 1;
            book.BookName = "测试书籍";
            book.Price = 33;
            book.PubCom = "测试出版社";
            book.Author = "测试作者";

            book.PrintInfo();
        }
    }
}
