﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Book
    {
        public int BookID;
        public string BookName;
        public int Price;
        public string PubCom;
        public string Author;

        public int price
        {
            get
            {
                return Price;
            }
            set
            {
                if (value < 0)
                {
                    Price = 0;
                }
                else
                {
                    Price = value;
                }
            }
        }

        public void PrintInfo()
        {
            Console.WriteLine("编号：" + BookID);
            Console.WriteLine("书名：" + BookName);
            Console.WriteLine("价格：" + Price);
            Console.WriteLine("出版社：" + PubCom);
            Console.WriteLine("作者信息：" + Author);
            Console.ReadKey();
        }
    }
}
