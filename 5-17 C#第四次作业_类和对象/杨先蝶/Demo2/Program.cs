﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 stu = new Class1();
            stu.StuID = 23;
            stu.StuName = "李白";
            stu.StuSex = "男";
            stu.StuAge = 26;
            stu.Message = "诗人";

            stu.Info();
        }
    }
}
