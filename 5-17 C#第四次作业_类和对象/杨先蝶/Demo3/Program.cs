﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEmo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Class1 book = new Class1();
            book.BookID = 125665;
            book.BookName = "红楼梦";
            book.Price = 80;
            book.Publisher = "清华出版社";
            book.Author = "曹雪芹";

            book.PrintInfo();
        }
    }
}
