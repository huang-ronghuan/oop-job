﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEmo3
{
    class Class1
    {
        public int BookID;
        public string BookName;
        public int Price;
        public string Publisher;
        public string Author;

        public int price
        {
            get
            {
                return Price;
            }
            set
            {
                if (value < 0)
                {
                    Price = 0;
                }
                else
                {
                    Price = value;
                }
            }
        }

        public void PrintInfo()
        {
            Console.WriteLine("编号：" + BookID);
            Console.WriteLine("书名：" + BookName);
            Console.WriteLine("价格：" + Price);
            Console.WriteLine("出版社：" + Publisher);
            Console.WriteLine("作者信息：" + Author);
            Console.ReadKey();
        }
    }
}
