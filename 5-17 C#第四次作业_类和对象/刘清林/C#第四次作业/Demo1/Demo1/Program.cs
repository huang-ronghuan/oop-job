﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
   
    class Program
    {

        static void Main(string[] args)
        {
            User ues1 = new User();
            User ues2 = new User();

            ues1.account = 1433233;
            ues1.name = "吴亦凡";
            ues1.cipher = "123456";

            ues2.account = 25802580;
            ues2.name = "彭于晏";
            ues2.cipher = "654321";

            ues1.Pass();
            ues2.Pass();

            Console.WriteLine("账户：" + ues2.account);
            Console.WriteLine("用户名：" + ues2.name);
            Console.WriteLine("密码：" + ues2.cipher);

            Console.ReadKey();
        }
    }
}
