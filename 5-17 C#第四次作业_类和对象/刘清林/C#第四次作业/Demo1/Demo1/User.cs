﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class User
    {
        public int account;//账户

        public string name;//用户名

        public string cipher;//密码
        public string Cipher
        {
            get
            {
                return cipher;
            }
            set
            {
                //判断value值是否满足赋值条件；value存放的是外部对属性进行赋值操作时右边表达式的具体值；
                if (value == "123456" || value == "654321")
                {
                    cipher = value;
                }
                else
                {
                    //不满足条件时，可根据业务需求实现处理代码；
                    //输出错误提示；
                    Console.WriteLine("密码错误");
                }
            }
        }
        public void Pass()
        {
            Console.WriteLine("账号" + account);
            Console.WriteLine("用户名" + name);
            Console.WriteLine("密码" + cipher);
        }
    }
}
