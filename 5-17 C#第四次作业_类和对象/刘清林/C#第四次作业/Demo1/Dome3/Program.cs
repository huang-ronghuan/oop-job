﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
    class Program
    {
        static void Main(string[] args)
        {
            Book book1 = new Book();
            Book book2 = new Book();
            Book book3 = new Book();

            book1.BookId = 9669369;
            book1.BookName = "金瓶梅";
            book1.price = 98;
            book1.Address = "湖北人民出版社";
            book1.Wirter = "兰陵笑笑生";

            book2.BookId = 1433233;
            book2.BookName = "斗罗大陆";
            book2.price = 25;
            book2.Address = "北京马家屯出版社";
            book2.Wirter = "唐家三少";

            book3.BookId = 7758258;
            book3.BookName = "三国演义";
            book3.price = 68;
            book3.Address = "福建教育出版社";
            book3.Wirter = "罗贯中";

            book1.BookInfo();
            book2.BookInfo();
            book3.BookInfo();

            Console.WriteLine("编号:"+book3.BookId);
            Console.WriteLine("书名:" +book3.BookName);
            Console.WriteLine("价格:" +book3.price);
            Console.WriteLine("出版社:" +book3.Address);
            Console.WriteLine("作者信息:" +book3.Wirter);

            Console.ReadKey();
        }
    }
}
