﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Student
    {
        public string name;

        public string job;

        public string sex;

        public int age;


        public string Sex
        {
            get
            {
                return sex;
            }
            set
            {
                if (value == "男" || value == "女")
                {
                    sex = value;
                }
                else
                {
                    Console.WriteLine("性别赋值错误");
                }
            }

        }
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value >= 0 && value <= 128)
                {
                    age = value;
                }

            }
        }
        public int GetAge()
        {
            return age;
        }


        public void SetAge(int age)
        {
            this.age = age;
        }
        public void PrintInfo()
        {
            Console.WriteLine("姓名：" + name);
            Console.WriteLine("性别：" + sex);
            Console.WriteLine("年龄：" + age);
            Console.WriteLine("专业信息：" + job);
        }
    }
 }
