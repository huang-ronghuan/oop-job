﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu1 = new Student();
            Student stu2 = new Student();
            Student stu3 = new Student();

            stu1.name = "彭于晏";
            stu1.Sex = "男";
            stu1.Age = 35;
            stu1.job = "赛车手";

            stu2.name = "迪丽热巴";
            stu2.Sex = "女";
            stu2.Age = 24;
            stu2.job = "演员";

            stu3.name = "蔡徐坤";
            stu3.Sex = "男";
            stu3.Age = 22;
            stu3.job = "篮球明星";

            stu1.PrintInfo();
            stu2.PrintInfo();
            stu3.PrintInfo();


            Console.WriteLine("姓名：" + stu1.name);
            Console.WriteLine("性别：" + stu1.Sex);
            Console.WriteLine("年龄：" + stu1.GetAge());
            Console.WriteLine("专业信息："+ stu1.job);


            Console.ReadKey();


        }
    }
}
