﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Student
    {

        public int stuid;
        public string name;
        public string sex;
        public int age;
        public string major;

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value >= 0 && value <= 128)
                {
                    age = value;
                }
            }
        }

        public void study()
        {
            Console.WriteLine("学号:{0}\n姓名：{1}\n性别：{2}\n年龄：{3}\n专业信息：{4}", stuid, name, sex, Age, major);
        }
    }
}
