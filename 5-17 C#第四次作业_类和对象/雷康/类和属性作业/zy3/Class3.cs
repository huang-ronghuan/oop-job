﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy3
{
    class Class3
    {
        private int number;
        public string bookname;
        private int price;
        public string pumk;
        public string writer;

        public int Number 
        {
            get 
            {
                return number;

            }
            set 
            {
                if (number>=0) 
                {
                    number = value;
                }
            }
        }
        public int Price 
        {
            get 
            {
                return price;
            }
            set 
            {
                if (price >=0)
                {
                    price = value;
                }
              
            }
        }

        public void printinfo() 
        {
            Console.WriteLine("编号："+number);
            Console.WriteLine("书名："+bookname);
            Console.WriteLine("价格："+price);
            Console.WriteLine("年龄："+pumk);
            Console.WriteLine("作者信息："+writer);
        }
    }
}
