﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy3
{
    class Program
    {
        static void Main(string[] args)
        {
            //3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
            //对价格进行赋值限制，小于0价格，赋值为0
            //在图书类中定义一个方法输出图书信息；
            //在主方法实例化对象，赋值并输出

            Class3 num3 = new Class3();

            num3.Number = 1;
            num3.bookname = "如何把富婆";
            num3.Price = 120;
            num3.pumk = "人民出版社";
            num3.writer = "李四";

            num3.printinfo();

            Console.ReadKey();

        }
    }
}
