﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Class2
    {
        public int xuehao;
        public string name;
        private string sex;
        private int age;
        public string skill;

        public string Sex
        {
            get
            {
                return sex;
            }
        
            set 
            {
                if (sex=="男" || sex=="女") {
                    sex = value;
                }
            }
        }
        public int Age 
        {
            get 
            {
                return age;
            }
            set 
            {
                if (age >= 0 && age <= 128)
                {
                    age = value;
                }
                else {
                    age = 0;
                }
            }
        }
        public void printinfo() {
            Console.WriteLine("学号："+xuehao);
            Console.WriteLine("姓名："+name);
            Console.WriteLine("性别："+sex);
            Console.WriteLine("年龄："+age);
            Console.WriteLine("专业信息："+skill);
        }
    }
}
