﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Class3
    {
        public int ID;  //书本编号
        public string Name;//书本名
        public int Price;  //价格
        public string PubCom;//出版社
        public string Author; //作者

        public int price
        {
            get
            {
                return Price;
            }
            set
            {
                if (value < 0)
                {
                    Price = 0;
                }
                else
                {
                    Price = value;
                }
            }
        }

        public void PrintInfo()
        {
            Console.WriteLine("编号：" + ID);
            Console.WriteLine("书名：" + Name);
            Console.WriteLine("价格：" + Price);
            Console.WriteLine("出版社：" + PubCom);
            Console.WriteLine("作者：" + Author);
            Console.ReadKey();
        }
    }
}
