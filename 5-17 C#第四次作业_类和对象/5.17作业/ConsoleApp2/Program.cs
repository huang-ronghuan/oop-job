﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {

        //2. 定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
       // 对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
   //在学生类中定义一个方法输出学生信息。
  // 在主方法实例化对象，赋值并输出
        static void Main(string[] args)
        {
            Class2 c2 = new Class2();

            c2.stuid = "2044010431";
            c2.name = "狄仁杰";
            c2.sex = "男";
            c2.age = 20;
            c2.specialty = "新媒体";

            c2.PrintInfo();


            Console.ReadKey();



        }
    }
}
