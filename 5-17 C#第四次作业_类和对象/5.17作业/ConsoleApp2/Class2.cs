﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Class2
    {
        public string stuid;  //定义字段存储学生学号
        public string name;   //姓名
        public string sex;  //性别
        public int age;  //年龄
        public string specialty;  //专业

        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (value < 0 || value > 128)
                {
                    age = 0;
                }
                else
                {
                    age = value;
                }
            }
        }
                public void PrintInfo()
                {
                    Console.WriteLine("学号:" + stuid);
                    Console.WriteLine("姓名:" + name);
                    Console.WriteLine("性别:" + sex);
                    Console.WriteLine("年龄:" + age);
                    Console.WriteLine("专业:" + specialty);
                }
            }
        }

    

