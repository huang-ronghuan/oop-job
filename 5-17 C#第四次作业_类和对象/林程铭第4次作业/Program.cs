﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class User 
    {
        public string admin;
        public string username;
        public string password;

        public User(string admin, string username, string password) 
        {
            this.admin = admin;
            this.username = username;
            this.password = password;

        }
        public void study() 
        {
            Console.WriteLine("用户账号：{0}，用户名：{1}，密码：{2}", this.admin, this.username, this.password);
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            User u = new User("2662298551", "李明", "123456789");
            u.study();
            Console.ReadKey();


        }
    }
}
