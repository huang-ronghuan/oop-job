﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo3
{
    class Library 
    {
        public string bookid;
        public string bookname;
        private int price;
        public int Price 
        {
            get 
            {
                return price;
            }
            set 
            {
                if (value > 0)
                {
                    price = value;
                }

            }
        }
        public string press;
        public string eidiinfo;

        public Library(string bookid, string bookname, int price, string press, string eidiinfo) 
        {
            this.bookid = bookid;
            this.bookname = bookname;
            this.price = price;
            this.press = press;
            this.eidiinfo = eidiinfo;
        }

        public void library ()
        {
            Console.WriteLine("编号：{0}",bookid);
            Console.WriteLine("书名：{0}",bookname);
            Console.WriteLine("价格：{0}",price);
            Console.WriteLine("出版社：{0}",press);
            Console.WriteLine("作者信息：{0}",eidiinfo);
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            Library lib = new Library("001", "《云边有个小卖部》", 48, "湖南出版社", "张嘉佳");
            lib.library();
            Console.ReadKey();

        }
    }
}
