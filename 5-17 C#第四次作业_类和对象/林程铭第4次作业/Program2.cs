﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Student 
    {
        public string classid;
        public string name;
        public string sex;
        private int age;
        public int Age 
        {
            get 
            {
                return age;  
            }
            set 
            {
                if (value>0 && value<128)
                {
                    age = value;
                }
            }
        
        }
        public string specialinfo;

        public Student(string classid, string name, string sex, int age, string specialinfo) 
        {
            this.classid = classid;
            this.name = name;
            this.sex = sex;
            this.age = age;
            this.specialinfo = specialinfo;
        
        }

        public void Study() 
        {
            Console.WriteLine("学号：{0}", classid);
            Console.WriteLine("姓名：{0}",name);
            Console.WriteLine("性别：{0}",sex);
            Console.WriteLine("年龄：{0}",age);
            Console.WriteLine("专业信息：{0}",specialinfo);
        }


    }

    class Program
    {
        static void Main(string[] args)
        {
            Student stu = new Student("001", "李明", "男", 20, "软件工程");
            stu.Study();
            Console.ReadKey();

        }
    }
}
