﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Program
    {
        private int studentID;
        private string name;
        private string sex;
        private int age;
        private string specialty;
        static void Main(string[] args)
        {
            Class1 a = new Class1();
            Class1 b = new Class1();
            a.StudentID = 01;
            a.Name = "李晓燕";
            a.Sex = "女";
            a.Age = 17;
            a.Specialty = "软件1班";

            b.StudentID = 02;
            b.Name = "李逍遥";
            b.Sex = "男";
            b.Age = 18;
            b.Specialty = "软件2班";

            a.PrintInfo();
            b.PrintInfo();
        }
    }
}
