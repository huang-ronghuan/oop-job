﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Class1
    {
        private int studentID;
        public int StudentID
        {
            get
            {
                return studentID;
            }
            set
            {
                studentID = value;
            }
        }
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        private string sex;
        public string Sex
        {
            get
            {
                return sex;
            }
            set
            {
                sex = value;
            }
        }
        private int age;
        public int Age
        {
            get
            {
                return age;
            }
            set
            {
                if (Age<0 && age>128)
                {
                    Age = 0;
                }
                age = value;
            }
        }
        private string specialty;
        public string Specialty
        {
            get
            {
                return specialty;
            }
            set
            {
                specialty = value;
            }

        }
        public void PrintInfo() { 
        Console.WriteLine("学号:" +studentID );
            Console.WriteLine("姓名:" + name);
            Console.WriteLine("性别:" + sex);
            Console.WriteLine("年龄:" + age);
            Console.WriteLine("专业信息:" + specialty);
        }
    }
}
