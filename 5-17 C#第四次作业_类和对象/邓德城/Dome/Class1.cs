﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome
{
    class Class1
    {
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        private string userID;
        public string UserID
        {
            get
            {
                return userID;
            }
            set
            {
                userID = value;
            }
        }
        private string userPwd;
        public string UserPwd
        {
            get
            {
                return userPwd;
            }
            set
            {
                userPwd=value;
            }
        }
        public void PrintInfo()
        {
            Console.WriteLine("姓名:" + name);
            Console.WriteLine("用户:" + userID);
            Console.WriteLine("密码:" + userPwd);
        }
    }
}
    

