﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
    class Class1
    {
        private int iD;
        public int ID
        {
            get
            {
                return iD;
            }
            set
            {
                iD = value;
            }
        }
        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        private int price;
        public int Price
        {   
            get
            {
                return price;
            }
            set
            {
                if (price<0)
                {
                    price = 0;
                }
                price = value;
            }
        }
        private string press;
        public string Press
        {
            get
            {
                return press;
            }
            set
            {
                press = value;
            }
        }
        private string author;
        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }
        public void PrintInfo()
        {
            Console.WriteLine("编号："+iD);
            Console.WriteLine("书名："+name);
            Console.WriteLine("价格："+ price);
            Console.WriteLine("出版社："+ Press);
            Console.WriteLine("作者："+ author);
        }
    }
}
