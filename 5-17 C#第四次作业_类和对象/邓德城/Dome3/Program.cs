﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome3
{
    class Program
    {
        private int iD;
        private string name;
        private int price;
        private string press;
        private string author;
        static void Main(string[] args)
        {
            Class1 a = new Class1();
            Class1 b = new Class1();

            a.ID = 1;
            a.Name = "格林童话";
            a.Price = 35;
            a.Press = "DDC出版社";
            a.Author = "格林";

            b.ID = 2;
            b.Name = "安徒生童话";
            b.Price = 40;
            b.Press = "DDC出版社";
            b.Author = "安徒生";

            a.PrintInfo();
            b.PrintInfo();
        }
    }
}
