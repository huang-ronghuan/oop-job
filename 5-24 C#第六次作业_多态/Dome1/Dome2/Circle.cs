﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Circle : Shape
    {
        public Circle(double r, double a) : base(r, a)
        {
        }

        public override void color()
        {
            Console.WriteLine("园的面积:" + (3.14 * r * r));
        }

    }
}
