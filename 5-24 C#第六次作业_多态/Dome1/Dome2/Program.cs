﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的半径");
            double r = double.Parse( Console.ReadLine());
            Console.WriteLine("请输入正方形的边长");
            double a = double.Parse(Console.ReadLine());
            Circle circle = new Circle(r, a);
            circle.color();
            Square square = new Square(r, a);
            square.color();
            Console.ReadKey();
        }
    }
}
