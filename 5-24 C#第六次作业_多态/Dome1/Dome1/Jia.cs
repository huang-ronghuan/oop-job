﻿using FakeItEasy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using VisioForge.Shared.MediaFoundation.OPM;

namespace Dome1
{
    internal class Jia : Calculate
    {
        public override void DisplayResult()
        {
            Console.WriteLine("a+b=" + (a + b));
        }

        public Jia(int a, int b) : base(a, b)
        {
        }
    }
}
