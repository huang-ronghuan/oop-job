﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("请输入第一个整数：");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个整数：");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入一个操作符");
            string str = Console.ReadLine();

            switch (str)
            {
                case "+":
                    Jia jia = new Jia(a, b);
                    jia.DisplayResult();
                    break;
                case "-":
                    Jian jian = new Jian(a, b);
                    jian.DisplayResult();
                    break;
                case "*":
                    Cheng cheng = new Cheng(a,b);
                    cheng.DisplayResult();
                    break;
                case "/":
                    Chu chu = new Chu(a, b);
                    chu.DisplayResult();
                    break;
                default:
                    Console.WriteLine("操作符错误");
                    break;
            }
            Console.ReadLine();
        }
    }
}
