﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework6
{
    class Square:Shape
    {
        private double sidelen;

        public Square(double sidelen)
        {
            this.sidelen = sidelen;
        }

        public override void GetArea()
        {
            Console.WriteLine(sidelen * sidelen);
        }

    }
}
