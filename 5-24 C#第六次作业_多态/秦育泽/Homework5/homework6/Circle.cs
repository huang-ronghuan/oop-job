﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework6
{
    class Circle:Shape
    {
        private double radius;

        public Circle(double radius)
        {
            this.radius = radius;
        }

        public override void GetArea()
        {
            Console.WriteLine(radius * radius * Math.PI);
        }
    }
}
