﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace homework6
{
    class Program
    {
        static void Main(string[] args)
        {

            //            二、创建一个Shape（形状）类，此类包含一个名为color的数据成员，用于存储颜色，这个类还包含一个名为GetArea()的虚方法（这个方法是用来获取形状面积的）。
            //基于这个Shape，创建两个子类：Circle（圆形类）和Square（正方形类），Circle类中包含radius（半径）的数据成员，Square类中包含sideLen（边长）的数据成员，这两个子类都去重写父类的GetArea()方法，各自去实现计算自己的面积。

            //在主类中添加一个方法，参数类型就是Shape，方法体中，用形参去调用GetArea()方法。
            //main方法中去测试这个方法。

            Console.WriteLine("请数字选择：1 圆形 2 正方形");
            int n = int.Parse(Console.ReadLine());
            if (n == 1)
            {
                Console.WriteLine("请输入圆形半径：");
                double radius = double.Parse(Console.ReadLine());
                Circle c1 = new Circle(radius);
                zbc(c1);
            }
            else if (n == 2)
            {
                Console.WriteLine("请输入正方形的边长：");
                double sidelen = double.Parse(Console.ReadLine());
                Square s1 = new Square(sidelen);
                zbc(s1);
            }
            else
            {
                Console.WriteLine("请输入正确的数字进行选择！！！");
            }
            Console.ReadKey();
        }
        static void zbc(Shape s1)
        {
            s1.GetArea();
        }


    }
    
}
