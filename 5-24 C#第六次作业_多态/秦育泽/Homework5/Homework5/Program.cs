﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{
    class Program
    {

//        一、编写一个控制台应用程序，接受用户输入的两个整数和一个操作符，以实现对两个整数的加、减、乘、除运算，并显示出计算结果。

//1、创建Calculate基类，其中包含两个整型的protected成员，用以接收用户输入的两个整数。定义一个DisplayResult()虚方法，计算并输出结果。
//2、定义四个类继承自Calculate类，分别重写DisplayResult()方法，实现两个整数的加、减、乘、除运算，并输出结果。
//3、根据用户输入的操作符，实例化相应的类，完成运算并输出结果。
//4、在主类中添加一个方法，形参为父类对象，根据传递实参的类型，调用方法，实现计算和显示结果。


        static void Main(string[] args)
        {
            Console.WriteLine("输入俩个整数和一个字符进行运算/n输入第一个数：");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数字:");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入字符：");
            string s = Console.ReadLine();
            if (s == "+")
            {
                Add add = new Add(num, num1);
                print(add);
            }
            else if (s == "-")
            {
                Subtract subtract = new Subtract(num, num1);
                print(subtract);
            }
            else if (s == "*")
            {
                Trave travel = new Trave(num, num1);
                print(travel);
            }
            else if (s == "/")
            {
                Doorsteps doorsteps = new Doorsteps(num, num1);
                print(doorsteps);
            }
            else
            {
                Console.WriteLine("输入的符号有误请重新运行程序！！！");
            }
            Console.ReadKey();
        }

        static void print(Calculate c1)
        {
            c1.DisplayResult();
        }

    }
    
}
