﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework5
{
    class Add : Calculate
    {
        public Add(int num, int num1) : base(num, num1) { }

        public override void DisplayResult()
        {
            Console.WriteLine(num + num1);
        }

    }
}
