﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo6
{   //6.在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，然后将数组中下标是偶数的元素输出。
    class Program
    {
        static void Main(string[] args)
        {
            string [] array = new string[5];

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine("请输入第{0}个数", i + 1);
                array[i] = Console.ReadLine();
            }
            int number = 0;
            for(int i = 0;i < array.Length;i++)
            {
                if (i % 2 == 0)
                {
                    Console.WriteLine("下标为偶数的是：{0}", array[i]);
                }
            }
            Console.ReadKey();
        }
            
        
    }
}
