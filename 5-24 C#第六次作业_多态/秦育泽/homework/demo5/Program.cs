﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo5
{
    class Program
    {       //5.实现查找数组元素索引的功能。定义一个数组，然后控制台输入要查找的元素，返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。(不要用Array类的方法)
        static void Main(string[] args)
        {
            int []array = { 1,2,3,4,5,6,7,8,9,10 };
            Console.WriteLine("请输入需要查找的元素");
            int search = int.Parse(Console.ReadLine());
            int c = 0;
            for(int i = 0; i < array.Length;i++)
            {
                if(search == array[i])
                {
                    c = i;
                }

            }
            Console.WriteLine("最后一次出现的位置：array{{0}}", c);
            int d = 0;
            for(int i = 0;i < array.Length;i++)
            {
                if(search != array[i])
                {
                    d++;
                }
            }
            if( d == 0)
            {
                Console.WriteLine("找不到");

            }
            Console.ReadKey();




        }
    }
}
