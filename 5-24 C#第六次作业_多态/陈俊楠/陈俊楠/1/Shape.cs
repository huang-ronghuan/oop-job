﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Shape
    {
        //父类
        //输出颜色 让用户可以输出想要图形的面积 protected 子类能够使用父类的数据
        protected string color;

        public string Color { get => color; set => color = value; }

        //virtual 虚方法 在子类方法添加override可以更新
        public virtual void Print()
        {
            Console.WriteLine("得到的结果如下：");
        }

    }
}
