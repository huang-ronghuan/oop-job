﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Square:Shape
    {
        //计算正方形

        //接受用户输入的边长
        private double b;

        public double B { get => b; set => b = value; }

        //属性赋值
        public Square(double b)
        {
            this.b = b;
        }

        //调用方法
        public override void Print()
        {
            base.Print();
            Console.WriteLine("正方形的面积为："+(b*b));
        }
    }
}
