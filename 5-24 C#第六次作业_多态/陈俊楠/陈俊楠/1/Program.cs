﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("*******欢迎进入面积计算系统*******");
            //登录页面
            //定义布尔类型判断是否退出还是继续运行
            bool a = true;
            while (a)
            {
                Console.WriteLine("");
                Console.WriteLine("请根据数字输入你要计算的图形");
                Console.WriteLine("1.圆形 2.正方形 3.退出");
                int num = int.Parse(Console.ReadLine());
                switch (num)
                {
                    case 1:
                        Console.WriteLine("请输入圆的半径");
                        int rr = int.Parse(Console.ReadLine());
                        Circle circle = new Circle(rr);
                        circle.Print();
                        break;
                    case 2:
                        Console.WriteLine("请输入正方形的半径");
                        int bb = int.Parse(Console.ReadLine());
                        Square square = new Square(bb);
                        square.Print();
                        break;
                    case 3:
                        Console.WriteLine("退出系统中....");
                        a = false;
                        break;
                    default:
                        Console.WriteLine("输入的数字有误！请核对后输入");
                        a = true;
                        break;
                }


            }
        }
    }
}
