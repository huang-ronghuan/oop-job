﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Circle:Shape
    {
        //计算圆形面积 接受用户输入的信息

        //半径
        private double r;

        public double R { get => r; set => r = value; }

        //属性赋值
        public Circle(double r)
        {
            this.r = r;
        }
        //更新父辈的输出
        public override void Print()
        {
            base.Print();
            Console.WriteLine("π的值默认为3.14");
            Console.WriteLine("圆的面积为"+(2*3.14*r));
        }


    }
}
