﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class Jiafa:Fuqin
    {
        //计算加法
        private string jiafa1;

        //属性封装
        public string Jiafa1 { get => jiafa1; set => jiafa1 = value; }

        public Jiafa(int num1,int num2,string jiafa1) : base(num1, num2)
        {
            //赋值
            this.jiafa1 = jiafa1;
        }

        //主方法输出
        public override void Print()
        {
            //输出父类的输出方法
            base.Print();
            //输出运算
            Console.WriteLine(num1+jiafa1+num2+"="+(num1+num2));


        }


    }
}
