﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class ChuFa:Fuqin
    {
        //定义符号
        private string Chufu4;

        //属性封装
        public string Chufu21 { get => Chufu4; set => Chufu4 = value; }

        public ChuFa(int num1, int num2, string Chufu4) : base(num1, num2)
        {
            //赋值
            this.Chufu4 = Chufu4;
        }

        //主方法输出
        public override void Print()
        {
            //输出父类的输出方法
            base.Print();
            //输出运算
            Console.WriteLine(num1 + Chufu4 + num2 + "=" + (num1 / num2));
        }


    }
}
