﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class Chengfa:Fuqin
    {
        //设置符号
        private string chengfa3;

        //属性封装
        public string Chengfa3 { get => chengfa3; set => chengfa3 = value; }

        public Chengfa(int num1, int num2, string chengfa3) : base(num1, num2)
        {
            //赋值
            this.chengfa3 = chengfa3;
        }

        //主方法输出
        public override void Print()
        {
            //输出父类的输出方法
            base.Print();
            //输出运算
            Console.WriteLine(num1 + chengfa3 + num2 + "=" + (num1 * num2));
        }

    }
}
