﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class Jianfa:Fuqin
    {
        //设置用户输入
        private string jianfa2;

        //属性封装
        public string Jianfa2 { get => jianfa2; set => jianfa2 = value; }

        public Jianfa(int num1,int num2,string jianfa2 ) : base(num1,num2)
        {
            //赋值
            this.jianfa2 = jianfa2;
        }

        //输出方法
        public override void Print()
        {
            base.Print();
            Console.WriteLine((num1 + Jianfa2 + num2 + "=" + (num1 - num2)));
        }
    }
}
