﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class Fuqin
    {
        //存放两个用户输出的变量和操作符
        protected int num1;
        protected int num2;

        //属性封装
        public int Num1 { get => num1; set => num1 = value; }
        public int Num2 { get => num2; set => num2 = value; }

        //设置用户输入赋值
        public Fuqin(int num1,int num2)
        {
            this.num1 = num1;
            this.num2 = num2;
        }

        //输出测试
        public virtual void Print()
        {
            Console.WriteLine("");
        }
    }
}
