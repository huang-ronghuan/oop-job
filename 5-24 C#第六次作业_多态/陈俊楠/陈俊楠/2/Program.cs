﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson524_text
{
    class Program
    {
        static void Main(string[] args)
        {
            //通过在父类的方法中 将父类的输出方法修改为 访问修饰符 virtual 返回值 方法名
            //再通过子类的方法中 用 访问修饰符 override 返回值 和父类相同的方法名
            //通过public 类名（定义的数据名）：base（父类的数据）
            //通过this 来对用户输出的值用于赋予 才可以进行计算
            //通过这样的操作对父类方法进行更新隐藏 在子类方法中 base.Print输出父类原有的数据
            //在通过cw新增语句 通过引入用户输入的两个数字 进行加减乘除的操作
            
            //一、编写一个控制台应用程序，接受用户输入的两个整数和一个操作符，以实现对两个整数的加、减、乘、除运算，并显示出计算结果。
            Console.WriteLine("********欢迎进入计算系统********");
            Console.WriteLine("");

            //接受用户输入的第一个数字
            Console.WriteLine("请输入第一个数字");
            int num1 = int.Parse(Console.ReadLine());
            //接受用户输入的第二个数字
            Console.WriteLine("请输入第二个数字");
            int num2 = int.Parse(Console.ReadLine());

            //定义变量进行或者退出程序
            bool a = true;
            while (a)
            {
                //测试是否能接受用户输入
                //Fuqin fuqin = new Fuqin(num1,num2);
                //fuqin.Print();

                Console.WriteLine("请根据符号输出你要计算的办法");
                Console.WriteLine("+:数字相加 -:数字相减 *:数字相乘 /:数字相除`");
                Console.WriteLine("请输入你要进行的操作符号");
                string b = string.Format(Console.ReadLine());


                switch (b)
                {
                    //计算加法
                    case "+":
                        Jiafa jiafa = new Jiafa(num1,num2,b);
                        jiafa.Print();
                        Console.WriteLine("");
                        break;
                    //计算减法
                    case "-":
                        Jianfa jianfa = new Jianfa(num1, num2, b);
                        jianfa.Print();
                        Console.WriteLine("");
                        break;
                    //计算乘法
                    case "*":
                        Chengfa chengfa = new Chengfa(num1, num2, b);
                        chengfa.Print();
                        break;
                    //计算除法
                    case "/":
                        ChuFa chufa = new ChuFa(num1, num2, b);
                        chufa.Print();
                        Console.WriteLine("");
                        break;
                    default:
                        Console.WriteLine("输出有误!请看清符号后输出符号！");
                        a = true;
                        Console.WriteLine("");
                        break;
                }

                Console.ReadKey();
            }

        }
    }
}
