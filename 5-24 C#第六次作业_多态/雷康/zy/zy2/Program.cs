﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Program
    {
 //创建一个Shape（形状）类，此类包含一个名为color的数据成员，用于存储颜色，
 ////这个类还包含一个名为GetArea()的虚方法（这个方法是用来获取形状面积的）。
//  基于这个Shape，创建两个子类：Circle（圆形类）和Square（正方形类），
//  //Circle类中包含radius（半径）的数据成员，Square类中包含sideLen（边长）的数据成员，
// //这两个子类都去重写父类的GetArea()方法，各自去实现计算自己的面积。
//  在主类中添加一个方法，参数类型就是Shape，方法体中，用形参去调用GetArea()方法。
// main方法中去测试这个方法。

        static void Main(string[] args)
        {
            Console.WriteLine("1.圆的面积.2.正方形的面积");
            int i = int.Parse(Console.ReadLine());
            
            switch (i)
            {
                case 1:
                    Console.WriteLine("请输入圆的半径");
                    double m = double.Parse(Console.ReadLine());
                    Circle circle = new Circle(m);
                    print(circle);
                    break;
                case 2:
                    Console.WriteLine("请输入正方形的长");
                    double n = double.Parse(Console.ReadLine());
                    Square square = new Square(n);
                    print(square);
                    break;
                default:
                    Console.WriteLine("输入错误");
                    break;

            }
            Console.ReadKey();
           
        }
        public static void print(Shape shape)
        {
            shape.Getarea();
        }
    }
}
