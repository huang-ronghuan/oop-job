﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson01
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入第一个整数：");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个整数：");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入操作符：");
            string str = Console.ReadLine();

            switch (str)
            {
                case "+":
                    add a = new add(num1, num2);
                    display(a);
                    break;
                case "-":
                    subtract s = new subtract(num1, num2);
                    display(s);
                    break;
                case "*":
                    multiply m = new multiply(num1, num2);
                    display(m);
                    break;
                case "/":
                    divide d = new divide(num1, num2);
                    display(d);
                    break;
                default:
                    Console.WriteLine("输入操作符有误！");
                    break;
            }
            Console.ReadKey();
        }

        public static void display(Calculate calculate)
        {
            calculate.DisplayResult();
        }
    }

}