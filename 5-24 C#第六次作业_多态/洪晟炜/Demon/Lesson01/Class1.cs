﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lesson01s
{
    class Calculate
    {
        protected int num1;
        protected int num2;

        public Calculate(int number1, int number2) {
            this.num1 = number1;
            this.num2 = number2;
        }

        public virtual void DisplayResult() { 
        
        }
    }
}
