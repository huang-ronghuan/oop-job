﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Shape
    {
        private string color;
        private double r;
        private double b;

        public string Color { get => color; set => color = value; }
        public double R { get => r; set => r = value; }
        public double B { get => b; set => b = value; }

        public Shape(double r, string color)
        {
            this.color = color;
            this.r = r;
            this.b = b;
        }

        public Shape(double r, double b)
        {
            this.r = r;
            this.b = b;
        }

        public virtual void GetArea()
        {

        }
    }
}
