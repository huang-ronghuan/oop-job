﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请挑选一个颜色 （红=圆形  蓝=正方形）");
            string color = Console.ReadLine();

            Console.WriteLine("请输入圆的半径");
            double r = double.Parse(Console.ReadLine());

            Console.WriteLine("请输入正方形的边长");
            double b = double.Parse(Console.ReadLine());

            switch (color)
            {
                case "红":

                    Circle circle = new Circle(r, b);
                    GetArea(circle);
                    break;

                case "蓝":

                    Square square = new Square(r, b);
                    GetArea(square);

                    break;
                default:
                    break;
            }
            
            Console.ReadKey();
        }
        public static void GetArea(Shape shape)
        {
            shape.GetArea();
        }
    }
}
