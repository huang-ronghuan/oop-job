﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入第一个数");
            int n = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入第二个数");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入操作符");
            string str = Console.ReadLine();


            switch (str)
            {
                case "+":
                    Add add = new Add (n, m);
                    Display(add);
                    break;
                case "*":
                    Product product = new Product(n, m);
                    Display(product);
                    break;
                case "-":
                    Subtracts subtracts = new Subtracts(n, m);
                    Display(subtracts);
                    break;
                case "/":
                    Division division = new Division(n, m);
                    Display(division);
                    break;
                default:
                    Console.WriteLine("操作符错误");
                    break;
            }
            Console.ReadKey();
        }
        public static void Display(Calculate caldulate)
        {
            caldulate.DisplayResult();
        }
    }
}
