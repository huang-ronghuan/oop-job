﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Calculate
    {
        private int n;
        private int m;

        public Calculate(int n, int m)
        {
            this.N = n;
            this.M = m;
           
        }

        public int N { get => n; set => n = value; }
        public int M { get => m; set => m = value; }

        public virtual void DisplayResult()
        {

        }
    }
}
