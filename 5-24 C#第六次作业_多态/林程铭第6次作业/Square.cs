﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    
    class Square:Shape
    {
        protected int sideLen;
        public Square(int sideLen) 
        {
            this.sideLen = sideLen; 
        }
        public override void GetArea()
        {
            Console.WriteLine("正方形面积：{0}",sideLen*sideLen);
        }

    }
}
