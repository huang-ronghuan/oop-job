﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请用户输入一个数：");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("请用户再输入一个数：");
            int num2 = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入一个操作符：");
            string make = Console.ReadLine();

            switch (make)
            {
                case "+":
                    Add add = new Add(num1,num2);
                    add.DisplayResult();
                    break;
                case "-":
                    Delete delete = new Delete(num1, num2);
                    delete.DisplayResult();
                    break;
                case "*":
                    Ride ride = new Ride(num1, num2);
                    ride.DisplayResult();
                    break;
                case "/":
                    Except except = new Except(num1, num2);
                    except.DisplayResult();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}
