﻿using System;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入半径值：");
            int radius = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入边长值：");
            int sideLen = int.Parse(Console.ReadLine());
            Console.WriteLine("输入1为计算圆面积，输入2为计算正方形面积");
            int make = int.Parse(Console.ReadLine());
            switch (make)
            {
                case 1:
                    Circle circle = new Circle(radius);
                    circle.GetArea();
                    break;
                case 2:
                    Square square = new Square(sideLen);
                    square.GetArea();
                    break;
                default:
                    break;
            }
            Console.ReadKey();
        }
    }
}
