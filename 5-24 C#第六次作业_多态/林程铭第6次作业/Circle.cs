﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Circle:Shape
    {
        protected int radius;
        public Circle(int radius) 
        {
            this.radius = radius;
        }
        public override void GetArea()
        {
            Console.WriteLine("圆形面积：{0}",3.14*radius*radius);
        }

    }
}
