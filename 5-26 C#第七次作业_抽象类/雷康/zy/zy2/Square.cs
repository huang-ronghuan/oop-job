﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Square:Shape
    {
        private double sideLen;

        public double SideLen { get => sideLen; set => sideLen = value; }
        public Square(double sideLen)
        {
            
            this.sideLen = sideLen;
        }
        public Square()
        {

        }

        public override void Getarea()
        {
            Console.WriteLine("面积为："+(sideLen*sideLen));
        }
    }
}
