﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Businessmannpc : Allnpc
    {
        public Businessmannpc(string name, string type) : base(name, type)
        {
            this.Name = name;
            this.Type = type;
        }

        public override void print()
        {
            Console.WriteLine("请输入选择。1.购买。2.售出。");
            int i = int.Parse(Console.ReadLine());

            if (i==1) 
            {
                Console.WriteLine("请输入要购买的东西。1.***。2.***。3****。");
                int p = int.Parse(Console.ReadLine());
                if (p==1||p==2||p==3)
                {
                    Console.WriteLine("购买成功");
                }
            }
            if (i==2)
            {
                Console.WriteLine("请输入要售出的东西。1.***.2.****。3.****。");
                int p = int.Parse(Console.ReadLine());
                if (p == 1 || p == 2 || p == 3)
                {
                    Console.WriteLine("售出成功");
                }
            }
        }
    }
}
