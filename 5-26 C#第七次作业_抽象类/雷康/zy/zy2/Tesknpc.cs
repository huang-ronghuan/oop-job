﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Tesknpc:Allnpc
    {
        public Tesknpc(string name,string type):base(name,type)
        {
            this.Name = name;
            this.Type = type;
        }

        public override void print()
        {
            Console.WriteLine("请输入。1.领取任务。2.结算任务。");
            int i = int.Parse(Console.ReadLine());
            switch (i)
            {
                case 1:
                    Console.WriteLine("请输入要领取的任务。1.****。2.*****。3.*****");
                    int p = int.Parse(Console.ReadLine());
                    if (p == 1 || p == 2 || p == 3)
                    {
                        Console.WriteLine("领取成功");
                    }
                    else
                    {
                        Console.WriteLine("输入错误，领取失败");
                    }
                    break;
                case 2:
                    Console.WriteLine("结算成功，奖励已领取");
                    break;
                default:
                    Console.WriteLine("输入错误");
                    break;
            }
        }
    }
}
