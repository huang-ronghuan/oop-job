﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Shape
    {
        private string color;

        public string Color { get => color; set => color = value; }

        public Shape(string color)
        {
            this.color = color;
        }
        public Shape()
        {

        }

        public virtual void Getarea()
        {

        }
    }
}
