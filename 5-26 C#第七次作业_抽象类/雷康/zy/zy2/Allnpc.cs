﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Allnpc
    {
        private string name;
        private string type;

        public string Name { get => name; set => name = value; }
        public string Type { get => type; set => type = value; }

        public Allnpc(string name, string type)
        {
            this.name = name;
            this.type = type;
        }

        public virtual void print()
        {

        }
    }
}
