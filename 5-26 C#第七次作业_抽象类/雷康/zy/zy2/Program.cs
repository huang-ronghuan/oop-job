﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Program
    {
        
//2.使用抽象类结构实现游戏中NPC 模块
//在游戏中会出现很多种不同用途的 NPC，这些 NPC 有各自的存在的价值和作用，同时又具备一些共性的东西。
//在开发 NPC 系统的时候，往往是需要提取共性，独立出一个父类，然后子类继承实现不同作用的 NPC。
//分析：任务 NPC，商贩 NPC，铁匠 NPC，三种 NPC 的种类。
//共有属性：npc 的名字，npc 的类型；
//共有方法：都能和玩家交互(交谈)；
        static void Main(string[] args)
        {
            bool a = true;
            while (a) {
                Console.WriteLine("请输入要选择的npc。1.任务 NPC。2.商贩 NPC。3.铁匠 NPC。4.退出系统");
                int i = int.Parse(Console.ReadLine());
                Console.WriteLine("请输入npc的名称");
                string n = Console.ReadLine(); 
                Console.WriteLine("请输入npc的类型");
                string m = Console.ReadLine();

                switch (i)
                {
                    case 1:
                        Tesknpc tesknpc = new Tesknpc(n,m);
                        tesknpc.print();
                        break;
                    case 2:
                        Businessmannpc businessmannpc = new Businessmannpc(n,m);
                        break;
                    case 3:
                        Blacksmithnpc blacksmithnpc = new Blacksmithnpc(n,m);
                        break;
                    case 4:
                        a=false;
                        break;
                    default:
                        Console.WriteLine("输入错误");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
