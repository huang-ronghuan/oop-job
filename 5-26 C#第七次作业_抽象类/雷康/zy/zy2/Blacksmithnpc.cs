﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Blacksmithnpc : Allnpc
    {
        public Blacksmithnpc(string name, string type) : base(name, type)
        {
            this.Name = name;
            this.Type = type;
        }

        public override void print()
        {
            Console.WriteLine("请输入要打造的物品。1.***.2.***。3.****。");
            int i = int.Parse(Console.ReadLine());
            if (i==1||i==2||i==3)
            {
                Console.WriteLine("打造成功");
            }
        }
    }
}
