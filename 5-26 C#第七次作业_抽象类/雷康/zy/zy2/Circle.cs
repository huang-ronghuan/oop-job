﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy2
{
    class Circle:Shape
    {
        private double radius;

        public double Radius { get => radius; set => radius = value; }
        public Circle(double radius)
        {
            
            this.radius = radius;

        }
        public Circle()
        {

        }

        public override void Getarea()
        {
            Console.WriteLine("面积为;"+(3.14*radius*radius));
        }
    }
}
