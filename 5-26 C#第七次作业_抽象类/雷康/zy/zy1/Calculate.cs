﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy1
{
    class Calculate
    {
        //1、创建Calculate基类，其中包含两个整型的protected成员，用以接收用户输入的两个整数。
        //定义一个DisplayResult()虚方法，计算并输出结果。
        protected int n;
        protected int m;

        public Calculate(int n, int m)
        {
            this.n = n;
            this.m = m;
        }
        public virtual void DisplayResult()
        {

        }
    }
}
