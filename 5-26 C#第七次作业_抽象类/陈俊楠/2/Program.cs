﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请少侠选择您要进行的类型：");
            Console.WriteLine("1.任务NPC 2.商贩NPC 3.铁匠NPC");
            int num = int.Parse(Console.ReadLine());
            switch (num)
            {
                case 1:
                    NPCdady task = new Task();
                    Console.WriteLine("\n进入任务NPC页面");
                    Console.WriteLine("A.送信NPC \n B.杀怪NPC \n C.采集NPC ");
                    Console.WriteLine("\n发任务玩家：请选择您要的选项：");
                    string num1 = Console.ReadLine();
                    if (num1 == "A")
                    {
                        Console.WriteLine("\n您已接受送信NPC,以下是本次任务内容：");
                        Console.WriteLine("送信NPC:给别人送一封信，贼难的\n，九九八十一关才能到的");
                    }
                    else if (num1 == "B")
                    {
                        Console.WriteLine("\n您已接受杀怪NPC");
                        Console.WriteLine("杀怪NPC:这次任务不仅可以锻炼你，还能升级和赚钱");
                    }
                    else if (num1 == "C")
                    {
                        Console.WriteLine("\n您已接受采集NPC");
                        Console.WriteLine("采集NPC:帮我收集一部分的灵虚草，必有重谢");
                    }
                    break;
                case 2:
                    NPCdady iron = new Iron();
                    Console.WriteLine("\n进入商贩NPC页面");
                    Console.WriteLine("A.武器NPC \n B.材料NPC \n C.食物NPC ");
                    Console.WriteLine("\n商铺外保镖：少侠请选择您要的选项：");
                    string num2 = Console.ReadLine();
                    if (num2 == "A")
                    {
                        Console.WriteLine("\n您已接受武器NPC,以下是我们的武器类别：");
                        Console.WriteLine("A.剑灵 ￥288\nB.风煞 ￥384\nC.饕餮￥343 ");
                        Console.WriteLine("\n商贩1：少侠想要哪个就快点买下它吧！");
                        string a = Console.ReadLine();
                        if (a == "A")
                        {
                            Console.WriteLine("\n少侠：我就要这个剑灵吧！快拿给我试试");
                        }
                        else if (a == "B")
                        {
                            Console.WriteLine("\n少侠：我就要这个风煞吧！快拿给我试试");
                        }
                        else if (a == "C")
                        {
                            Console.WriteLine("\n少侠：我就要这个饕餮吧！快拿给我试试");
                        }
                    }
                    else if (num2 == "B")
                    {
                        Console.WriteLine("\n您已接受材料NPC,看看我们小店的材料吧！");
                        Console.WriteLine("A.灵石￥34 B.木材￥50 C.魔核￥300 D.彩金￥400 E.龙血￥8888");
                        Console.WriteLine("\n商贩2：少侠想要哪个就快点买下它吧！");
                        string b = Console.ReadLine();
                        if (b == "A")
                        {
                            Console.WriteLine("\n少侠：我就要这个灵石吧！\n就是不知道你这灵石真的还是假的");
                        }
                        else if (b == "B")
                        {
                            Console.WriteLine("\n少侠：快把木材抬上来！");
                        }
                        else if (b == "C")
                        {
                            Console.WriteLine("\n少侠：买个魔核练练手！快拿给我看看");
                        }
                        if (b == "D")
                        {
                            Console.WriteLine("\n少侠：我就要这个彩金吧！");
                        }
                        else if (b == "E")
                        {
                            Console.WriteLine("\n少侠：那这个龙血来！\n那么贵的东西有什么不同的，\n让我瞧瞧");
                        }
                    }
                    else if (num2 == "C")
                    {
                        Console.WriteLine("\n您已接受食物NPC ,开看看本店的新进食物吧\n保你在野外不受饥饿的困扰");
                        Console.WriteLine("A.大馒头￥34 B.压缩饼干￥50 C.可可豆粉￥99 D.麦丽素￥100 E.可热餐￥888");
                        Console.WriteLine("\n商贩3：少侠想要哪个就快点买下它吧，别饿死在野外！");
                        string c = Console.ReadLine();
                        if (c == "A")
                        {
                            Console.WriteLine("\n少侠：快给我拿100个大馒头！\n就是不知道你这馒头好不好吃");
                        }
                        else if (c == "B")
                        {
                            Console.WriteLine("\n少侠：压缩饼干给我来一打！");
                        }
                        else if (c == "C")
                        {
                            Console.WriteLine("\n少侠：我要在野外泡咖啡，快给我来几包。");
                        }
                        if (c == "D")
                        {
                            Console.WriteLine("\n少侠：麦丽素不可以没有，快给我拿五盒来。");
                        }
                        else if (c == "E")
                        {
                            Console.WriteLine("\n少侠：这个可热餐那么贵的东西\n有什么不同的，\n让我瞧瞧");
                        }
                    }
                    break;
                case 3:
                    NPCdady shop = new Shop();
                    Console.WriteLine("进入铁匠NPC页面");
                    Console.WriteLine("A.修补NPC \n B.强化NPC \n C.打造NPC ");
                    break;
                default:
                    break;
            }


            Console.ReadKey();
        }
    }
}
