﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp16
{
    
    //创建抽象类 为家电功率

   abstract class ElectricEquipment
    {
        protected int electricEquipment; //功率
        protected ElectricEquipment(int power) 
        {
            electricEquipment = power;
        }

        //创建电器工作抽象方法
        public abstract void Working();
   }
    class AirCond : ElectricEquipment
    {
        public AirCond(int power) : base(power)
        {
        }

        public override void Working()
        {
            Console.WriteLine("空调在功率"+this.electricEquipment+"的情况下也可以制热。");
        }
    }
}
