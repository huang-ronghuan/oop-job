﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入您选择的机器人：炒菜、传菜");
            string get = Console.ReadLine();
            Choose(get);
        }
        public static void Choose(string get)
        {
            switch (get)
            {
                case "炒菜":
                    CookRobot cook = new CookRobot();
                    cook.Working();
                    break;
                case "传菜":
                    DeliveryRobot delivery = new DeliveryRobot();
                    delivery.Working();
                    break;
                default:
                    Console.WriteLine("输入有误");
                    break;
            }
        }

    }
}
