﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入需要咨询哪种机器人：（炒菜or传菜）");
            string robot1 = Console.ReadLine();
            switch (robot1)
            {
                case "炒菜":
                    Robot cookrobot = new CookRobot();
                    Veg veg1 = Veg.酸辣土豆丝;
                    Veg veg2 = Veg.鸡汤;
                    Console.WriteLine(veg1);
                    Console.WriteLine(veg2);
                    
                    cookrobot.name();
                    cookrobot.Working();
                    break;
                case "传菜":
                    Robot deliveryrobot = new DeliveryRobot();
                    Delivery del = Delivery.每天可工作10小时;
                    Console.WriteLine(del);
                    deliveryrobot.name();
                    deliveryrobot.Working();
                    break;
                default:
                    break;
            }

            Console.ReadKey();
            
        }
    }
}
