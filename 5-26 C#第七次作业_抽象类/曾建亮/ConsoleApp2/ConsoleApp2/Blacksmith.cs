﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{

    class Blacksmith : NPC
    {
        public override void Name()
        {
            Console.WriteLine("你好冒险家，我的名字是斌斌。");
        }

        public override void Type()
        {
            Console.WriteLine("我是铁匠，你可以找我造东西或修东西。");
        }
    }
}
