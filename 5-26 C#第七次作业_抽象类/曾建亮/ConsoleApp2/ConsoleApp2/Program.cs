﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        //2.使用抽象类结构实现游戏中NPC 模块
        //在游戏中会出现很多种不同用途的 NPC，这些 NPC 有各自的存在的价值和作用，
        //同时又具备一些共性的东西。在开发 NPC 系统的时候，往往是需要提取共性，独立出一个父类，然后子类继承实现不同作用的 NPC。
        //分析：任务 NPC，商贩 NPC，铁匠 NPC，三种 NPC 的种类。
        //共有属性：npc 的名字，npc 的类型;
        //共有方法：都能和玩家交互(交谈)；

        static void Main(string[] args)
        {
            Console.WriteLine("请选择和那个npc对话 任务NPC，商贩NPC，铁匠NPC");
            int n = int.Parse(Console.ReadLine());
            switch (n)
            {
                case 1:
                    Task task = new Task();
                    a(task);
                    Console.WriteLine("请选择以下任务：1、简单。 2、中等。 3、困难。 4、退出。");
                    int i1 = int.Parse(Console.ReadLine());
                    if (i1==1)
                    {
                        Console.WriteLine("年轻的冒险家啊！请你去森林里采集十个蘑菇，你将得到神奇的功法。");
                    }
                    if (i1==2)
                    {
                        Console.WriteLine("年轻的冒险家啊！请你去森林里杀死一只野猪王将它的内丹给我，你将得到少女的衣服和一把匕首。");
                    }
                    if (i1==3)
                    {
                        Console.WriteLine("年轻的冒险家啊！听说森林里有两位少女苏苏和莹莹被人抓走了，将少女们带回，你将得到少女们的情书。注：情书有白黑两种，黑情书她们喜欢你但她们更喜欢死去的你！！！");
                    }
                    if (i1==4)
                    {
                        Console.WriteLine("年轻的冒险家再见！");
                    }
                    break;
                case 2:
                    Pedlar pedlar = new Pedlar();
                    a(pedlar);
                    Console.WriteLine("请选择以下 1、买东西。 2、卖东西。 3、退出。");
                    int i2 = int.Parse(Console.ReadLine());
                    if (i2==1)
                    {
                        Console.WriteLine("年轻的冒险家啊!本店长应女儿不在，本店暂无开放！");
                    }
                    if (i2==2)
                    {
                        Console.WriteLine("年轻的冒险家啊!本店长应女儿不在，本店暂无开放！");
                    }
                    if (i2==3)
                    {
                        Console.WriteLine("年轻的冒险家啊!再见");
                    }
                    break;
                case 3:
                    Blacksmith blacksmith = new Blacksmith();
                    a(blacksmith);
                    Console.WriteLine("请选择以下 1、造东西。 2、修东西。 3、退出。");
                    int i3 = int.Parse(Console.ReadLine());
                    if (i3 == 1)
                    {
                        Console.WriteLine("年轻的冒险家啊!本店长应女儿不在，本店暂无开放！");
                    }
                    if (i3 == 2)
                    {
                        Console.WriteLine("年轻的冒险家啊!本店长应女儿不在，本店暂无开放！");
                    }
                    if (i3 == 3)
                    {
                        Console.WriteLine("年轻的冒险家啊!再见");
                    }
                    break;
                default:
                    Console.WriteLine("错误");
                    break;
            }
            Console.ReadKey();
        }

        private static void a(NPC npc)
        {
            npc.Name();
            npc.Type();
        }
    }
}
