﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Pedlar : NPC
    {
        public override void Name()
        {
            Console.WriteLine("你好冒险家，我的名字是彬彬。");
        }

        public override void Type()
        {
            Console.WriteLine("我是商贩，你可以找我买东西或卖东西。");
        }
    }
}
