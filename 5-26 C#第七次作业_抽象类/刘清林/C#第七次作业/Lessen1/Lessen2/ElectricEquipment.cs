﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lessen2
{
    abstract class ElectricEquipment
    {
        protected int J; 
        protected ElectricEquipment(int power)
        {
            J = power;
        }
        public abstract void Working();
    }
    class AirCond : ElectricEquipment
    {
        public AirCond(int power) : base(power)
        {
        }

        public override void Working()
        {
            Console.WriteLine("空调在功率" + this.J + "的情况下也可以制热。");
        }
    }
}
