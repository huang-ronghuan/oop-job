﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Demo1.aspx.cs" Inherits="WebApplication1.Demo1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名：&nbsp;
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="用户名不能为空" ControlToValidate="TextBox1" Text="*"></asp:RequiredFieldValidator>
            <br />
            密 码： &nbsp;
            <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="密码不能为空" ControlToValidate="TextBox2" ></asp:RequiredFieldValidator>
            <br />
            确认密码：
            <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="确认密码不能为空" ControlToValidate="TextBox3"></asp:RequiredFieldValidator>
            <br />
            性别：
            <asp:TextBox ID="TextBox5" runat="server" ></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="男或女"  ValidationExpression="[男,女]" ControlToValidate="TextBox5"></asp:RegularExpressionValidator>
            <br />
            <%--<asp:CustomValidator ID="CustomValidator1" runat="server" ErrorMessage="性别只能是男或女" OnServerValidate="CustomValidator1_ServerValidate" ControlToValidate="TextBox5"></asp:CustomValidator>
            <br />--%>
            年龄：
            <asp:TextBox ID="TextBox6" runat="server"></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="年龄必须在10-100" ControlToValidate="TextBox6" MinimumValue="10" MaximumValue="100"></asp:RangeValidator>
            <br />
            邮箱：
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="邮箱格式不正确"  ForeColor="Red" ControlToValidate="TextBox4" ValidationExpression="^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$" ></asp:RegularExpressionValidator>
            <br />
            详细地址：
            <asp:TextBox ID="TextBox8" runat="server" TextMode="MultiLine"></asp:TextBox>
            <br />
            <asp:ValidationSummary ID="ValidationSummary1" runat="server"  ShowMessageBox="true" />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ErrorMessage="两次密码不一致" ControlToValidate="TextBox3"  ControlToCompare="TextBox2" ></asp:CompareValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="登录" />
            <asp:Button ID="Button2" runat="server" Text="重置"  CausesValidation="false"  OnClick="Button2_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text="管理员提示"></asp:Label>
            <br />
            <asp:TextBox ID="TextBox7" runat="server" Text="用户须知：我们将保护您的隐私并保证您提供的个人资料的保密性。我们收集的个人资料仅用于为您。。。。" TextMode="MultiLine" Height="90px" Width="243px">用户须知：我们将保护您的隐私并保证您提供的个人资料的保密性。我们收集的个人资料仅用于为您。。。。</asp:TextBox>
        </div>
    </form>
</body>
</html>
