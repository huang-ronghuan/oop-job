﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="SWK.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
     <style type="text/css">
        .auto-style1 {
            width: 64px;
        }
        .auto-style2 {
            width: 64px;
            height: 181px;
        }
        .auto-style4 {
            width: 67%;
            height: 599px;
            margin-right: 0px;
        }
        .auto-style5 {
            height: 149px;
            margin-left: 80px;
        }
        .auto-style6 {
            width: 64px;
            height: 30px;
        }
        .auto-style7 {
            height: 30px;
        }
        .auto-style8 {
            width: 64px;
            height: 31px;
        }
        .auto-style11 {
            height: 26px;
        }
        .auto-style13 {
            width: 301px;
            height: 31px;
        }
        .auto-style14 {
            width: 301px;
            height: 181px;
        }
        .auto-style15 {
            height: 30px;
            width: 301px;
        }
        .auto-style16 {
            width: 301px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <table class="auto-style4">
                <tr>
                    <td class="auto-style5" colspan="2" >
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label1" runat="server" ClientIDMode="Static" Text="顾客信息登记" algin="center"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style6">姓名：</td>
                    <td class="auto-style15">
                        <asp:TextBox ID="TextBox1" runat="server" Width="227px" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style8">年龄：</td>
                    <td class="auto-style13">
                        <asp:TextBox ID="TextBox2" runat="server" Width="227px" OnTextChanged="TextBox2_TextChanged"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style2">爱好</td>
                    <td class="auto-style14">
                        <asp:TextBox ID="TextBox3" runat="server" Height="158px" OnTextChanged="TextBox3_TextChanged" TextMode="MultiLine" Width="563px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="2">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button1" runat="server" algin="right" Text="提交" OnClick="Button1_Click" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style7" colspan="2">
                        <asp:TextBox ID="TextBox5" runat="server" BorderStyle="None" Height="21px" OnTextChanged="TextBox5_TextChanged" Width="642px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style16">
                        <asp:TextBox ID="TextBox4" runat="server" BorderStyle="None" Height="152px" OnTextChanged="TextBox4_TextChanged" Width="446px"></asp:TextBox>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
