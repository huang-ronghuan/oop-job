﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Class1 {
        // {2. 定义一个学生类，存放学生的学号、姓名、性别、年龄、专业信息；
        //对年龄字段进行赋值的安全性设置，如果是非法值（小于0或者大于128岁），该年龄值为0；
        //在学生类中定义一个方法输出学生信息。
        //在主方法实例化对象，赋值并输出
        public int stuID;
        public string name;
        public string sex;
        private int age;
        public string major;
        public void Student1() {
            Console.WriteLine("学号" + stuID);
            Console.WriteLine("姓名" + name);
            Console.WriteLine("性别" + sex);
            Console.WriteLine("年龄" + age);
            Console.WriteLine("专业信息" + major);

        }
        public int Age
        {
            get
            {
                return Age;
            }
            set
            {
                if (value> 0 && value < 128)
                {
                    Age = value;
                }else
                    { Age = 0; }
            }
        }
        public static void Main(string[]args) {
            Class1 stu1 = new Class1();
            stu1.stuID = 12;
            stu1.name = "张三";
            stu1.sex = "男";
            stu1.Age = 19;
            stu1.major = "软件工程";
            stu1.Student1();
            Console.ReadKey();
        }
    }
   

    }
    

   

    

