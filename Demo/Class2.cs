﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Class2 
    {
        //  {3.定义一个图书类，存放图书的编号、书名、价格、出版社、作者信息；
        //对价格进行赋值限制，小于0价格，赋值为0
        //在图书类中定义一个方法输出图书信息；
        //在主方法实例化对象，赋值并输出
      
        public int bookID;
        public string bookName;
        private double bookPrice;
        public string press;
        public string author;
        public void book1() {
            Console.WriteLine("编号"+bookID);
            Console.WriteLine("书名"+bookName);
            Console.WriteLine("价格"+bookPrice);
            Console.WriteLine("出版社"+press);
            Console.WriteLine("作者信息"+author);
        }
        public double BookPrice {
            get
            {
                return BookPrice;
            }
            set
            {
                if (value < 0 )
                {
                    BookPrice = value;
                }
                else
                { BookPrice = 0; }
            }
        }
        public static void Main(string []args) {
            Class2 book = new Class2();
            book.bookID = 15;
            book.bookName="老人与海";
            book.BookPrice = 15.9;
            book.press = "天桥出版社";
            book.author = "海明威";
            book.book1();
            Console.ReadKey();
        }
}
        
}
