﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//1、有一款叫做“切水果”的游戏，里面有各种各样的水果，
//A、现在请定义一个叫做“Fruit”的枚举类型，里面有：葡萄、草莓、奇异果、苹果、香蕉、哈密瓜、西瓜。
//B、然后定义一个输出水果分数的方法，参数类型就是这个水果枚举，方法中根据不同的枚举值，输出水果对应的分数（可以参考切水果游戏）。
//   注：水果形状越小，切到的时候分数就越高。
//C、在主方法里面采用循环实现：随机生成10个数(0~6)，代表水果枚举值（可以将整数强制转换为枚举类型值）；
//   然后调用输出水果分数的方法，将该随机水果枚举值作为参数
namespace Demo1
{
    class Program
    {
        public enum Fruit
        {
            葡萄,
            草莓,
            奇异果,
            苹果,
            香蕉,
            哈密瓜,
            西瓜
        }        
        static void Main(string[] args)
        {
            Random rd = new Random();
            Fruit fruit;
            int sum = 0;
            for (int i = 0; i < 10; i++)
            {
                int num = rd.Next(0, 7);
                fruit = (Fruit)num;
                int a =grade(fruit);
                sum += a;
                Console.WriteLine("当前得分为："+sum);
            }
            Console.WriteLine("按任意键结束程序...");
            Console.ReadKey();
        }
        public static int grade(Fruit fruit)
        {
            int a = 0;
            switch (fruit)
            {
                case (Fruit)0:
                    Console.WriteLine("切除葡萄，获得10分！");
                    a = 10;
                    break;
                case (Fruit)1:
                    Console.WriteLine("切除草莓，获得8分！");
                    a = 8;
                    break;
                case (Fruit)2:
                    Console.WriteLine("切除奇异果，获得6分！");
                    a = 6;
                    break;
                case (Fruit)3:
                    Console.WriteLine("切除苹果，获得5分！");
                    a = 5;
                    break;
                case (Fruit)4:
                    Console.WriteLine("切除香蕉，获得3分！");
                    a = 3;
                    break;
                case (Fruit)5:
                    Console.WriteLine("切除哈密瓜，获得2分！");
                    a = 2;
                    break;
                case (Fruit)6:
                    Console.WriteLine("切除西瓜，获得1分！");
                    a = 1;
                    break;
                default:
                    Console.WriteLine("切除失败！");
                    break;
            }
            return a;
        }
    }
}
