﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    enum Fruit
    {
        葡萄,
        草莓,
        奇异果,
        苹果,
        香蕉,
        哈密瓜,
        西瓜
    }
    class Program
    {

        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 20; i++)
            {
                int num = random.Next(0, 7);
                fruit((Fruit)num);

            }
            Console.ReadKey();
        }


        public static void fruit(Fruit fruit)
        {
            switch (fruit)
            {
                case Fruit.葡萄:
                    Console.WriteLine(fruit + "18分");
                    break;
                case Fruit.草莓:
                    Console.WriteLine(fruit + "20分");
                    break;
                case Fruit.奇异果:
                    Console.WriteLine(fruit + "16分");
                    break;
                case Fruit.苹果:
                    Console.WriteLine(fruit + "10分");
                    break;
                case Fruit.香蕉:
                    Console.WriteLine(fruit + "15分");
                    break;
                case Fruit.哈密瓜:
                    Console.WriteLine(fruit + "4分");
                    break;
                case Fruit.西瓜:
                    Console.WriteLine(fruit + "2分");
                    break;
                default:
                    Console.WriteLine("未切中+0分");
                    break;
            }
        }
    }
}
