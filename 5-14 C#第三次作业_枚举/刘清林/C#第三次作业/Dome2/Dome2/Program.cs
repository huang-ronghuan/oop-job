﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    enum RPG
    {
        战士, 法师, 精灵
    }
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 5; i++)
            {
                int num = random.Next(0, 3);
                rPG((RPG)num);
            }
            Console.ReadKey();
        }
        static void rPG(RPG rPG)
        {
            switch (rPG)
            {
                case RPG.战士:
                    Console.WriteLine(rPG + "：碎石打击、烈焰锚钩、战斗咆哮");
                    break;
                case RPG.法师:
                    Console.WriteLine(rPG + "：巨浪冲击、元素突击、复仇杀戮");
                    break;
                case RPG.精灵:
                    Console.WriteLine(rPG + "：减速陷阱、能量浪潮、旋风剑舞");
                    break;
                default:
                    break;
            }
        }
    }
}
