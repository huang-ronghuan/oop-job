﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo
{
    class Program
    {
        enum num {
            星期一,
            星期二,
            星期三,
            星期四,
            星期五,
            星期六,
            星期日
        }


        static void Main(string[] args)
        {
            num a = num.星期一;
            num b = num.星期二;
            num c = num.星期三;
            num d = num.星期四;
            num g = num.星期五;
            num h = num.星期六;
            num f = num.星期日;

            Console.WriteLine(num.星期一 + (int)a);
            Console.WriteLine(num.星期二 + (int)b);
            Console.WriteLine(num.星期三 + (int)c);
            Console.WriteLine(num.星期四 + (int)d);
            Console.WriteLine(num.星期五 + (int)g);
            Console.WriteLine(num.星期六 + (int)h);
            Console.WriteLine(num.星期日 + (int)f);

            Console.ReadKey();


        }
    }
}
