﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        enum Fruit {
            葡萄,草莓,奇异果,苹果,香蕉,哈密瓜,西瓜
        }
        static void Main(string[] args)
        {    
                fruit();
        }

        public static void fruit() {
            Console.WriteLine("相关水果：葡萄,草莓,奇异果,苹果,香蕉,哈密瓜,西瓜");
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                int num = random.Next(0, 7);
                switch ((Fruit)num)
                {
                    case Fruit.葡萄:
                        Console.WriteLine("切葡萄得10分");
                        break;
                    case Fruit.草莓:
                        Console.WriteLine("切草莓得10分");
                        break;
                    case Fruit.奇异果:
                        Console.WriteLine("切奇异果得10分");
                        break;
                    case Fruit.苹果:
                        Console.WriteLine("切苹果得10分");
                        break;
                    case Fruit.香蕉:
                        Console.WriteLine("切香蕉得10分");
                        break;
                    case Fruit.哈密瓜:
                        Console.WriteLine("切哈密瓜得10分");
                        break;
                    case Fruit.西瓜:
                        Console.WriteLine("切西瓜得10分");
                        break;
                    default:
                        break;
                }

            }
            Console.ReadKey();
           

        }
    }
}
