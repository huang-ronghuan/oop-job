﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome2
{
    class Program
    {
        public enum RPG
        {
            战士,法师,精灵
        }
        public enum Skill
        {
            碎石打击,烈焰锚钩, 战斗咆哮, 巨浪冲击, 元素突击, 复仇杀戮, 减速陷阱, 能量浪潮, 旋风剑舞
        }
        static void a(RPG rPG)
        {
            switch (rPG)
            {
                case RPG.战士:
                    
                    Console.WriteLine("战士的技能："+Skill.碎石打击,Skill.烈焰锚钩,Skill.战斗咆哮);
                    break;
                case RPG.法师:
                    Console.WriteLine("法师的技能："+Skill.巨浪冲击,Skill.元素突击,Skill.复仇杀戮);
                    break;
                case RPG.精灵:
                    Console.WriteLine("精灵的技能："+Skill.减速陷阱,Skill.能量浪潮,Skill.旋风剑舞);
                    break;
                default:
                    break;
            }
        }
        static void Main(string[] args)
        {
            RPG b= RPG.战士;
            RPG c = RPG.法师;
            RPG d = RPG.精灵;
            Console.WriteLine("职业："+b+" "+"技能:"+ Skill.碎石打击+ " "+Skill.烈焰锚钩+ " "+Skill.战斗咆哮);
            Console.WriteLine("职业："+c+" "+"技能:"+ Skill.巨浪冲击+" "+ Skill.元素突击+" "+Skill.复仇杀戮);
            Console.WriteLine("职业：" + d + " "+"技能:" + Skill.减速陷阱 +" "+ Skill.能量浪潮+" "+ Skill.旋风剑舞);

        }
    }
}
