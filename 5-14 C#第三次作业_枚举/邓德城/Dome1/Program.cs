﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Program
    {
        public enum Fruit
        {
            葡萄,草莓,奇异果,苹果,香蕉,哈密瓜,西瓜
        }
        static void a(Fruit fruit)
        {
            switch (fruit)
            {
                case Fruit.葡萄:
                    Console.WriteLine("葡萄"+20);
                    break;
                case Fruit.草莓:
                    Console.WriteLine("草莓" + 18);
                    break;
                case Fruit.奇异果:
                    Console.WriteLine("奇异果" + 16);
                    break;
                case Fruit.苹果:
                    Console.WriteLine("苹果" + 14);
                    break;
                case Fruit.香蕉:
                    Console.WriteLine("香蕉" + 12);
                    break;
                case Fruit.哈密瓜:
                    Console.WriteLine("哈密瓜" + 8);
                    break;
                case Fruit.西瓜:
                    Console.WriteLine("西瓜" + 5);
                    break;
                default:
                    break;
            }
        }
        static void Main(string[] args)
        {
            Fruit a = (Fruit)20;
            Fruit b = (Fruit)18;
            Fruit c = (Fruit)16;
            Fruit d = (Fruit)14;
            Fruit e = (Fruit)12;
            Fruit f = (Fruit)8;
            Fruit g = (Fruit)5;
            Random sum = new Random();
            for (int i = 0; i < 7; i++)
            {
                
                int num = sum.Next(0, 7);

                if (num == 0)
                {
                    Console.WriteLine("葡萄=" + 20);
                }
                if (num == 1)
                {
                    Console.WriteLine("草莓=" + 18);
                }
                if (num == 2)
                {
                    Console.WriteLine("奇异果=" + 16);
                }
                if (num == 3)
                {
                    Console.WriteLine("苹果=" + 14);
                }
                if (num == 4)
                {
                    Console.WriteLine("香蕉=" + 12);
                }
                if (num == 5)
                {
                    Console.WriteLine("哈密瓜=" + 8);
                }
                else
                {
                    Console.WriteLine("西瓜=" + 5);
                }
            }

        }
    }
}
