﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        enum MyEnum
        {
            战士,
            法师,
            精灵
        }
        static void Main(string[] args)
        {
            //2、RPG游戏中，通常有不同的职业，比如“战士”、“法师”、“精灵”等等职业，
            //A、现在请定义一个游戏职业的枚举。
            //B、然后定一个输出职业技能的方法，根据传入的职业枚举的值来输出，
            //战士的技能：碎石打击、烈焰锚钩、战斗咆哮
            //法师的技能：巨浪冲击、元素突击、复仇杀戮
            //精灵的技能：减速陷阱、能量浪潮、旋风剑舞
            MyEnum a1 = MyEnum.战士;
            MyEnum a2 = MyEnum.法师;
            MyEnum a3 = MyEnum.精灵;
            Console.WriteLine("请输入游戏职业0（战士），1（法师），2（精灵）");
            int n = int.Parse(Console.ReadLine());
            A((MyEnum)n);

            Console.ReadKey();
        }

        private static void A(MyEnum n)
        {
                switch (n)
                {
                    case MyEnum.战士:
                        Console.WriteLine("战士的技能：碎石打击、烈焰锚钩、战斗咆哮");
                        break;
                    case MyEnum.法师:
                        Console.WriteLine("法师的技能：巨浪冲击、元素突击、复仇杀戮");
                        break;
                    case MyEnum.精灵:
                        Console.WriteLine("精灵的技能：减速陷阱、能量浪潮、旋风剑舞s");
                        break;
                    default:
                        Console.WriteLine("无");
                        break;
                }
            }
        }
    }
