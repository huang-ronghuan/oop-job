﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    enum Fruit
    {
            葡萄=7,
            草莓=6,
            奇异果=5,
            苹果=4,
            香蕉=3,
            哈密瓜=2,
            西瓜=1
    }
    class Program
    {
        static void Main(string[] args)
        {

            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                int num = random.Next(0,7);
                a((Fruit)num);
            }
            
            Console.ReadKey();
        }
        public static void a(Fruit fruit) {

                switch (fruit)
                {
                    case Fruit.葡萄:
                        Console.WriteLine(fruit + "7分");
                       
                        break;
                    case Fruit.草莓:
                        Console.WriteLine(fruit + "6分");
                 
                        break;
                    case Fruit.奇异果:
                        Console.WriteLine(fruit + "5分");
                
                        break;
                    case Fruit.苹果:
                        Console.WriteLine(fruit + "4分");
                 
                        break;
                    case Fruit.香蕉:
                        Console.WriteLine(fruit + "3分");
               
                        break;
                    case Fruit.哈密瓜:
                        Console.WriteLine(fruit + "2分");
               
                        break;
                    case Fruit.西瓜:
                        Console.WriteLine(fruit + "1分");
              
                        break;
                    default:
                        Console.WriteLine("不得分");
                   
                        break;
                }
          
    
        }
    }
}
//1、有一款叫做“切水果”的游戏，里面有各种各样的水果，
//A、现在请定义一个叫做“Fruit”的枚举类型，里面有：葡萄、草莓、奇异果、苹果、香蕉、哈密瓜、西瓜。
//B、然后定义一个输出水果分数的方法，参数类型就是这个水果枚举，方法中根据不同的枚举值，输出水果对应的分数（可以参考切水果游戏）。
//注：水果形状越小，切到的时候分数就越高。
//C、在主方法里面采用循环实现：随机生成10个数(0~6)，代表水果枚举值（可以将整数强制转换为枚举类型值）；
//然后调用输出水果分数的方法，将该随机水果枚举值作为参数
