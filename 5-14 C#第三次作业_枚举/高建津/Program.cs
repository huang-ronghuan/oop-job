﻿using System;


namespace ConsoleApp2
{
    enum Hoby
    {
        战士,
        法师,
        精灵,
    }
    class Program
    {
        //2、RPG游戏中，通常有不同的职业，比如“战士”、“法师”、“精灵”等等职业，
        //A、现在请定义一个游戏职业的枚举。
        //B、然后定一个输出职业技能的方法，根据传入的职业枚举的值来输出，
        //战士的技能：碎石打击、烈焰锚钩、战斗咆哮
        //法师的技能：巨浪冲击、元素突击、复仇杀戮
        //精灵的技能：减速陷阱、能量浪潮、旋风剑舞
        static void Main(string[] args)
        {
            a();
        }

        public static void a()
        {
            Console.WriteLine("1,战士 2,法师 3,精灵");
            int b= int.Parse(Console.ReadLine());
            switch ((Hoby)b - 1)
            {
                case Hoby.战士:
                    Console.WriteLine("战士的技能：碎石打击、烈焰锚钩、战斗咆哮");
                    break;
                case Hoby.法师:
                    Console.WriteLine("法师的技能：巨浪冲击、元素突击、复仇杀戮");
                    break;
                case Hoby.精灵:
                    Console.WriteLine("精灵的技能：减速陷阱、能量浪潮、旋风剑舞");
                    break;
                default:
                    break;
            }
            Console.ReadKey();


        }
    }
}
