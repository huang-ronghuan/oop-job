﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DEmo1
{
    public enum Fruit
    {
        葡萄,
        草莓,
        奇异果,
        苹果,
        香蕉,
        哈密瓜,
        西瓜
    }
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                int n = random.Next(0,7);
                score((Fruit)n);
            }
        }
        public static void score(Fruit fruit)
        {
            switch (fruit)
            {
                case Fruit.葡萄:
                    Console.WriteLine("葡萄+10分！");
                    break;
                case Fruit.草莓:
                    Console.WriteLine("草莓+8分！");
                    break;
                case Fruit.奇异果:
                    Console.WriteLine("奇异果+7分！");
                    break;
                case Fruit.苹果:
                    Console.WriteLine("苹果+6分！");
                    break;
                case Fruit.香蕉:
                    Console.WriteLine("香蕉+5分！");
                    break;
                case Fruit.哈密瓜:
                    Console.WriteLine("哈密瓜+3分！");
                    break;
                case Fruit.西瓜:
                    Console.WriteLine("西瓜+1分！");
                    break;
                default:
                    break;
            }


        }
    }
}
