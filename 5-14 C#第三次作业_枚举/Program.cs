﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    enum fruit
    {
        葡萄, 草莓, 奇异果, 苹果, 香蕉, 哈密瓜, 西瓜
    }
    enum yose
    { 
        战士,法师,精灵
    }
    class Program
    {
        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                fruit sex1 = (fruit)random.Next(0,7);
                apple(sex1);
            }

            Console.ReadKey(true);
            Console.WriteLine("\n");
            Console.Beep();
            yose sex2 = (yose)random.Next(0, 4);
            NDS(sex2);
        }

        public static void apple(fruit sex1)  //切水果
        {
            switch (sex1)
            {
                case fruit.葡萄:
                    Console.WriteLine("切到葡萄："+"+10分");
                    break;
                case fruit.草莓:
                    Console.WriteLine("切到草莓：" + "+8分");
                    break;
                case fruit.奇异果:
                    Console.WriteLine("切到奇异果：" + "+7分");
                    break;
                case fruit.苹果:
                    Console.WriteLine("切到苹果：" + "+5分");
                    break;
                case fruit.香蕉:
                    Console.WriteLine("切到香蕉：" + "+4分");
                    break;
                case fruit.哈密瓜:
                    Console.WriteLine("切到哈密瓜：" + "+2分");
                    break;
                case fruit.西瓜:
                    Console.WriteLine("切到西瓜：" + "+1分");
                    break;
                default:
                    break;
            }

        }

        public static void NDS(yose sex2) //职业技能
        {
            switch (sex2)
            {
                case yose.战士:
                    Console.WriteLine("您选择的职业是：战士\n" + "战士的技能：碎石打击、烈焰锚钩、战斗咆哮");
                    break;
                case yose.法师:
                    Console.WriteLine("您选择的职业是：法师\n" + "法师的技能：巨浪冲击、元素突击、复仇杀戮");
                    break;
                case yose.精灵:
                    Console.WriteLine("您选择的职业是：精灵\n"+"精灵的技能：减速陷阱、能量浪潮、旋风剑舞");
                    break;
                default:
                    break;
            }

        }
    }
}
