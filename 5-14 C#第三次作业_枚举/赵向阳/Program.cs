﻿using System;


namespace ConsoleApp1
{
    enum Fruit
    {
        葡萄,
        草莓,
        奇异果,
        苹果,
        香蕉,
        哈密瓜,
        西瓜,
    }
    class Program
    {
        //1、有一款叫做“切水果”的游戏，里面有各种各样的水果，
        //A、现在请定义一个叫做“Fruit”的枚举类型，里面有：葡萄、草莓、奇异果、苹果、香蕉、哈密瓜、西瓜。
        //B、然后定义一个输出水果分数的方法，参数类型就是这个水果枚举，
        //方法中根据不同的枚举值，输出水果对应的分数（可以参考切水果游戏）。注：水果形状越小，切到的时候分数就越高。
        //C、在主方法里面采用循环实现：随机生成10个数(0~6)，代表水果枚举值（可以将整数强制转换为枚举类型值）；
        //然后调用输出水果分数的方法，将该随机水果枚举值作为参数

        static void Main(string[] args)
        {
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                Fruit w = (Fruit)random.Next(0, 7);
                q(w);
            }
        }
        public static void q(Fruit Fruit)
        {
            switch (Fruit)
            {
                case Fruit.葡萄:
                    Console.WriteLine("葡萄的分数为" + 10 + "分");
                    break;
                case Fruit.草莓:
                    Console.WriteLine("草莓的分数为"+5+"分");
                    break;
                case Fruit.奇异果:
                    Console.WriteLine("奇异果的分数为"+4+"分");
                    break;
                case Fruit.苹果:
                    Console.WriteLine("苹果的分数为"+6+"分");
                    break;
                case Fruit.香蕉:
                    Console.WriteLine("香蕉的分数为"+5+"分");
                    break;
                case Fruit.哈密瓜:
                    Console.WriteLine("哈密瓜的分数为"+7+"分");
                    break;
                case Fruit.西瓜:
                    Console.WriteLine("西瓜的分数为"+3+"分");
                    break;
                default:
                    break;
            }
        }
    }
}

