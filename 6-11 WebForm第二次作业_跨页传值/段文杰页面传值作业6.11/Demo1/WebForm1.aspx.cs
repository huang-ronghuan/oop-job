﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //第一次加载页面的时候赋初始值为0;
                ViewState["count"] = 0;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string str = ViewState["count"].ToString();
            int count = Convert.ToInt32(str);
            count++;
            ViewState["count"] = count;
            Label2.Text = "按钮单机的次数" + ViewState["count"];
        }
    }
}