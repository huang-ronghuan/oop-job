﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Demo1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <br/>
          请输入您的姓名：  <asp:TextBox ID="TextBox1" runat="server" AutoPostBack="True"></asp:TextBox>
            <br/>
            <br/>
            <asp:Button ID="Button1" runat="server" Text="提交本页面" OnClick="Button1_Click" />
            &nbsp &nbsp &nbsp<asp:Button ID="Button2" runat="server" Text="提交到Page2" PostBackUrl="~/WebForm3.aspx" OnClick="Button2_Click1"  />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
