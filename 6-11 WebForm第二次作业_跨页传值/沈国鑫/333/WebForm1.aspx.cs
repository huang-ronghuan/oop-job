﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;

namespace Demo3
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void Button1_Click(object sender, EventArgs e)
        {            
            if (TextBox3.Text != TextBox2.Text)
            {
                Response.Write("确认密码错误！");
            }
            else
            {
                Label2.Text = "用户名：" + TextBox1.Text;
                Label3.Text = "密码：" + TextBox2.Text;
                Label4.Text = "性别：" + RadioButtonList1.Text;
                Label5.Text = "邮箱：" + TextBox4.Text;
                Label6.Text = "地址：" + DropDownList1.Text + DropDownList2.Text;
                Label7.Text = "详细地址：" + TextBox5.Text;
            }
        }
    }
}