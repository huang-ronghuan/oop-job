﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
               
                ViewState["count"] = 0;

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string countStr = ViewState["count"].ToString();
            int count = Convert.ToInt32(countStr);
            count++;
            ViewState["count"] = count;
            Label2.Text = "按钮单击次数" + ViewState["count"];
        }
    }
}