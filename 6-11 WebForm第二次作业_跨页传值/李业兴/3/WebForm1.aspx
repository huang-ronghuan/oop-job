﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <style type="text/css">
        .auto-style1 {
            height: 23px;
            width: 80px;
        }
        .auto-style2 {
            width: 140px;
        }
        .auto-style3 {
            height: 23px;
            width: 140px;
        }
        .auto-style4 {
            width: 80px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--编写一个用户注册页面，
                其中包括用户名、密码、确认密码、性别(单选)、邮箱、地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、个人简介；
                最后添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>
            <table>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label1" runat="server" Text="用户名："></asp:Label></td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBox1" runat="server" ></asp:TextBox></td>
                    <td></td>
                    <td rowspan="10">
                        <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label2" runat="server" Text="密码："></asp:Label></td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label3" runat="server" Text="确认密码："></asp:Label></td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox></td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label4" runat="server" Text="性别："></asp:Label></td>
                    <td class="auto-style2">
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged">
                            <asp:ListItem Value="0" Text="男"></asp:ListItem>
                            <asp:ListItem Value="1" Text="女"></asp:ListItem>
                        </asp:RadioButtonList></td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label5" runat="server" Text="邮箱："></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                        
                    </td>
                    
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label6" runat="server" Text="地址："></asp:Label>
                    </td>
                    <td class="auto-style2">
                        <asp:DropDownList ID="DropDownList1" runat="server" Width="75px">
                            <asp:ListItem>北京市</asp:ListItem>
                        </asp:DropDownList>
                        <asp:DropDownList ID="DropDownList2" runat="server" Width="65px">
                            <asp:ListItem>朝陽区</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Label ID="Label7" runat="server" Text="详细地址"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:TextBox ID="TextBox5" runat="server" Height="145px" TextMode="MultiLine" Width="224px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label8" runat="server" Text="个人简介"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:TextBox ID="TextBox6" runat="server" Height="70px" TextMode="MultiLine" Width="140px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4"></td>
                    <td class="auto-style2"></td>
                    <td>
                        <asp:Button ID="Button1" runat="server" Text="提交" ToolTip="提交信息" OnClick="Button1_Click" /></td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
