﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["count"]= 0;
                
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //定义一个数组countStr存放初始值
            string countStr = ViewState["count"].ToString();
            //定义整数类型int的数组count,countStr转换成int。
            int count = Convert.ToInt32(countStr);
            //每次加1
            count++;
            ViewState["count"]= count;
            Label2.Text = "按钮单击次数：" + ViewState["count"]+"次";
        }
    }
}