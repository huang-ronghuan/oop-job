﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Work2.aspx.cs" Inherits="Work1.Work2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            请输入你的姓名:<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server"  Text="提交本页面" OnClick="Button1_Click" />
            <asp:Button ID="Button2" runat="server" Text="提交到Work3" PostBackUrl="~/Work3.aspx"/>
            <br />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
