﻿ <%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Work4.aspx.cs" Inherits="Work1.Work4" %>

<!DOCTYPE html>

<%--3.编写一个用户注册页面，其中包括用户名、密码、确认密码、性别(单选)、邮箱、
    地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、个人简介；最后
    添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            用户名:&nbsp; <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            密码:&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
           确认密码:<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>
            <br />
           性别:<asp:RadioButtonList ID="rblSex" runat="server" >
                <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="女" Value="1"></asp:ListItem>
                </asp:RadioButtonList>
           邮箱:&nbsp;&nbsp;<asp:TextBox ID="TextBox5" runat="server"></asp:TextBox>
            <br />
           地址:<asp:DropDownList ID="DropDownList1" runat="server">
               <asp:ListItem Text="北京" Value="1"></asp:ListItem>
               <asp:ListItem Text="上海" Value="1"></asp:ListItem>
               <asp:ListItem Text="广州" Value="1"></asp:ListItem>
              </asp:DropDownList>
            <br />
           详细地址:<asp:TextBox ID="TextBox6" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
            <br />
           个人简历:<asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine" Rows="10"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label3" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label4" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label5" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label6" runat="server" Text="Label"></asp:Label>
            <asp:Label ID="Label7" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
