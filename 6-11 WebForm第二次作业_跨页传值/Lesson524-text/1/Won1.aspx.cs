﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Won1 : System.Web.UI.Page
    {
        static int Count;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Count = 0;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Count++;
           Label2.Text = "您单击了我" + Count + "次！";
        }
    }
}