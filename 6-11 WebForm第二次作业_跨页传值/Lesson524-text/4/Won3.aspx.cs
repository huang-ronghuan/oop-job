﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class Won3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                this.DrList2.Items.Add("泉州市");
                this.DrList2.Items.Add("杭州市");
                this.DrList2.Items.Add("金华市");
                this.DrList2.Items.Add("深圳市");
                this.DrList2.Items.Add("中山市");
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = "用户名：" + Text1.Text;
            Label2.Text += "<br />" + "密码：" + Text2.Text;
            Label2.Text += "<br />" + "确认密码：" + Text3.Text;
            Label2.Text += "<br />" + "性别：" + this.RadioList1.SelectedItem.Text;
            Label2.Text += "<br />" + "邮箱：" + Text4.Text;
            Label2.Text += "<br />" + "省：" + DrList1.Text;
            Label2.Text +="  市：" + DrList2.Text;
            Label2.Text += "<br />" + "详细地址：" + Text5.Text;
            Label2.Text += "<br />" + "个人简介：" + Text6.Text;
        }

    }
}