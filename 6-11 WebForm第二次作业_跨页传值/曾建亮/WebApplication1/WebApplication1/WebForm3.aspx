﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm3.aspx.cs" Inherits="WebApplication1.WebForm3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #TextArea1 {
            height: 88px;
            width: 153px;
            margin-bottom: 0px;
        }
        #TextArea2 {
            height: 88px;
            width: 148px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
<%--3.	编写一个用户注册页面，
    其中包括用户名、密码、确认密码、性别(单选)、邮箱、
    地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、个人简介；
    最后添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>

            用户名:&nbsp;&nbsp;<asp:TextBox ID="TextBox1" runat="server"  TextMode="SingleLine" EnableViewState="False"></asp:TextBox>
            <br />
            密码:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            确认密码:&nbsp;<asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            性别:&nbsp;&nbsp;&nbsp;
            <asp:RadioButtonList ID="rblSex" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="RadioButtonList1_SelectedIndexChanged" AutoPostBack="True">
                <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="女" Value="1"></asp:ListItem>
            </asp:RadioButtonList>
            <br />
            邮箱:&nbsp;&nbsp;&nbsp;<asp:TextBox ID="TextBox4" runat="server" TextMode="SingleLine"></asp:TextBox>

            <br />
            地址: 省份<asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged">
                <asp:ListItem Value="1">福建省</asp:ListItem>
                <asp:ListItem Value="2">广东省</asp:ListItem>
            </asp:DropDownList>
            城市<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Value="1">龙岩市</asp:ListItem>
                <asp:ListItem Value="2">深圳市</asp:ListItem>
            </asp:DropDownList>
            <br />
&nbsp;&nbsp;&nbsp; 详细地址<textarea id="TextArea1" name="S1"></textarea><br />
&nbsp;&nbsp;&nbsp; 个人简介<textarea id="TextArea2" name="S2"></textarea><br />
&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="提交" />
            <br />
&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
