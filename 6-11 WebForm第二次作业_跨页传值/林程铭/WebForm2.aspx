﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="Demo3.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:Label ID="Label1" runat="server" Text="请输入您的姓名："></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:TextBox ID="name" runat="server" AutoPostBack="true"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交本页面" />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" PostbackUrl="~/WebForm3.aspx" Text="提交到Page2" OnClick="Button2_Click" />
&nbsp;&nbsp;&nbsp;
        </div>
    </form>
</body>
</html>
