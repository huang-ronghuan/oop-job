﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo3
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["MyCounter"] = 0;
            }
        }
        static int count = 0;
        protected void Button1_Click(object sender, EventArgs e)
        {
            count++;
            Literal1.Text = "猛戳了" + count + "次";
        }
    }
}