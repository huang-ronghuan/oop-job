﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="Demo3.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <br />
            <b>用户名:</b>&nbsp;&nbsp; <asp:TextBox ID="TextBox1" runat="server" TextMode="SingleLine" MaxLength="10"></asp:TextBox>
            <br />
            <b>密码:</b>&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox2" runat="server" TextMode="Password" OnTextChanged="TextBox2_TextChanged" MaxLength="12" Height="16px"></asp:TextBox>
            <br />
            <b>确认密码:&nbsp; </b><asp:TextBox ID="TextBox3" runat="server" TextMode="Password" MaxLength="12"></asp:TextBox>
            <br />
            <b>性别:&nbsp;&nbsp; </b> &nbsp;<asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="女" Value="1"></asp:ListItem>
            </asp:RadioButtonList>
         
            <br />
            <b>邮箱:&nbsp;&nbsp;&nbsp; </b><asp:TextBox ID="TextBox5" runat="server" TextMode="SingleLine"></asp:TextBox>
            <br />
            <b>地址：&nbsp;&nbsp; </b>
            <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text ="北京" Value="1"></asp:ListItem>
                <asp:ListItem Text ="福建" Value="1"></asp:ListItem>
                <asp:ListItem Text ="湖南" Value="1"></asp:ListItem>
                <asp:ListItem Text ="广东" Value="1"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Text ="海淀" Value="1"></asp:ListItem>
                <asp:ListItem Text ="龙岩" Value="1"></asp:ListItem>
                <asp:ListItem Text ="长沙" Value="1"></asp:ListItem>
                <asp:ListItem Text ="广州" Value="1"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="TextBox4" runat="server" TextMode="MultiLine"></asp:TextBox>
            <br />
            <b> 个人简介：</b><asp:TextBox ID="TextBox7" runat="server" TextMode="MultiLine" Height="89px" Width="183px"></asp:TextBox>
            <br />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button1" runat="server" Text="提交" BackColor="#66FF66" BorderColor="#0066FF" Width="60px" OnClick="Button1_Click" />
&nbsp;&nbsp; <br />
            <br />
            <asp:Label ID="Label1" runat="server" Text="Label"  ></asp:Label>

        </div>
    </form>
</body>
</html>
