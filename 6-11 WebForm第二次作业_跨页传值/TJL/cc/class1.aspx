﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="class1.aspx.cs" Inherits="cc.class1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--1.	实现一个Counter计数器，每次点击显示单机总次数--%>
            <asp:Label ID="Label1" runat="server" Text="按钮计数器"></asp:Label>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="单击我啊" OnClick="Button1_Click" />
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
        </div>
    </form>
</body>
</html>
