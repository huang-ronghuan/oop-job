﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="class2.aspx.cs" Inherits="cc.class2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
           <%-- 2.	实现跨页传值，在Page1输入姓名，通过按钮跳转到Page2,在Page2显示用户输入的姓名信息。--%>
             <asp:Label ID="Label1" runat="server" Text="请输入你的姓名："></asp:Label>
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交本页面" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="Button2" runat="server" Text="提交到Page2"  PostBackUrl="~/class3.aspx"/>


        </div>
    </form>
</body>
</html>
