﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="class3.aspx.cs" Inherits="cc.class3" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%--3.	编写一个用户注册页面，其中包括用户名、密码、确认密码、性别(单选)、邮箱、地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、
            个人简介；最后添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>
          <b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label1" runat="server" Text="用户注册"></asp:Label>
            <br />
            </b>
            <br />
            用户名：&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="TextBox1" runat="server"  TextMode="SingleLine"></asp:TextBox>  <%--//单行--%>
            <br />
            <br />
            密码：&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>  <%--//加密--%>
            <br />
            <br />
            确认密码： <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <br />
            性别：<br />
&nbsp;<asp:RadioButtonList ID="sex" runat="server" RepeatDirection="Horizontal" >
                <asp:ListItem  Text="男" Value="nan"></asp:ListItem>
                <asp:ListItem Text="女" Value="nv"></asp:ListItem>
               </asp:RadioButtonList>
            <br />
            邮箱：<asp:TextBox ID="TextBox4" runat="server" TextMode="SingleLine"></asp:TextBox >
            <br />
            <br />
            省份：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="广西" Value="guangxi"></asp:ListItem>
                <asp:ListItem Text="福建" Value="fujian"></asp:ListItem>
                <asp:ListItem Text="江苏" Value="jiangsu"></asp:ListItem>
                <asp:ListItem Text="湖南" Value="hunan"></asp:ListItem>
                <asp:ListItem Text="浙江" Value="zhejiang"></asp:ListItem>
                <asp:ListItem Text="北京" Value="beijing"></asp:ListItem>
               </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Text="河池" Value="hechi"></asp:ListItem>
                <asp:ListItem Text="龙岩" Value="longyan"></asp:ListItem>
                <asp:ListItem Text="南京" Value="nanjing"></asp:ListItem>
                <asp:ListItem Text="长沙" Value="changsha"></asp:ListItem>
                <asp:ListItem Text="杭州" Value="hangzhou"></asp:ListItem>
                <asp:ListItem Text="石家庄" Value="shijiazhuang"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            个人简介：<asp:TextBox ID="TextBox5" runat="server" Height="146px" Width="241px"></asp:TextBox>

            <br />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

            <asp:Button ID="Button1" runat="server" Text="确认提交" OnClick="Button1_Click" />
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label2" runat="server" Text=""></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label4" runat="server" Text=""></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
            <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label7" runat="server" Text=""></asp:Label>

        </div>
    </form>
</body>
</html>
