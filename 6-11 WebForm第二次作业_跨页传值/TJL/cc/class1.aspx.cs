﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace cc
{
    public partial class class1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Label1.Text = "<b>按钮计时器</b>";
            if (!IsPostBack)
            {

                ViewState["sum"] = 0;
            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string sum1 = ViewState["sum"].ToString();
            int sum = Convert.ToInt32(sum1);
            sum++;
            ViewState["sum"] = sum;
            Label2.Text = "您单杀了我" + ViewState["sum"] + "次";
        }
    }
}