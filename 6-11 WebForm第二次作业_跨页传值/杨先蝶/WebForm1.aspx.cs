﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Demo1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
       static int count;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                count=0;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            count++;
            Label2.Text = "您单击了我"+count.ToString()+"次";
        }
    }
}