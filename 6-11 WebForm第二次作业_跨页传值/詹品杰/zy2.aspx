﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="zy2.aspx.cs" Inherits="WebApplication3.zy2" %>

<%@ Register assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" namespace="System.Web.UI.DataVisualization.Charting" tagprefix="asp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .form {
            text-align:center;
        
            }
        .auto-style3 {
            width: 249px;
            height: 22px;
        }
        .auto-style4 {
            width: 169px;
        }
        .auto-style5 {
            width: 90%;
            height:50%;
            margin-left:20%;
            margin-top:5%
            
           
        }
        .auto-style7 {
            height: 22px;
            text-align:center;
        }
        .auto-style8 {
            width: 5px;
            height: 22px;
        }
        .auto-style9 {
            margin-right: 0px;
            margin-top: 0px;
        }
        .auto-style10 {
            padding:0;
            
        }
        .auto-style11 {
            height: 19px;
        }
        .auto-style12 {
            width: 5px;
            height: 19px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%-- 2.	实现跨页传值，
                在Page1输入姓名，通过按钮跳转到Page2,
                在Page2显示用户输入的姓名信息。 --%>


            <table class="auto-style5" >
                <tr>
                    <td colspan="2" class="auto-style7"></td>
                    <td class="auto-style3">
                        <h3>
                            学生信息注册表
                        </h3>
                        </td>
                    <td class="auto-style8">&nbsp;

                    </td>
                    <td class="auto-style10" rowspan="9">
                        
                        <asp:Label ID="Label1" runat="server" BorderStyle="Groove" Height="339px" Width="359px" CssClass="auto-style9"></asp:Label>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">用户名:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox1" runat="server" MaxLength="10"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">密码:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">确定密码：</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">邮箱:</td>
                    <td colspan="3">
                        <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">地址:</td>

                    <td colspan="3">
                        省:<asp:DropDownList ID="DropDownList1" runat="server">

                        <asp:ListItem Text ="北京" Value="1"></asp:ListItem>
                        <asp:ListItem Text ="广东" Value="2"></asp:ListItem>
                        <asp:ListItem Text ="福建" Value="3"></asp:ListItem>

                        </asp:DropDownList>


                        城市:<asp:DropDownList ID="DropDownList2" runat="server">

                        <asp:ListItem Text ="北京" Value="1"></asp:ListItem>
                        <asp:ListItem Text ="福安" Value="2"></asp:ListItem>
                        <asp:ListItem Text ="宁德" Value="3"></asp:ListItem>

                        </asp:DropDownList>

                        街道:<asp:DropDownList ID="DropDownList3" runat="server">
                            <asp:ListItem Text="城北街道" Value="1"></asp:ListItem>
                            <asp:ListItem Text="城南街道" Value="2"></asp:ListItem>
                            <asp:ListItem Text="城西街道" Value="3"></asp:ListItem>

                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">性别：</td>

                    <td colspan="3">

                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" AutoPostBack="true" RepeatDirection="Horizontal" Visible="true">
                            <asp:ListItem Text="男" Value="Sex_Gn"  Selected="True"></asp:ListItem>
                            <asp:ListItem Text="女" Value="Sex_Fn"  ></asp:ListItem>
                        </asp:RadioButtonList>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">个人简介：</td>

                    <td colspan="3">

                        <asp:TextBox ID="TextBox5" runat="server" Height="152px" Rows="10" TextMode="MultiLine" Width="333px"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="auto-style11">
                        <asp:ChangePassword ID="ChangePassword1" runat="server">
                        </asp:ChangePassword>
                    </td>

                    <td class="auto-style12" aria-atomic="False">
                        <asp:Button ID="Button1" runat="server" Height="40px" Text="提交" Width="95px" OnClick="Button1_Click" />
                    </td>

                </tr>
            </table>


        </div>
    </form>
</body>
</html>
