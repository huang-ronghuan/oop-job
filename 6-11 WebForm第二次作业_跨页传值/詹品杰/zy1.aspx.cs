﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication3
{
    public partial class zy1 : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ViewState["count"]=0;

            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            //定义值 用于取值
            String counStr = ViewState["count"].ToString();

            int count = Convert.ToInt32(counStr);
            count++;
            ViewState["count"] = count;

            Label2.Text = "点击次数为：" + ViewState["count"];
               
           
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }
    }
}