﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="Demo3.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <%--3.	编写一个用户注册页面，其中包括用户名、密码、确认密码、性别(单选)、
    邮箱、地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、
    个人简介；最后添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>
    <form id="form1" runat="server">
        <div>
            <b>
                <asp:Label ID="Label1" runat="server" Text="用户注册页面"></asp:Label>
            <br />
            <br />
            <asp:Label ID="Label2" runat="server" Text="用户名："></asp:Label>
            &nbsp;
            <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label3" runat="server" Text="密码：" TextMode="Password" MaxLength="10"></asp:Label>
            &nbsp;&nbsp;
            <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
            <br />
            <br />
            <asp:Label ID="Label4" runat="server" Text="确认密码：" TextMode="Password" MaxLength="10"></asp:Label>
            &nbsp;<asp:TextBox ID="TextBox3" runat="server"></asp:TextBox>

            <br />
            <br />
            <asp:RadioButtonList ID="RadioButtonList2" runat="server"  RepeatDirection="Horizontal">
                <asp:ListItem Text="男" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="女" Value="1"></asp:ListItem>
                 
            </asp:RadioButtonList>
           

            <br />
            邮箱：&nbsp;&nbsp; 
            <asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br />
            <br />
            详细地址：&nbsp;<asp:TextBox ID="TextBox5" runat="server" TextMode="MultiLine" Rows="10" Height="50px"></asp:TextBox>
           

            <br />
            <br />
            省份：<asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="福建" Value="福建"></asp:ListItem>
                <asp:ListItem Text="贵州" Value="贵州"></asp:ListItem>
                <asp:ListItem Text="广西" Value="广西"></asp:ListItem>
                <asp:ListItem Text="云南" Value="云南"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            城市：<asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Text="龙岩" Value="龙岩"></asp:ListItem>
                <asp:ListItem Text="漳州" Value="漳州"></asp:ListItem>
                <asp:ListItem Text="泉州" Value="泉州"></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="Button1" runat="server" Text="提交" OnClick="Button1_Click" />
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label5" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label6" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label7" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label8" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label9" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label11" runat="server" Text=""></asp:Label>
            <br />
            &nbsp;&nbsp;
            <asp:Label ID="Label10" runat="server" Text=""></asp:Label>
            <br />
           

        </div>
    </form>
</body>
</html>
