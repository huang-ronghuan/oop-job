﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(PreviousPage!=null)
            {
                TextBox textBox = (TextBox)(PreviousPage.FindControl("TextBox1"));
                string name = textBox.Text;
                Response.Write("Hello " + name);

            }

        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Label2.Text = "请确认您填写的信息";
            Label3.Text = "用户名：" + TextBox1.Text;
            string Sex = RadioButtonList1.SelectedValue;
            Label4.Text = "性别：" + Sex;
            Label5.Text = "邮箱：" + TextBox4.Text;
            string province = DropDownList1.SelectedValue;
            Label6.Text = "省份：" + province;
            string city = DropDownList2.SelectedValue;
            Label7.Text = "城市：" + city;
            Label8.Text = "详细地址：" + TextBox5.Text;
        }
    }
}