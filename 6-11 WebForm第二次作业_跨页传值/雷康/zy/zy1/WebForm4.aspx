﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm4.aspx.cs" Inherits="zy1.WebForm4" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            
           <%--1.	编写一个用户注册页面，其中包括用户名、密码、确认密码、
            性别(单选)、邮箱、地址（省份—下拉列表、城市—下拉列表、详细地址—文本域）、
            个人简介；最后添加一个按钮，该按钮将用户填写的文本信息，全部输出到Label标签中。--%>
            用户名;<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <div/>
            <br/>
            <div>
            密码:   <asp:TextBox ID="TextBox2" runat="server" TextMode="Password"></asp:TextBox>
                <div/>
            <br/>
            确认密码：<asp:TextBox ID="TextBox3" runat="server" TextMode="Password"></asp:TextBox>
            <br/>
            性别：<asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                 <asp:ListItem Text="男" Value="0"></asp:ListItem >
                <asp:ListItem Text="女" Value="1"></asp:ListItem>
               </asp:RadioButtonList>
            <br/>
            邮箱：<asp:TextBox ID="TextBox4" runat="server"></asp:TextBox>
            <br/>
            地址：
            <asp:DropDownList ID="DropDownList1" runat="server">
                <asp:ListItem Text="福建" value="fj"></asp:ListItem>

                <asp:ListItem Text="广西" value="gx"></asp:ListItem>
                <asp:ListItem Text="广东" value="gd"></asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server">
                <asp:ListItem Text="龙岩"></asp:ListItem>
                <asp:ListItem Text="深圳"></asp:ListItem>
                <asp:ListItem Text="柳州"></asp:ListItem>
            </asp:DropDownList>
            <asp:TextBox ID="TextBox5" runat="server" Text="详细地址" TextMode="MultiLine"></asp:TextBox>
            <br/>
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
            <br/>
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

        </div/>
    </form>
</body>
</html>
