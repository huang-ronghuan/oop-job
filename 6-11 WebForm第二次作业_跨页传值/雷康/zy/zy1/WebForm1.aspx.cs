﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace zy1
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        static int num;
        protected void Page_Load(object sender, EventArgs e)
        {
            Label2.Text = "按钮计数器";
            if (!IsPostBack)
            {
                num = 0;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            num++;
            Label1.Text = "您单击我了" + num + "次";
        }
    }
}