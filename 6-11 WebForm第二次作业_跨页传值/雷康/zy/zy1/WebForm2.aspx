﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm2.aspx.cs" Inherits="zy1.WebForm2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
          <%--  2.	实现跨页传值，在Page1输入姓名，通过按钮跳转到Page2,在Page2显示用户输入的姓名信息。--%>
            请输入姓名：<asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
            <br />
            <asp:Button ID="Button1" runat="server"  text="提交页面1" OnClick="Button1_Click"  />
            <asp:Button ID="Button2" runat="server"  text="提交页面2" OnClick="Button2_Click" PostBackUrl="~/WebForm3.aspx"/>
            <br />
            <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
        </div>
    </form>
</body>
</html>
