﻿using System;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一串字符串");
            string s = Console.ReadLine();
            char[] ch = s.ToCharArray();
            int count1 = 0;
            int count2 = 0;
            int count3 = 0;
            for (int i = 0; i < ch.Length; i++)
            {
                if (char.IsLetter(ch[i]))
                {
                    count1++;
                    continue;
                }
                if (char.IsNumber(ch[i]))
                {
                    count2++;
                    continue;
                }
                else
                {
                    count3++;
                    continue;
                }
                
            }
            Console.WriteLine("字母的个数为：" + count1);
            Console.WriteLine("数字的个数为：" + count2);
            Console.WriteLine("空格的个数为：" + count3);
        }
    }
}
