﻿using System;

namespace Dome3
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] num = new int[5];
            for (int i = 0; i < num.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位数");
                num[i] = int.Parse(Console.ReadLine());
            }
            for (int j = 0; j < num.Length-1; j++)
            {
                for (int i = 0; i < num.Length; i++)
                {
                    if (num[i]<num[i+1])
                    {
                        int tepm = num[i + 1];
                        num[i + 1] = num[i];
                        num[i] = tepm;
                    }
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                Console.Write(num[i] + " ");
            }
            Console.WriteLine("\n");
        }
    }
}

