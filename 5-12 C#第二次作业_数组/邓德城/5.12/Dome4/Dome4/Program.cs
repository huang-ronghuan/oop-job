﻿using System;

namespace Dome4
{
    class Program
    {
        static void Main(string[] args)
        {
            int sum = 1;
            int[] num = new int[] { 666, 777, 888, 999, 555 };
            Console.WriteLine("请输入需要查找的元素：");
            int s = int.Parse(Console.ReadLine());
            for (int i = 0; i < num.Length; i++)
            {
                if (s == num[i])
                {
                    Console.WriteLine(num[i] + "最后出现的位置是"+(i+1)+"" );
                    sum = i;
                    }
            }
            if (sum == 1)
            {
                Console.WriteLine("没有找到");
            }
            Console.WriteLine(" ");
        }
    }
}
