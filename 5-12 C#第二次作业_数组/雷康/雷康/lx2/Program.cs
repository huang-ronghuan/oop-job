﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lx2
{
    class Program
    {
        static void Main(string[] args)
        {
            //2.输入一行字符串，
            //分别统计出其中英文字母、数字、空格的个数。
            Console.WriteLine("请输入一行字符串");
            string a = Console.ReadLine();

            int word = 0;
            int math = 0;
            int sp = 0;
            int other = 0;


            char[] b = a.ToCharArray();
            foreach (char i in b) {
                if (i >= 'a' && i <= 'z' || i >= 'A' && i <= 'Z')
                {
                    word++;
                }
                else if (i >= '0' && i <= '9')
                {
                    math++;
                }
                else if (i == ' ')
                {
                    sp++;
                }
                else other++;
            }
        }
    }
}
