﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lx4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入数字");
            int i = int.Parse(Console.ReadLine());
            int[] a = new int[i];

            for (int j=0;j<a.Length;j++) {
                Console.WriteLine("请输入第"+(j+1)+"位数字");
                a[j] = int.Parse(Console.ReadLine());
            }

            for (int p=0;p<i-1;p++) {
                for (int q=0;q<i-1-p;q++) {
                    if (a[q]<a[q+1]) {
                        int tamp = a[q];
                        a[q] = a[q + 1];
                        a[q + 1] = tamp;
                    }
                }
            }
            for (int o=0;o<i;o++) {
                Console.WriteLine(a[o]);
            }
            Console.ReadKey();
        }
    }
}
