﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lx6
{
    class Program
    {
        static void Main(string[] args)
        {
            //6.在 Main 方法中创建一个字符串类型的数组，并存入 5 个值
            ////，然后将数组中下标是偶数的元素输出。
            string[] a = new string[5];
            for (int i=0;i<a.Length;i++) {
                Console.WriteLine("请输入");
                a[i]=Console.ReadLine();
            }
            for (int j=0;j<a.Length;j++) {
                if (j%2==0) {
                    Console.WriteLine("第"+(j+1)+"位为："+a[j]);

                }
            }
            Console.ReadKey();
        }
    }
}
