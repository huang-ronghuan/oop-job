﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        //1.	用循环打印九九乘法表（用二维数组保存字符串后再打印）
        static void Main(string[] args)
        {
            int[][] q = new int[9][];
            for (int i = 0; i < q.Length; i++)
            {
                q[i] = new int[i + 1];
            }
            for (int i = 1; i < q.Length + 1; i++)
            {
                for (int j = 1; j < i + 1; j++)
                {
                    Console.Write(j + "*" + i + "=" + i * j + "\t");

                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }

    }
}
