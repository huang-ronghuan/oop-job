﻿using System;

namespace Lonsse1
{
    class Program
    {
        //2.	输入一行字符串，分别统计出其中英文字母、数字、空格的个数。
        static void Main(string[] args)
        {
            int a = 0;

            int b = 0;

            int c = 0;
            string sum;
            Console.WriteLine("请输入一个字符串");
            sum = Console.ReadLine();
            foreach (char chr in sum)
            {
                if (char.IsLetter(chr))
                {
                    a++;
                }
                if (char.IsDigit(chr))
                {
                    b++;
                }
                if (char.IsUpper(chr))
                {
                    c++;
                }
            }
            Console.WriteLine("字母的个数为:"+a);
            Console.WriteLine("数字的个数为:"+b);
            Console.WriteLine("空格的个数为:"+c);
            Console.ReadKey();
        }
    }
}
