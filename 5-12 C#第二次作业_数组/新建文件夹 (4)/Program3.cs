﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] num0 = new double[5];
            for (int i = 0; i < num0.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位学生的考试成绩:");
                num0[i] = double.Parse(Console.ReadLine());
            }
            double score = 0;
            foreach (double scores in num0)
            {
                score += scores;
            }
            Console.WriteLine("5 名学生的总成绩:" + score + "  平均成绩" + score / num0.Length);
            Console.WriteLine("\t");
        }
    }
}
