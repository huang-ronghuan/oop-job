﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] mm = new int[5];
            for (int i = 0; i < mm.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位的数：");
                mm[i] = int.Parse(Console.ReadLine());
            }
            for (int j = 0; j < mm.Length; j++)
            {
                for (int i = 0; i < mm.Length - 1; i++)
                {
                    if (mm[i] < mm[i + 1])
                    {
                        int tepm = mm[i + 1];
                        mm[i + 1] = mm[i];
                        mm[i] = tepm;
                    }
                }
            }
            for (int i = 0; i < mm.Length; i++)
            {
                Console.Write(mm[i] + " ");
            }
            Console.WriteLine("\n");

        }
    }
}
