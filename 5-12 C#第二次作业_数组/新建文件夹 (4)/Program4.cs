﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] num1 = new string[5] { "sadfw", "aww", "grehd", "416543", "34yer" };
            for (int i = 0; i < num1.Length; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(num1[i] + "\t");
                }
            }
            Console.WriteLine("\n");
        }
    }
}
