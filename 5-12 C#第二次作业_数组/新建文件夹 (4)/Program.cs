﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {//1.用循环打印九九乘法表（用二维数组保存字符串后再打印）
            string[][] num = new string[9][];
            for (int i = 0; i < num.Length; i++)
            {
                num[i] = new string[i + 1];
            }

            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    num[i][j] = (i + 1) + "*" + (j + 1) + "=" + (i + 1) * (j + 1);
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    Console.Write(num[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");
        }
    }
}
