﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一串字符：");
            string b1 = Console.ReadLine();
            int letter = 0;//用来统计字母的数量
            int blank = 0;//用来统计空格的数量
            int number = 0; //用来统计数字的数量

            char[] c = b1.ToCharArray();//把字符串转换成字符数组
            foreach (char i in c)
            {
                if (i >= 'a' && i <= 'z' || i >= 'A' && i <= 'Z')
                    letter++;
                else if (i >= '0' && i <= '9')
                    number++;
                else if (i == ' ')
                    blank++;
            }
            Console.WriteLine("字母有" + letter + "个，空格有" + blank + "个，数字有" + number);
            Console.WriteLine("\n");
        }
    }
}
