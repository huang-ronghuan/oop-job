﻿using System;

namespace Lonsse2
{
    class Program
    {
        //3.在 Main 方法中创建一个 double 类型的数组，
        //并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。
        //（要求使用foreach语句实现该功能）
        static void Main(string[] args)
        {
            double[] q = new double[5];
            for (int i = 0; i < q.Length; i++)
            {
                Console.WriteLine("请输入第"+(i+1)+"位学生考试成绩");
                q[i] = double.Parse(Console.ReadLine());
            }
            double w = 0;
            foreach (double item in q)
            {
                w += item;
            }
            Console.WriteLine("学生的总成绩为:"+w+"学生的平均成绩为:"+w/q.Length);
            Console.WriteLine("\t");
        }
    }
}
