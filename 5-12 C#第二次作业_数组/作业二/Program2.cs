﻿using System;

namespace Lonsse1
{
    class Program
    {
        //2.	输入一行字符串，分别统计出其中英文字母、数字、空格的个数。
        static void Main(string[] args)
        {
            int q = 0;

            int w = 0;

            int e = 0;
            string sum;
            Console.WriteLine("请输入一个字符串");
            sum = Console.ReadLine();
            foreach (char chr in sum)
            {
                if (char.IsLetter(chr))
                {
                    q++;
                }
                if (char.IsDigit(chr))
                {
                    w++;
                }
                if (char.IsUpper(chr))
                {
                    e++;
                }
            }
            Console.WriteLine("字母的个数为:"+q);
            Console.WriteLine("数字的个数为:"+w);
            Console.WriteLine("空格的个数为:"+e);
            Console.ReadKey();
        }
    }
}
