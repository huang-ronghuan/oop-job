﻿using System;

namespace Lonsse5
{
    class Program
    {
        //6.在 Main 方法中创建一个字符串类型的数组，
        //并存入 5 个值，然后将数组中下标是偶数的元素输出。
        static void Main(string[] args)
        {
            int[] q = new int[5];
            for (int i = 0; i < q.Length; i++)
            {
                if (i%2==0)
                {
                    Console.WriteLine(q[i]+"\t");
                }
            }
            Console.WriteLine("\n");
        }
    }
}
