﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp4
{
    class Program
    {
        static void Main(string[] args)
        {
            int n;
            int[] a = new int[]{5,4,7,8,1,3 };
            for (int i = 0; i < a.Length - 1; i++)
            {
                for (int j = 0; j < a.Length - 1 - i; j++)
                {

                    if (a[j] < a[j + 1])
                    {
                        n = a[j];
                        a[j] = a[j + 1];
                        a[j + 1] = n;
                    }
                }

            }
            for (int i = 0; i < a.Length; i++)
            {
                Console.Write(a[i]+"\t");
            }
            Console.ReadKey();
        }
     } 
}
