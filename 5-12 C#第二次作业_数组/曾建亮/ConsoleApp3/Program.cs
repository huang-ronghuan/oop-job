﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] points = { 55,98,78,54,54 };
            double sum = 0;
            double avg = 0;
            foreach (double point in points)
            {
                sum+=point;
            }
            avg = sum / points.Length;
            Console.WriteLine("总成绩为：" + sum);
            Console.WriteLine("平均成绩为：" + avg);
        }
    }
}
