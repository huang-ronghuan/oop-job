﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            //5.	实现查找数组元素索引的功能。定义一个数组，
            //然后控制台输入要查找的元素，返回输入值在数组中最后一次出现的位置。
            //若是找不到，请打印找不到。(不要用Array类的方法)
            int[] a = new int[] {1,2,3,4,5,6,7,8,9 };
            Console.WriteLine("输入要查找的元素");
            int n = int.Parse(Console.ReadLine());
            
               
                    int index = -1;
                    if (a != null && a.Length != 0)
                    {

                        for (int j = 0; j < a.Length; j++)
                        {
                            if (a[j] == n)
                            {
                                index = j;
                                Console.WriteLine(index);
                            }
                        }
                        
                    }
                
                else
                {
                    Console.WriteLine("找不到");
                }
            
            Console.ReadKey();
        }
    }
}
