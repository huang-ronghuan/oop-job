﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp6
{
    class Program
    {
        static void Main(string[] args)
        {
            //在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，然后将数组中下标是偶数的元素输出。
            string[] a = new string[] {"1","2","3","4","5" };
            for (int i = 0; i < a.Length; i++)
            {
                if (i%2==0)
                {
                    Console.WriteLine(a[i]);
                }
            }
            Console.ReadKey();
        }
    }
}
