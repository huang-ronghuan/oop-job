﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            int countLetters = 0;
            int countDigits = 0;
            int countPunctuations = 0;
            Console.WriteLine("请输入一行字符串：");
            string input = Console.ReadLine();

            foreach (char item in input)
            {
                if (char.IsLetter(item))
                    countLetters++;
                if (char.IsDigit(item))
                    countDigits++;
                if (char.IsPunctuation(item))
                    countPunctuations++;
            }

            Console.WriteLine("字母个数为：{0}"+countLetters);
            Console.WriteLine("字母个数为：{0}" + countDigits);
            Console.WriteLine("字母个数为：{0}" + countPunctuations);
            Console.ReadKey();

        }
    }
}
