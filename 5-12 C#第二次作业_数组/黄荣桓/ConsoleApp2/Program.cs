﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.用循环打印九九乘法表（用二维数组保存字符串后再打印）
            string[][] num = new string[9][];
            for (int i = 0; i < num.Length; i++)
            {
                num[i] = new string[i + 1];
            }

            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    num[i][j] = (i + 1) + "*" + (j + 1) + "=" + (i + 1) * (j + 1);
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    Console.Write(num[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");



            //2.输入一行字符串，分别统计出其中英文字母、数字、空格的个数
            Console.WriteLine("请输入一串字符：");
            string b = Console.ReadLine();
            int d = 0;//统计字母
            int e = 0;//统计空
            int f = 0; //统计数字
            char[] c = b.ToCharArray();//把字符串转换成字符数组
            foreach (char i in c)
            {
                if (i >= 'a' && i <= 'z' || i >= 'A' && i <= 'Z')
                    d++;
                else if (i >= '0' && i <= '9')
                    f++;
                else if (i == ' ')
                    e++;
                
                    
            }
            Console.WriteLine("字母有" + d + "个，空格有" + e + "个，数字有" + f  );
            Console.WriteLine("\n");



            //4.	定义一个方法，实现一维数组的排序功能，从大到小排序。
            int[] n = new int[5];
            for (int i = 0; i < n.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位的数：");
                n[i] = int.Parse(Console.ReadLine());
            }
            for (int j = 0; j < n.Length; j++)
            {
                for (int i = 0; i < n.Length - 1; i++)
                {
                    if (n[i] < n[i + 1])
                    {
                        int tepm = n[i + 1];
                        n[i + 1] = n[i];
                        n[i] = tepm;
                    }
                }
            }
            for (int i = 0; i < n.Length; i++)
            {
                Console.Write(n[i] + " ");
            }
            Console.WriteLine("\n");


            //5.	实现查找数组元素索引的功能。定义一个数组，然后控制台输入要查找的元素，
            //返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。
            //(不要用Array类的方法)
            int index = -1;
            int[] nn = new int[] { 6, 2, 8, 1, 7, 4 };
            Console.WriteLine("请输入要查找的元素：");
            int one = int.Parse(Console.ReadLine());
            for (int i = 0; i < nn.Length; i++)
            {
                if (one == nn[i])
                {
                    Console.WriteLine(nn[i] + "最后出现的位置是nn[" + i + "]");
                    index = i;
                    break;
                }
            }
            if (index == -1)
            {
                Console.WriteLine("找不到！");
            }
            Console.WriteLine("\n");



            //6.	在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，
            //后将数组中下标是偶数的元素输出。
            string[] num1 = new string[5] { "小基", "小黑", "小白", "小红", "大黄！" };
            for (int i = 0; i < num1.Length; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(num1[i] + "\t");
                }
            }
            Console.WriteLine("\n");


        }
    }
    }
