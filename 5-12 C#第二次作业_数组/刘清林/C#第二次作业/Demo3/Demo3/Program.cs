﻿using System;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            int   [] students = new int  [5];
            int sum =0;
            for (int i = 0; i < students.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "名同学的成绩：");
                students[i] = Convert.ToInt32(Console.ReadLine());
                sum += students[i];
            }
          foreach(int  i in students)
            {
                sum += i;
            }
            Console.WriteLine("总成绩为："+sum);
            Console.WriteLine("平均值为："+sum/students.Length/2);
            Console.Read();
        }
    }
}
