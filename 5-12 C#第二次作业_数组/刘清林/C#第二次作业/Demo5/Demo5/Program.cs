﻿using System;

namespace Demo5
{
    static class FindArray
    {
        public static void find<T>(ref T[] A, ref int n)
        {
            Console.WriteLine("数组中第{0}个元素为{1}", n, A[n - 1]);
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            double[] A = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
            Console.WriteLine("请输入要查找的元素的位置");
            int n = int.Parse(Console.ReadLine());
            FindArray.find<double>(ref A, ref n);
            Console.ReadKey();
        }
    }
}
