﻿using System;

namespace Dome4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] nums = new int[] { 1, 3, 2, 5, 6, 7, 5, 9 };

            // 从小到大排序
            Array.Sort(nums);

            foreach (var val in nums)
            {
                Console.Write(val + " ");
            }
            Console.WriteLine();


            // 从大到小排序
            Array.Sort(nums);
            Array.Reverse(nums);
            foreach (var val in nums)
            {
                Console.Write(val + " ");
            }
            Console.WriteLine();


            Console.ReadKey();
        }
    }
}

