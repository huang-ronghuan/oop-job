﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.用循环打印九九乘法表（用二维数组保存字符串后再打印）
            string[][] num = new string[9][];
            for (int i = 0; i < num.Length; i++)
            {
                num[i] = new string[i + 1];
            }

            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    num[i][j] = (i + 1) + "*" + (j + 1) + "=" + (i + 1) * (j + 1);
                }
            }
            for (int i = 0; i < num.Length; i++)
            {
                for (int j = 0; j < num[i].Length; j++)
                {
                    Console.Write(num[i][j] + "\t");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");

            //2.输入一行字符串，分别统计出其中英文字母、数字、空格的个数。
            Console.WriteLine("请输入一串字符：");
            string b1 = Console.ReadLine();
            int ENG = 0;//用来统计字母的数量
            int kong = 0;//用来统计空格的数量
            int alb = 0; //用来统计数字的数量
            int other = 0;//用来统计其它字符的数量
            char[] c = b1.ToCharArray();//把字符串转换成字符数组
            foreach (char i in c)
            {
                if (i >= 'a' && i <= 'z' || i >= 'A' && i <= 'Z')
                    ENG++;
                else if (i >= '0' && i <= '9')
                    alb++;
                else if (i == ' ')
                    kong++;
                else
                    other++;
            }
            Console.WriteLine("字母有" + ENG + "个，空格有" + kong + "个，数字有" + alb + "个，其它字符有" + other + "个.");
            Console.WriteLine("\n");

            //3.	在 Main 方法中创建一个 double 类型的数组，并在该数组中
            //存入 5 名学生的考试成绩，计算总成绩和平均成绩。
            //（要求使用foreach语句实现该功能）
            double[] num0 = new double[5];
            for (int i = 0; i < num0.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位学生的考试成绩:");
                num0[i] = double.Parse(Console.ReadLine());
            }
            double score = 0;
            foreach (double scores in num0)
            {
                score += scores;
            }
            Console.WriteLine("5 名学生的总成绩:" + score + "  平均成绩" + score / num0.Length);
            Console.WriteLine("\t");

            //4.	定义一个方法，实现一维数组的排序功能，从大到小排序。
            //(不要用Array类的方法)
            int[] mm = new int[5];
            for (int i = 0; i < mm.Length; i++)
            {
                Console.WriteLine("请输入第" + (i + 1) + "位的数：");
                mm[i] = int.Parse(Console.ReadLine());
            }
            for (int j = 0; j < mm.Length; j++)
            {
                for (int i = 0; i < mm.Length - 1; i++)
                {
                    if (mm[i] < mm[i + 1])
                    {
                        int tepm = mm[i + 1];
                        mm[i + 1] = mm[i];
                        mm[i] = tepm;
                    }
                }
            }
            for (int i = 0; i < mm.Length; i++)
            {
                Console.Write(mm[i] + " ");
            }
            Console.WriteLine("\n");

            //5.	实现查找数组元素索引的功能。定义一个数组，然后控制台输入要查找的元素，
            //返回输入值在数组中最后一次出现的位置。若是找不到，请打印找不到。
            //(不要用Array类的方法)
            int index = -1;
            int[] nn = new int[] { 6, 7, 8, 1, 9, 3 };
            Console.WriteLine("请输入要查找的元素：");
            int one = int.Parse(Console.ReadLine());
            for (int i = 0; i < nn.Length; i++)
            {
                if (one == nn[i])
                {
                    Console.WriteLine(nn[i] + "最后出现的位置是nn[" + i + "]");
                    index = i;
                    break;
                }
            }
            if (index == -1)
            {
                Console.WriteLine("找不到！");
            }
            Console.WriteLine("\n");

            //6.	在 Main 方法中创建一个字符串类型的数组，并存入 5 个值，
            //后将数组中下标是偶数的元素输出。
            string[] num1 = new string[5] { "sadf", "asefg", "grehd", "416543", "恭喜你看到这条信息，你走运了！" };
            for (int i = 0; i < num1.Length; i++)
            {
                if (i % 2 == 0)
                {
                    Console.Write(num1[i] + "\t");
                }
            }
            Console.WriteLine("\n");

            //7.	用户输入正方形边长，用*打印出空心正方形。
            Console.WriteLine("请用户输入正方形的边长求空心：");
            int n1 = int.Parse(Console.ReadLine());
            for (int i = 0; i < n1; i++)
            {
                if (i != 0 && i != n1 - 1)
                {
                    Console.Write("* ");
                    for (int j = 0; j < n1 - 2; j++)
                    {
                        Console.Write("  ");
                    }
                    Console.Write("*");
                }
                else
                {
                    for (int k = 0; k < n1; k++)
                    {
                        Console.Write("* ");
                    }
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");

            //8.	用户输入正方形边长，用*打印出实心正方形。
            Console.WriteLine("请用户输入正方形的边长求实心：");
            int n2 = int.Parse(Console.ReadLine());
            for (int i = 0; i < n2; i++)
            {
                for (int j = 0; j < n2; j++)
                {
                    Console.Write("* ");
                }
                Console.WriteLine();
            }
            Console.WriteLine("\n");

            //9.	用二维数组存放数据，实现杨辉三角形的打印；
            int[][] ss = new int[10][];
            for (int i = 0; i < ss.Length; i++)
            {
                ss[i] = new int[i + 1];
            }
            for (int i = 0; i < ss.Length; i++)
            {
                for (int j = 0; j < ss[i].Length; j++)
                {
                    if (0==j || i==j)
                    {
                        ss[i][j] = 1;
                    }
                    else
                    {
                        ss[i][j] = ss[i - 1][j - 1] + ss[i - 1][j];
                    }
                }
            }
            for (int i = 0; i < ss.Length; i++)
            {
                for (int j = 0; j < ss[i].Length; j++)
                {
                    Console.Write(ss[i][j]+"\t");
                }
                Console.WriteLine();
            }

        }
    }
}
