﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Name : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.name.CompareTo(y.name);
        }
    }
}
