﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Stuid:IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.stuid.CompareTo(y.stuid);
        }
    }
}
