﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Age : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.age.CompareTo(y.age);
        }
    }
}
