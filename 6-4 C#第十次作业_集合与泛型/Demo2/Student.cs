﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo2
{
    class Student
    {
        public string stuid { get; set; }
        public string name { get; set; }
        public string age { get; set; }
        public Student(string stuid, string name, string age)
        {
            this.stuid = stuid;
            this.name = name;
            this.age = age;
        }
        public override string ToString()
        {
            return "学号：" + stuid + ",姓名：" + name + ",年龄：" + age;

        }
    }
}
