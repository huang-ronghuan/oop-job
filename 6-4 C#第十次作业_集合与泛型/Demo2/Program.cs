﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace Demo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu1 = new Student("001", "小米", "20");
            Student stu2 = new Student("002", "小华", "23");
            Student stu3 = new Student("003", "小伟", "22");
            Dictionary<string, Student> stu = new Dictionary<string, Student>();
            stu.Add(stu1.stuid,stu1);
            stu.Add(stu2.stuid,stu2);
            stu.Add(stu3.stuid,stu3);

            List<Student> list = new List<Student> { stu1, stu2, stu3 };

            Console.WriteLine("学生信息表容量：{0}，学生个数{1}",stu.Comparer, stu.Count);
            Console.WriteLine("\r\n ---------请选择----------");
            Console.WriteLine("1.添加学生信息  2.删除学生信息  3.查询学生信息  4.退出");
            Console.WriteLine("-------------------------------------------------------\r\n");
            Console.WriteLine("请输入您的选择：");
            bool num = true;
            while (num) 
            {
                string n = Console.ReadLine();
                switch (n)
            {
                    case "1":
                        bool a = true;
                        while (a) { 
                            Console.WriteLine("请添加同学信息：（学号）");
                            string stuid = Console.ReadLine();
                            Console.WriteLine("请添加同学信息：（姓名）");
                            string name = Console.ReadLine();
                            Console.WriteLine("请添加同学信息：（年龄）");
                            string age = Console.ReadLine();
                            int index = -1;
                            foreach (var item in stu)
                            {
                                if (item.Equals(stuid))
                                {

                                    index = 1;
                                }
                            }
                            if (index == -1)
                            {
                                Student stu4 = new Student(stuid, name, age);
                                stu.Add(stu4.stuid, stu4);
                                Console.WriteLine("添加成功");
                            }
                            else 
                            {
                                Console.WriteLine("添加失败");
                            }
                            Console.WriteLine("还要继续吗？（y/n）");
                            string fi = Console.ReadLine();

                            if (fi.Equals("n"))
                            {
                                Console.WriteLine("学生信息表：");
                                foreach (var Student in stu)
                                {
                                    Console.WriteLine(Student);
                                }
                                break;
                            }
                        }
                        break;
                    case "2":
                        bool b = true;
                        while (b) 
                        {
                            Console.WriteLine("删除前学生信息表：");
                            foreach (var item in stu)
                            {
                                Console.WriteLine(item);
                            }
                            Console.WriteLine("请输入要删除的学生学号：");
                            string id = Console.ReadLine();
                            
                            if (stu.ContainsKey(id))
                            {
                                stu.Remove(id);
                                Console.WriteLine("删除成功");
                                Console.WriteLine("删除后的学生表：");
                                foreach (var item in stu)
                                {
                                    Console.WriteLine(item);
                                }
                                Console.WriteLine("是否继续：（y/n）");
                            string num1 = Console.ReadLine();
                            if (num1.Equals("n"))
                            {
                                Console.WriteLine("退出");
                            }
                            }
                            else
                            {
                                Console.WriteLine("删除失败,该学生不存在");
                            }
                        }
                        break;
                    case "3":
                        Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序）" +
                                          "3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）" +
                                          "5、退出");
                        Console.WriteLine("请选择一个选项：");
                        bool c = true;
                        while (c) 
                        {
                            string sum = Console.ReadLine();
                            
                             switch (sum)
                            { 
                                
                                case "1":
                                    //按学号排序
                                    list.Sort(new Stuid());
                                    break;
                                case "2":
                                    list.Sort(new Name());
                                    break;
                                case "3":
                                    list.Sort(new Age());
                                    break;
                                case "4":
                                    Console.WriteLine("请输入需要查询的学生学号：");
                                    string fId = Console.ReadLine();
                                    if (stu.ContainsKey(fId))
                                    {
                                        Console.WriteLine("有该学生信息");
                                        foreach (var item in list)
                                        {
                                            Console.WriteLine(item);
                                        }
                                    }
                                    else 
                                    {
                                        Console.WriteLine("找不到该学生信息");
                                      
                                    }
                                    break;
                                case "5":
                                    c = false;
                                    break;
                                default:
                                    Console.WriteLine("输入数字错误，请重新输入！");
                                   break;
                            }
                            
                        }
                        break;
                    case "4":
                        num = false;
                        break;
                    default:
                        Console.WriteLine("输入有误，请重新输入！");
                        break;
            }
            
            }
            

        }

        private class stuid : IComparer<Student>
        {
            public int Compare(Student x, Student y)
            {
                return x.stuid.CompareTo(y.stuid);
            }
        }
    }
}
