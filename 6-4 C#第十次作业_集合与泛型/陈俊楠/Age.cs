﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
namespace ConsoleApp1
{
    class Age : IComparer<Student>

    {
        public int Compare([AllowNull] Student x, [AllowNull] Student y)
        {
            return x.age.CompareTo(y.age);
        }
    }

}
