﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1
{
    class Student
    {
        //、学生类：学号、姓名、年龄
        public int stuID { set; get; }
        public string name { set; get; }
        public int age { set; get; }
        public Student(int stuID, string name ,int age){
            this.stuID = stuID;
            this.name = name;
            this.age = age;
        }
        public override string ToString()
        {
            return $"学号:{stuID},姓名:{name},年龄:{age}";
        }

    }
}
