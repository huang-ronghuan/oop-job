﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Student : IComparable <Student>
    {
        public string Num { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public Student()
        {

        }
        public Student(string num, string name, int age)
        {

            this.Num = num;
            this.Name = name;
            this.Age = age;

        }



        public override string ToString()
        {
            return $"学号：{Num},姓名：{Name},年龄：{Age}";
        }

        public int CompareTo(Student other)
        {
            return this.Num.CompareTo(other.Num);
            return this.Name.CompareTo(other.Name);
            return this.Age.CompareTo(other.Age);
        }
    }
}
