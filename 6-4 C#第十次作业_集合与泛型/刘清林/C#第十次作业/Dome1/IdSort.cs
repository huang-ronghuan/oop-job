﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class IdSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
           return x.Num.CompareTo(y.Num);
        }
    }
}
