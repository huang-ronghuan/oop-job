﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu1 = new Student("001", "任正非", 63);
            Student stu2 = new Student("002", "阿姆斯特朗", 36);
            Student stu3 = new Student("003", "马云", 42);
            
            List<Student> list = new List<Student>() {stu1,stu2,stu3 };
            list.Add(stu1);
            list.Add(stu2);
            list.Add(stu3);
            list.Sort();

            Dictionary<string, Student> stu = new Dictionary<string, Student>();
            stu.Add(stu1.Num, stu1);
            stu.Add(stu2.Num, stu2);
            stu.Add(stu3.Num, stu3);

            bool A = true;
            while (A)
            {
                Console.WriteLine("请选择：1.添加学生信息。2.删除学生信息 3.查询学生信息、4.退出");
                int num1 = int.Parse(Console.ReadLine());
                bool C = true;
                switch (num1)
                {
                    case 1:
                        Console.WriteLine("请输入你要添加的学生的学号");
                        int a = int.Parse(Console.ReadLine());//学号
                        Console.WriteLine("请输入你要添加的学生的姓名");
                        string b = Console.ReadLine();//姓名
                        Console.WriteLine("请输入你要添加的学生的年龄");
                        int c = int.Parse(Console.ReadLine());//年龄
                        Console.WriteLine("添加成功！");
                        break;
                    case 2:
                        Console.WriteLine("删除前学生信息表：");
                        foreach (var item in stu)
                        {
                            Console.WriteLine(item);
                        }
                        Console.WriteLine("请输入要删除的学生学号：");
                        string id = Console.ReadLine();

                        if (stu.ContainsKey(id))
                        {
                            stu.Remove(id);
                            Console.WriteLine("删除成功");
                            Console.WriteLine("删除后的学生表：");
                            foreach (var item in stu)
                            {
                                Console.WriteLine(item);
                            }
                            if (num1.Equals("n"))
                            {
                                Console.WriteLine("退出");
                            }
                        }
                        else
                        {
                            Console.WriteLine("删除失败,该学生不存在");
                        }
                break;
                    case 3:
                        bool B = true;
                        while (B)
                        {
                            Console.WriteLine("请输入你要查询的学生信息");
                            Console.WriteLine
                                ("1、按学号查询 " +
                                "2、按姓名查询 " +
                                "3、按年龄查询 " +
                                "4、按学号查询（查没有，则打印查无此学生）" +
                                "5、退出");
                            int num3 = int.Parse(Console.ReadLine());
                            switch (num3)
                            {
                                case 1:
                                    Console.WriteLine("按学号排序：");
                                    for (int i = 0; i < list.Count; i++)
                                    {
                                        Console.WriteLine(list[i]);
                                    }
                                    break;
                                case 2:
                                    Console.WriteLine("按姓名排序：");
                                    IComparer<Student> comparer = new NameSort();
                                    list.Sort(comparer);


                                    for (int i = 0; i < list.Count; i++)
                                    {
                                        Console.WriteLine(list[i]);
                                    }
                                    break;
                                case 3:
                                    Console.WriteLine("按年龄排序");
                                    for (int i = 0; i < list.Count; i++)
                                    {
                                        Console.WriteLine(list[i]);
                                    }
                                    break;
                                case 4:
                                    Console.WriteLine("请输入需要查询的学生学号：");
                                    string fId = Console.ReadLine();
                                    if (stu.ContainsKey(fId))
                                    {
                                        Console.WriteLine("有该学生信息");
                                        foreach (var item in list)
                                        {
                                            Console.WriteLine(item);
                                        }
                                    }
                                    else
                                    {
                                        Console.WriteLine("找不到该学生信息");

                                    }
                                    break;
                                case 5:
                                    B = false;
                                    break;
                                default:
                                    Console.WriteLine("输入有误，请重新输入！");
                                    break;
                            }
                        }
                        break;
                    case 4:
                        C = false;
                        Console.WriteLine("退出登录！");
                        break;
                    default:
                        Console.WriteLine("输入有误，请重新输入！");
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
