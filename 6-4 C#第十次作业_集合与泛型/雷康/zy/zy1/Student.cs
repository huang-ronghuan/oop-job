﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace zy1
{
    class Student:IComparable<Student>
    {
        public string name { get; set; }
        public int age { get; set; }
        public int num { get; set; }

        public Student()
        {

        }
        public Student(string name,int age,int num)
        {
            this.name = name;
            this.age = age;
            this.num = num;
        }
        public override string ToString()
        {
            string all = "姓名：" + name + "年龄：" + age +"学号："+num;
            return all;
        }

        public int CompareTo(Student other)
        {
            return this.num.CompareTo(other.num);
        }
    }
}
