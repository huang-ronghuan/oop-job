﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace zy1
{
    class Program
    {

        //1、学生类：学号、姓名、年龄
        //2、请选择：1.添加学生信息。2.删除学生信息 3.查询学生信息、4.退出
        //3、添加学生信息：要求重复的学号不能添加。学生信息从控制台输入
        //4、查询学生信息功能中有：1、查询所有（按学号排序）2、查询所有（按姓名排序），
        //                         3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）
        //                         5、退出
        //添加3个类，分别实现 IComparer<T>接口，实现对Student类的三个字段的排序。
        static void Main(string[] args)
        {
             
            List<Student> list= new List<Student>();
            
            Hashtable hash = new Hashtable();

            ArrayList arry = new ArrayList();
            Student stu1 = new Student( "类直接",20,001 );
            Student stu2 = new Student( "张波长",21,002 );
            Student stu3 = new Student( "律羽片",10,003 );
            list.Add(stu1);
            list.Add( stu2);
            list.Add(stu3);

            arry.Add(stu1);
            arry.Add(stu2);
            arry.Add(stu3);

            hash.Add(stu1.num,stu1);
            hash.Add(stu2.num, stu2);
            hash.Add(stu3.num, stu3);

            bool a = true;
            while (a==true)
            {
                        Console.WriteLine("请选择：1.添加学生信息。2.删除学生信息 3.查询学生信息、4.退出");
                        int i = int.Parse(Console.ReadLine());

                switch (i)
                {
                    case 1:
                        
                        
                        for (int o = 0;o < list.Count; o++)
                        {

                            Student stu= new Student();
                            Console.WriteLine("请输入要添加学生的学号");
                                      
                             int num1 = int.Parse(Console.ReadLine());
                            Console.WriteLine("请输入要添加学生的姓名");
                            string name1 = Console.ReadLine();
                            Console.WriteLine("请输入要添加学生的年龄");
                            int age = int.Parse(Console.ReadLine());
                            if (hash.ContainsKey(num1))
                            {
                                Console.WriteLine("该学号已存在，请重新输入");
                            }
                            if (!hash.ContainsKey(num1))
                            {
                                stu.age = age;
                                stu.num = num1;
                                stu.name = name1;
                                list.Add(stu);
                                hash.Add(stu.num,stu);
                                arry.Add(stu);
                                break;
                            }
                        }

                        break;
                    case 2:
                       
                        Console.WriteLine("请输入要删除的学生的学号");
                        int name = int.Parse(Console.ReadLine());
                        //int num = int.Parse(Console.ReadLine());
                        // arry.Remove(name);
                        if (!hash.ContainsKey(name))
                        {
                            Console.WriteLine("没有该学生");
                        }
                        else
                        {
                            hash.Remove(name);
                            //Student[] ab = new Student[list.Count];
                            //for (int l=0;l<list.Count;l++)
                            //{
                            //    ab[l] = list[l];
                            //}
                            arry.AddRange(hash);
                            list.Clear();
                            foreach (Student item in arry)
                            {
                                list.Add(item);
                            }
                            
                            //for (int l = 0; l < list.Count; l++)
                            //{
                                
                                
                                
                            //}
                                
                        }
                        break;
                    case 3:

                        bool b = true;
                        while (b == true)
                        {
                            Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序），" +
                                "3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）" +
                                "5、退出");
                            int p = int.Parse(Console.ReadLine());

                            ArrayList arry1= new ArrayList();
                            arry1.Add(hash);
                            switch (p)
                            {
                                case 1:
                                    list.Sort();
                                    foreach (var item in list)
                                    {
                                        Console.WriteLine(item);
                                    }
                                    break;
                                case 2:
                                    IComparer<Student> comparer = new Newsort();
                                    list.Sort(comparer);
                                    foreach (var item in list)
                                    {
                                        Console.WriteLine(item);
                                    } 
                                    break;
                                case 3:
                                    IComparer<Student> comparer1 = new Agesort();
                                    list.Sort(comparer1);
                                    foreach (var item in list)
                                    {
                                        Console.WriteLine(item);
                                    }
                                    break;
                                case 4:
                                    Console.WriteLine("请输入要查询的学生的学号");
                                    int ip = int.Parse(Console.ReadLine());
                                             
                                    if (hash.ContainsKey(ip))
                                    {
                                        Console.WriteLine(hash[ip]);
                                    }

                                    else
                                    {
                                        Console.WriteLine("没有该同学");
                                    }
                                    
                                    break;
                                case 5:
                                    b = false;
                                    break;
                                default:
                                    Console.WriteLine("输入错误，请重新输入");
                                    break;
                            }
                        }
                        break;
                    case 4:
                        a = false;
                        break;
                    default:
                        Console.WriteLine("输入错误，请重新选择");
                        break;
                }
            }

            Console.ReadKey();
        }

    }
}
    

