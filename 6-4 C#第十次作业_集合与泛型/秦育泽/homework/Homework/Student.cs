﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Homework
{
    class Student:IComparable<Student>
    {
        private string number;
        private string name;
        private int age;

        public string Number { get => number; set => number = value; }
        public string Name { get => name; set => name = value; }
        public int Age { get => age; set => age = value; }


        public Student(string number, string name, int age)
        {
            this.Number = number;
            this.Name = name;
            this.Age = age;
        }

        public Student()
        {

        }

        public int CompareTo(Student other)
        {
           return  this.number.CompareTo(other.number);
            
        }

        public override string ToString()
        {
            return $"学号:{Number},姓名：{Name}，年龄{Age}";
        }
    }
}
