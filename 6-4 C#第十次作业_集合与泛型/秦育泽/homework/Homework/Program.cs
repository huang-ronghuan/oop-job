﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
namespace Homework
{
    class Program
    {
        static void Main(string[] args)
        {

            Student stu1 = new Student("001", "类直接", 20);
            Student stu2 = new Student("002", "张波长", 21);
            Student stu3 = new Student("003", "律羽片", 10);

            List<Student> list1 = new List<Student>();
            list1.Add(stu1);
            list1.Add(stu2);
            list1.Add(stu3);



            Console.WriteLine("请选择：1.添加学生信息。2.删除学生信息 3.查询学生信息、4.退出");
            int select = int.Parse(Console.ReadLine());
            if (select == 1)            //添加学生信息
            {
                Console.WriteLine("请输入需要添加的学生个数");
                int stuNumber = int.Parse(Console.ReadLine());
                Console.WriteLine("请依次输入：学生学号，学生姓名，学生年龄");


                for (int i = 0; i < stuNumber; i++)
                {   

                    string numberAdd = Console.ReadLine();
                    string nameAdd = Console.ReadLine();
                    int ageAdd = int.Parse(Console.ReadLine());         
                    Student stu4 = new Student(numberAdd, nameAdd, ageAdd);//接受形参
                    list1.Add(stu4);
                    

                }
                for (int i = 0; i < list1.Count; i++)
                {
                    Console.WriteLine(list1[i]);
                }


            }
            else if(select == 2)
            {
               Console.WriteLine("请选择要删除的学生信息,学生下标从0开始");

                for (int i = 0; i < list1.Count; i++)
                {
                    Console.WriteLine(list1[i]);
                }

                int del = int.Parse(Console.ReadLine());
                list1.RemoveAt(del);

                for (int i = 0; i < list1.Count; i++)
                {
                    Console.WriteLine(list1[i]);
                }

            }
            else if(select == 3)
            {
                Console.WriteLine("1、查询所有（按学号排序）2、查询所有（按姓名排序），3、查询所有（按年龄排序）4、按学号查询（查没有，则打印查无此学生）5、退出");
                   int serch = int.Parse(Console.ReadLine()); 

                   if(serch == 1)
                    {
                        list1.Sort();
                    for (int i = 0; i < list1.Count; i++)
                    {
                        Console.WriteLine(list1[i]);
                    }

                    }
                   else if(serch == 2)
                    {
                        list1.Sort(new Sort());
                    for (int i = 0; i < list1.Count; i++)
                    {
                        Console.WriteLine(list1[i]);
                    }
                    }
                   else if(serch == 3)
                    {
                        list1.Sort(new SortAge());
                    for (int i = 0; i < list1.Count; i++)
                    {
                        Console.WriteLine(list1[i]);
                    }
                }
                   else if(serch == 4)
                    {
                    list1.Sort();
                    for (int i = 0; i < list1.Count; i++)
                    {
                        Console.WriteLine(list1[i]);
                    }
                    Console.WriteLine("请输入需要查询的学号");
                    string selectNumber = Console.ReadLine();
                    if(selectNumber == "001")
                    {
                        Console.WriteLine("学号：001 姓名：类直接 年龄：20");
                    }
                    else if(selectNumber == "002")
                    {
                        Console.WriteLine("学号：002 姓名：张波长 年龄：21");
                    }
                    else if (selectNumber == "003")
                    {
                        Console.WriteLine("学号：003 姓名：律羽片 年龄：10");
                    }
                    else
                    {
                        Console.WriteLine("查无此学生");
                    }

                }
                   else
                    {
                    Console.WriteLine("退出");
                    }
                   

            }
            else
            {
                Console.WriteLine("退出");
            }

           Console.ReadKey();

        }
    }
}
