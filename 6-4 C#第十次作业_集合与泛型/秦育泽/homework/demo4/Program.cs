﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo4
{
    class Program
    {          //4.定义一个方法，实现一维数组的排序功能，从大到小排序。(不要用Array类的方法)
        static void Main(string[] args)
        {
            int[] array = new int[5];
            for(int i = 0;i < array.Length;i++)
            {
                Console.WriteLine("请输入第{0}个数", i+1);
                array[i] = int.Parse(Console.ReadLine());

            }
            Console.WriteLine("排序：");
            output(array);

            Console.ReadLine();

        }
        static void output(int []array)
        {
            int temp;
            for(int i = 0;i < array.Length - 1;i++)
            {
                for(int j = i + 1;j < array.Length;j++)
                {
                    if(array[i] < array[j])
                    {
                        temp = array[i];
                        array[i] = array[j];
                        array[j] = temp;
                    }
                }
            }
            for(int i = 0; i < array.Length;i++)
            {
                Console.Write(array[i] + "\t");
            }
        }
    }
}
