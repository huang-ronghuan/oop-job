﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo2
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("请从键盘输入任意数值");
            string a;
            int b = 0;
            int c = 0;
            int d = 0;
            a = Console.ReadLine();
            foreach( char s in a)
            {
                if(s >= 'a'&& s < 'z'||s > 'A' && s <= 'Z' )
                {
                    b++;
                }
                if(s >= '0' && s <= '9')
                {
                    c++;
                }
                if(s == ' ')
                {
                    d++;
                }
            }

            Console.WriteLine("中英文字母的个数是：" + b);
            Console.WriteLine("数字的个数是：" + c);
            Console.WriteLine("空格的个数是：" + d);

            Console.ReadLine();

        }
    }
}
