﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace demo3
{
    class Program
    {

        //3.在 Main 方法中创建一个 double 类型的数组，并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。（要求使用foreach语句实现该功能）
        static void Main(string[] args)
        {
            double[] array = new double[5];
            for(int i = 0; i < array.Length;i++)
            {
                Console.WriteLine("请输入第{0}个学生的成绩", (i + 1));
                array[i] = double.Parse(Console.ReadLine());
            }
            double sum = 0;
            double avg = 0;
            foreach(double s in array)
            {
                sum = sum + s;
                avg = sum / 5;
            }
            Console.WriteLine("总成绩为：{0}", sum);
            Console.WriteLine("平均成绩为：{0}",avg);


            Console.WriteLine();
        }
    }
}
