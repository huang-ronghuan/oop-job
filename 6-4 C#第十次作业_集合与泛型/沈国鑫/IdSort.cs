﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class IdSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Id.CompareTo(y.Id);
        }
        public static List<Student> IdSelect(Dictionary<int,Student> stu)
        {
            List<Student> s = new List<Student>();
            s.AddRange(stu.Values);
            s.Sort(new IdSort());
            return s;
        }
    }
}
