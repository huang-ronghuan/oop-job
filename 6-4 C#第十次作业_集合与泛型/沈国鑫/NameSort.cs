﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class NameSort:IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Name.CompareTo(y.Name);
        }
        public static List<Student> NameSelect(Dictionary<int, Student> stu)
        {
            List<Student> s = new List<Student>();
            s.AddRange(stu.Values);
            s.Sort(new NameSort());
            return s;
        }
    }
}
