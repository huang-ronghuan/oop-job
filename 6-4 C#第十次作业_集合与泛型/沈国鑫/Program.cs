﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stu1 = new Student(1, "小王", 19);
            Student stu2 = new Student(2, "小李", 20);
            Dictionary<int, Student> stu = new Dictionary<int, Student>();
            stu.Add(stu1.Id,stu1);
            stu.Add(stu2.Id, stu2);
            bool t = true;
            while (t)
            {
                Console.WriteLine("请选择：");
                Console.WriteLine("1.添加学生信息" + "  " + "2.删除学生信息" + "  " + "3.查询学生信息" + "  " + "4.退出");
                int choose = int.Parse(Console.ReadLine());
                switch (choose)
                {
                    case 1:
                        StuAdd(stu);
                        break;
                    case 2:
                        StuDelete(stu);
                        break;
                    case 3:
                        StuSelect(stu);
                        break;
                    case 4:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("输入有误，请重新输入！");
                        break;
                }
            }
            Console.WriteLine("程序结束");
            Console.ReadKey();
        }

        public static void StuSelect(Dictionary<int, Student> stu)
        {
            bool t = true;
            while (t)
            {
                Console.WriteLine("请选择查询方式：");
                Console.WriteLine("1、查询所有（按学号排序） " + "2、查询所有（按姓名排序） " + "3、查询所有（按年龄排序） " + "4、按学号查询（查没有，则打印查无此学生） " + "5、退出");
                int choose = int.Parse(Console.ReadLine());
                List<Student> s;
                switch (choose)
                {
                    case 1:
                        s = IdSort.IdSelect(stu);
                        Print(s);
                        break;
                    case 2:
                        s = NameSort.NameSelect(stu);
                        Print(s);
                        break;
                    case 3:
                        s = AgeSort.AgeSelect(stu);
                        Print(s);
                        break;
                    case 4:
                        SelectId(stu);
                        break;
                    case 5:
                        t = false;
                        break;
                    default:
                        Console.WriteLine("输入有误，请重新输入！");
                        break;
                }
            }
        }

        private static void SelectId(Dictionary<int, Student> stu)
        {
            Console.WriteLine("请输入想查询的学生学号：");
            int id = int.Parse(Console.ReadLine());
            if (stu.ContainsKey(id))
            {
                Console.WriteLine(stu[id]);
            }
            else
            {
                Console.WriteLine("不存在该学号所对应的学生");
            }
        }

        public  static void StuDelete(Dictionary<int, Student> stu)
        {
            Console.WriteLine("删除前");
            Print(stu);
            Console.WriteLine("请输入要删除的学生学号：");
            int del = int.Parse(Console.ReadLine());
            if (stu.ContainsKey(del))
            {
                stu.Remove(del);
                Console.WriteLine("删除成功！");
                Console.WriteLine("删除后");
                Print(stu);
            }
            else
            {
                Console.WriteLine("该学生不存在！");
            }           
        }

        public static void StuAdd(Dictionary<int,Student> stu)
        {
            Console.WriteLine("添加前");
            Print(stu);

            Console.WriteLine("请输入要添加的学生学号：");
            int Id = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入要添加的学生姓名：");
            string Name = Console.ReadLine();

            Console.WriteLine("请输入要添加的学生年龄：");
            int Age = int.Parse(Console.ReadLine());

            Student student = new Student(Id, Name, Age);
            stu.Add(student.Id,student);
            Console.WriteLine("添加成功！");
            Console.WriteLine("添加后：");
            Print(stu);
        }

        public static void Print(Dictionary<int,Student> stu)
        {
            foreach (var i in stu.Values)
            {
                Console.WriteLine(i);
            }
        }
        public static void Print(List<Student> s)
        {
            foreach (var i in s)
            {
                Console.WriteLine(i);
            }
        }
    }
}
