﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo1
{
    class AgeSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Age.CompareTo(y.Age);
        }
        public static List<Student> AgeSelect(Dictionary<int, Student> stu)
        {
            List<Student> s = new List<Student>();
            s.AddRange(stu.Values);
            s.Sort(new AgeSort());
            return s;
        }
    }
}
