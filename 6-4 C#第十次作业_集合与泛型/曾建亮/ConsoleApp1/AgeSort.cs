﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class AgeSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Num1.CompareTo(y.Num1);
        }
    }
}
