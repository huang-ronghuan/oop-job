﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class NameSort : IComparer<Student>
    {
        public int Compare(Student x, Student y)
        {
            return x.Name1.CompareTo(y.Name1);
        }
    }
}
