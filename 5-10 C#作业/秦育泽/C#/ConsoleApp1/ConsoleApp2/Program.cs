﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个四位数");
            int number = int.Parse(Console.ReadLine());

            int unitsDigit = number % 1000 % 100 % 10;
            int decade = number % 100 / 10;
            int hundredsPlace = number / 100 % 10;
            int kiloBit = number / 1000;
            if (number > 1000 && number < 9999)
            {
                Console.WriteLine("个位" + unitsDigit);
                Console.WriteLine("十位" + decade);
                Console.WriteLine("百位" + hundredsPlace);
                Console.WriteLine("千位" + kiloBit);

            }
            else
                Console.WriteLine("输入的数值有错误！");




            Console.ReadKey();
        }
    }
}
