﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("请输入半径的值");
            const double Π = 3.1415926535;

            double radius = double.Parse(Console.ReadLine());
            double area = Π * radius * radius;
            Console.WriteLine("圆的面积为：" + area);


            Console.ReadKey();

        }    
    }
}
