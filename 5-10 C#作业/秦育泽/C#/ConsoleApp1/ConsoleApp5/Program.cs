﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个数：");
            int input = int.Parse(Console.ReadLine());
            int sum = 0;
            int ret = 1;
            for(int i = 1; i <= input; ++i)
            {
                ret *= i;
                sum += ret;
            }

            Console.WriteLine("结果：" + sum);

            Console.ReadLine();
        }
    }
}
