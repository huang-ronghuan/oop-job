﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lx2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入4位整数");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("千位："+(num/1000));
            Console.WriteLine("百位："+((num%1000)/100));
            Console.WriteLine("十位："+((num%100)/10));
            Console.WriteLine("个位："+(num%10));
            Console.ReadKey();
        }
    }
}
