﻿using System;

namespace Lonsse
{
    class Program
    {
        //1.	求圆的面积
        //要求用户输入半径的值，打印出以此值为半径的圆的面积
        //求圆的面积的公式：πr²。
        //圆周率π定义成常量取3.14。
        //r由用户输入并存入一个变量中，此变量用double比较合适，因为用户可能会输入小数。

        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的面积");
            double q = double.Parse(Console.ReadLine());
            const double Π = 3.14;
            int w = 3;
            Console.WriteLine("圆的面积为" + Π * w * w);
        }
    }
}
