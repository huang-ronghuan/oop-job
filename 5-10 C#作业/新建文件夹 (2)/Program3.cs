﻿using System;

namespace Lonsse3
{
    class Program
    {
        //用户输入三个数，找出最大的数，打印输出。
        static void Main(string[] args)
        {
            Console.WriteLine("请输入第一个数");
            int a = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入第二个数");
            int w = int.Parse(Console.ReadLine());

            Console.WriteLine("请输入第三个数");
            int e = int.Parse(Console.ReadLine());

            int max = a;
            if (max < w) {
                max = w;
            }
            if (max < e){
                max = e;
            }
            Console.WriteLine("最大数为" + max);
        }
    }
}
