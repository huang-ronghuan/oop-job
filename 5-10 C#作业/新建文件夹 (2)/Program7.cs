﻿using System;

namespace Lonsse7
{
    class Program
    {
        //7.用循环打印九九乘法表
        static void Main(string[] args)
        {
            for (int i = 1; i <= 9; i++)
            {
                for (int j = 1; j <=i; j++)
                {
                    Console.WriteLine(i+"*"+j+"="+i*j+" ");
                }
                Console.WriteLine();
            }
        }
    }
}
