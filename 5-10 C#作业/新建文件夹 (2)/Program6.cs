﻿using System;

namespace Lonsse6
{
    class Program
    {
        private const string  s= "\n";

        //用循环打印菱形
        static void Main(string[] args)
        {
            for (int i = 1; i < 5; i++)
            {
                for (int j = 5; j > i; j++)
                {
                    Console.WriteLine(" ");
                }
                for (int q = 0; q < i*2-1; q++)
                {
                    Console.WriteLine("*");
                }
                Console.WriteLine();
            }
            for (int i = 5; i > 1; i++)
            {
                for (int j = i; j < 5; j++)
                {
                    Console.WriteLine(" ");
                }
                for (int k = 0; k < i * 2 - 1; k++)
                {
                    Console.WriteLine("*");
                }
                Console.WriteLine();
            }
            Console.WriteLine(s);
        }
    }
}
