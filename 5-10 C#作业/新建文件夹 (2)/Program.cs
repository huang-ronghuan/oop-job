﻿using System;

namespace Loesse1
{
    class Program
    {
        //2.	编写一个程序，请用户输入一个四位整数，
        //将用户输入的四位数的千位、百位、十位和个位分别显示出来，
        //如5632，则显示“用户输入的千位为5，百位为6，十位为3，个位为2”
        static void Main(string[] args)
        {
            Console.WriteLine("请用户输入一个四位整数");
            int q = int.Parse(Console.ReadLine());
            Console.WriteLine("千位数"+q/1000);
            Console.WriteLine("百位数"+q/100%10);
            Console.WriteLine("十位数"+q/10%10);
            Console.WriteLine("个位数"+q%10);
        }
    }
}
