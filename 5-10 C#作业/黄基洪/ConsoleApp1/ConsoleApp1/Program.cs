﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.复习           

            //Console.WriteLine("请输入你的身高");
            ////转换数据类型 具体语法为:数据类型 变量=数据类型.parse(console.readline());
            //int height=int.Parse(Console.ReadLine());

            ////输出身高 console.writeLine
            //Console.WriteLine("身高为：" + height);
            ////console.readkey() 使电脑页面暂停
            // Console.ReadKey();

            //强制转换为字符串类型 具体语法为：string 变量=目标数据类型.toString
            //string a = height.ToString();

            //定义一个常量
            //const int Age = 18;

            //命名规范
            //1.变量用小写 
            //2.常量用大写
            //3.方法用大写

            //Console.WriteLine("请输入你的期末成绩");
            //int c = int.Parse(Console.ReadLine());
            ////通过switch case 的语句判断学生的成绩是否合格
            ////在90~100之间为优秀 80~89为良好 60~79~为及格 其它(default) 为不及格
            //switch (c/10)
            //{
            //    case 10:
            //        Console.WriteLine("优秀");
            //        break;
            //    case 9:
            //        Console.WriteLine("优秀");
            //        break;
            //    case 8:
            //        Console.WriteLine("良好");
            //        break;
            //    case 7:
            //        Console.WriteLine("及格");
            //        break;
            //    case 6:
            //        Console.WriteLine("及格");
            //        break;
            //    default:
            //        Console.WriteLine("不及格");
            //        break;
            //}


            //题目1.	求圆的面积
            //要求用户输入半径的值，打印出以此值为半径的圆的面积
            //求圆的面积的公式：πr²。
            //圆周率π定义成常量取3.14。
            //r由用户输入并存入一个变量中，此变量用double比较合适，因为用户可能会输入小数。

            Console.WriteLine("面积计算器");
            Console.WriteLine("请输入圆的半径");
            double r =double.Parse(Console.ReadLine());
            const double Pai = 3.14;

            //定义一个中间数存放面积
            double s = Pai * r * r;

            Console.WriteLine("圆的面积为" + s);

            //题目2.编写一个程序，请用户输入一个四位整数，
            //将用户输入的四位数的千位、百位、十位和个位分别显示出来，如5632，
            //则显示“用户输入的千位为5，百位为6，十位为3，个位为2”


            Console.WriteLine("四位数运算");
            Console.WriteLine("请输入一个四位数");
            int num = int.Parse(Console.ReadLine());

            int qian = num / 1000;
            int bai = num  % 1000 /100;
            int shi = num %1000 / 10;
            int ge = num % 10;

            Console.WriteLine("千位数：" + qian + "百位数" + bai + "十位数" + shi + "个位数" + ge);

            //题目3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("数字比较");
            Console.WriteLine("请输入第一个数字");
            int a = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数字");
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("请输入第三个数字");
            int c = int.Parse(Console.ReadLine());

            if (a>b && a>c)
            {
                Console.WriteLine("最大值为：" + a);
            }else if (b > a &&  b> c)
            {
                Console.WriteLine("最大值为：" + b);
            }
            else
            {
                Console.WriteLine("最大值为：" + c);
            }
            //4.	接受用户输入一个数n，求这个数的阶乘；5! = 5*4*3*2*1;
            Console.WriteLine("阶乘运算");
            Console.WriteLine("输入要计算的数字");
            int num1=int.Parse(Console.ReadLine());

            int S1 = 1;
            for (int i=1;i<num1;i++)
            {
                S1 =i*num1;
            }
            Console.WriteLine("输出的值为"+S1);

            //在 Main 方法中创建一个 double 类型的数组，并在该数组中存入 5 名学生的考试成绩，计算总成绩和平均成绩。
            //要求使用foreach语句实现该功能，
            double[] Score = new double[5];
            double sum = 0;
            for (int i=0;i< Score.Length;i++) {
                Console.WriteLine("请输入第" + (i + 1) + "位同学的成绩");
                Score[i] = int.Parse(Console.ReadLine());
                sum = sum + Score[i];
            }
            Console.WriteLine("总成绩为:" + sum);
            Console.WriteLine("平均成绩为：" + sum / 5);
            Console.ReadKey();

            ////定义一个方法，实现一维数组的排序功能，从大到小排序。
            //int[] num2 = new int[5];
            //for (int i=0;i<=num2.Length-1;i++)
            //{
            //    Console.WriteLine("请输入第" + (i + 1) + "位数字");
            //    num2[i]=int.Parse(Console.ReadLine());
            //}
            //int temp;
            //for (int i2=0;i2<=num2.Length-1;i2++)
            //{   
            //        for(int j = i2 + 1; j <= num2.Length; j++)
            //    {
            //        if (num2[i2] < num2[j]) { 
            //            temp = num2[i2];
            //            num2[i2] = num2[j];
            //            num2[j] = temp;
            //        }
            //    }
            //}
            //for(int i3 = 0; i3 < num2.Length; i3++)
            //{
            //    Console.Write(num2[i3] + "\t");
            //}

            //打印九九乘法表：
            Console.WriteLine("打印九九乘法表：");
            for (int i = 1; i <= 9; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(j + "*" + i + "=" + i * j + "\t");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }









}
    

