﻿using System;

namespace Demo5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个整数n：");
            int n = int.Parse(Console.ReadLine());
            int num2 = 0;
            for (int i = 1; i <= n; i++)               
            {
                int num1 = 1;
                for(int j = 1; j <= i; j++)
                {
                    num1 *= j;
                }
                num2 += num1;
            }
            Console.WriteLine(n+"到1所有数的阶乘和="+num2);
            Console.ReadKey();
        }
    }
}
