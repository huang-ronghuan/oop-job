﻿using System;

namespace Demo4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个整数：");
            int n = int.Parse(Console.ReadLine());
            int num = 1;
            for (int i = 1; i <= n; i++)
            {
                num = num*i;
            }
            Console.WriteLine("该数的阶乘为："+num);
            Console.ReadKey();
        }
    }
}
