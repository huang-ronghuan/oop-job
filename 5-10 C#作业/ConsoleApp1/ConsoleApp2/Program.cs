﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo6
{
    class Program
    {
        static void Main(string[] args)
        {
            for (int i = 1; i <= 10; i++)
            {
                for (int k = 9; k >= i; k--)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i*2-1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            for (int i = 9; i >=1 ; i--)
            {
                for (int k = i-1; k <=8 ; k++)
                {
                    Console.Write(" ");
                }
                for (int j = 1; j <= i * 2 - 1; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
