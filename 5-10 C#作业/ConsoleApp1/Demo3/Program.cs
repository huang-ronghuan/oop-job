﻿using System;

namespace Demo3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入第一个数：");
            double one = double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第二个数：");
            double two = double.Parse(Console.ReadLine());
            Console.WriteLine("请输入第三个数：");
            double three = double.Parse(Console.ReadLine());
            double max;

            if (one > two && one > three)
            {
                Console.WriteLine("最大数为：" + one);
            }
            else if (two > one && two > three)
            {
                Console.WriteLine("最大数为：" + two);
            }
            else if (three > one && three > two)
            {
                Console.WriteLine("最大数为：" + three);
            }
            Console.ReadKey();
        }
    }
}
