﻿using System;

namespace Demo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的半径");
            double r = int.Parse(Console.ReadLine());
            const float π = 3.14f;
            double S = π * r * r;
            Console.WriteLine("圆的面积为："+S);
            Console.ReadKey();
        }
    }
}
