﻿using System;

namespace Damo2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个四位整数：");
            int num = int.Parse(Console.ReadLine());
            int a = num / 1000;
            int b = num / 100 % 10;
            int c = num / 10 % 10;
            int d = num % 10;
            Console.WriteLine("用户输入的千位为：" + a);
            Console.WriteLine("用户输入的百位为：" + b);
            Console.WriteLine("用户输入的十位为：" + c);
            Console.WriteLine("用户输入的个位为：" + d);
            Console.ReadKey();
        }
    }
}
