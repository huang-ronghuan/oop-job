﻿using System;

namespace Damo4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个数");
            int n = 0;
            n = int.Parse(Console.ReadLine());
            long sum = 0;
            long fac = 1;
            for (int i = 1; i < n; i++)
            {
                fac *= i;
                sum += fac;
            }
            Console.WriteLine("1~"+n+"的阶乘之和为：{1}", n, sum);
        }
    }
}
