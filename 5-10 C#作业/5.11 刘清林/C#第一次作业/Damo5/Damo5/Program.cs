﻿using System;

namespace Damo5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个数：");
            int input = int.Parse(Console.ReadLine());
            int n = 0;
            int m = 1;
            for (int i = 1; i <= input; ++i)
            {
                m *= i;
                n+= m;
            }

            Console.WriteLine("结果：" + n);

            Console.ReadLine();
        }
    }
}
