﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的半径");
            const double Π = 3.1412;
            int r = int.Parse(Console.ReadLine());  //存放半径
            Console.WriteLine("圆的周长是：" + 2 * Π * r);
            Console.WriteLine("圆的面积是：" + Π * r * r);
            Console.WriteLine("\n");

            //2.请用户输入一个四位整数，将用户输入的四位数的
            //千位、百位、十位和个位分别显示出来
            Console.WriteLine("请用户输入一个四位整数");
            int a = int.Parse(Console.ReadLine());
            int b = a % 10;
            int c = a % 100 / 10;
            int d = a % 1000 / 100;
            int e = a / 1000;
            Console.WriteLine("个位是：" + b + "十位是：" + c + "百位是：" + d + "千位是：" + e);
            Console.WriteLine("\n");

            //3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("输入第一位数");
            int num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("输入第二位数");
            int num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("输入第三位数");
            int num3 = int.Parse(Console.ReadLine());
            if (num1 > num2 && num1 > num3)
            {
                Console.WriteLine("最大数为:" + num1);
            }

            else if (num2 > num1 && num2 > num3)
            {
                Console.WriteLine("最大数为:" + num2);
            }

            else if (num3 > num1 && num3 > num2)
            {
                Console.WriteLine("最大数为:" + num3);
            }
            Console.WriteLine("\n");


            //4.	接受用户输入一个数n，求这个数的阶乘；5! = 5*4*3*2*1;
            Console.WriteLine("请输入要求的阶乘");
            int n = int.Parse(Console.ReadLine());
            int s = n;
            for (int i = n - 1; i > 0; i--)
            {
                s = s * i;
            }
            Console.Write(n + "! = " + n);
            for (int i = n - 1; i > 0; i--)
            {
                Console.Write("*" + i);
            }
            Console.WriteLine("\n");




            //5.	接受用户输入的一个数n，求n到1所有数的阶乘和；n!+(n-1!)+(n-2)!+……+1!
            Console.WriteLine("请用户输入一个数");
            int m = int.Parse(Console.ReadLine());
            int ss = 0;
            for (int i = 0; i < m; i++)
            {
                ss = s + (m - i);
            }
            Console.Write(ss + "=" + m);
            for (int i = 1; i < m; i++)
            {
                Console.Write("+(" + m + "-" + i + ")");
            }
            Console.WriteLine("\n");


           //7.	用循环打印九九乘法表
            for (int i = 1; i <= 9; i++)
            {
                for (int j = 1; j <= i; j++)
                {
                    Console.Write(i + "*" + j + " = " + i * j + "   ");
                }
                Console.WriteLine();


            }
        }
    }
}
