﻿using System;

namespace Dome
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入圆的半径");
            int r = int.Parse(Console.ReadLine());
           const double Π = 3.14;
            Console.WriteLine("圆的面积" + Π * r * r);
        }
    }
}
