﻿using System;

namespace Dome1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个四位整数");
            int num = int.Parse(Console.ReadLine());
            Console.WriteLine("千位" + num / 1000);
            Console.WriteLine("百位" + num / 100 % 10);
            Console.WriteLine("十位" + num / 10 % 10);
            Console.WriteLine("个位" + num % 10);

        }
    }
}
