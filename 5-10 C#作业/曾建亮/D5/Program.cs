﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D5
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("输入一个数值");
            int num = Convert.ToInt32(Console.ReadLine());
            Console.Write("{0}!", num);
            for (int i = 1; i <= num; i++)
            {
                if (i % 2 == 1)
                {
                    Console.Write("+({0}-{1}!)", num, i);
                }
                else
                {
                    Console.Write("+({0}-{1})!", num, i);
                }

            }
            Console.Write("+1");
            Console.ReadKey();
        }
    }
}
