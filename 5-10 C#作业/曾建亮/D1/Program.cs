﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D1
{
    class Program
    {
        static void Main(string[] args)
        {
            const double π = 3.14;
            Console.WriteLine("请输入半径的值");
            int r = int.Parse(Console.ReadLine());
            double s = π * r * r;
            Console.WriteLine("圆的面积"+s);
            Console.ReadKey();
        }
    }
}
