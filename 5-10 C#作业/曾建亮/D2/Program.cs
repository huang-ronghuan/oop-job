﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace D2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个四位整数");
            int i = int.Parse(Console.ReadLine());
            int a, s, d, f;
            a = i / 1000;
            s = i / 100 % 10;
            d = i / 10  % 10;
            f = i % 10;
            Console.WriteLine("用户输入的千位为"+a+"，百位为"+s+"，十位为"+d+"，个位为"+f);
            Console.ReadKey();
        }
    }
}
