﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //1.	求圆的面积
            //要求用户输入半径的值，打印出以此值为半径的圆的面积
            const double Π = 3.14;      //加const代表常量
            Console.WriteLine("请输入圆半径的值");
            int r = int.Parse(Console.ReadLine());
            double r2 = Π * r * r;
            Console.WriteLine("圆的面积：" + r2);
            Console.WriteLine("\n");

            //2.请用户输入一个四位整数，将用户输入的四位数的
            //千位、百位、十位和个位分别显示出来
            Console.WriteLine("请用户输入一个四位整数");
            int a = int.Parse(Console.ReadLine());
            int aa1 = a % 10;
            int aa2 = a % 100 / 10;
            int aa3 = a % 1000 / 100;
            int aa4 = a / 1000;
            Console.WriteLine("个位是：" + aa1 + "\t" + "十位是：" + aa2 + "\t" + "百位是：" + aa3 + "\t" + "千位是：" + aa4);
            Console.WriteLine("\n");

            //3.	用户输入三个数，找出最大的数，打印输出。
            Console.WriteLine("输入第一位数");
            int a1 = int.Parse(Console.ReadLine());
            Console.WriteLine("输入第二位数");
            int a2 = int.Parse(Console.ReadLine());
            Console.WriteLine("输入第三位数");
            int a3 = int.Parse(Console.ReadLine());
            if (a1 > a2 && a1 > a3)
            {
                Console.WriteLine("最大数是:" + a1);
            }
            else if (a2 > a1 && a2 > a3)
            {
                Console.WriteLine("最大数是:" + a2);
            }
            else if (a3 > a1 && a3 > a2)
            {
                Console.WriteLine("最大数是:" + a3);
            }
            Console.WriteLine("\n");

            //4.	接受用户输入一个数n，求这个数的阶乘；5! = 5*4*3*2*1;
            Console.WriteLine("请输入阶乘");
            int n = int.Parse(Console.ReadLine());
            int s = n;
            for (int i = n - 1; i > 0; i--)
            {
                s = s * i;
            }
            Console.Write(n + "! = " + n);
            for (int i = n - 1; i > 0; i--)
            {
                Console.Write("*" + i);
            }
            Console.WriteLine("\n");

            //5.	接受用户输入的一个数n，求n到1所有数的阶乘和；n!+(n-1!)+(n-2)!+……+1!
            Console.WriteLine("请用户输入一个数");
            int n5 = int.Parse(Console.ReadLine());
            int ss= 0;
            for (int i = 0; i < n5; i++)
            {
                ss = s + (n5 - i);
            }
            Console.Write(ss + "=" + n5);
            for (int i = 1; i < n5; i++)
            {  
                Console.Write("+(" + n5 + "-" + i + ")");
            }
            Console.WriteLine("\n");

            //6.用循环打印菱形
            for (int i = 1; i < 5; i++)
            {
                for (int j = 5; j > i; j--)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i * 2 - 1; k++)
                {
                     Console.Write("*");
                }
                Console.WriteLine(); 
            }
            for (int i = 5; i > 0; i--)
            {
                for (int j = i; j < 5; j++)
                {
                    Console.Write(" ");
                }
                for (int k = 0; k < i*2-1; k++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }
                Console.WriteLine("\n");

                //7.	用循环打印九九乘法表
                for (int i = 1; i <= 9; i++)
                {
                    for (int j = 1; j <= i; j++)
                    {
                        Console.Write(i + "*" + j + " = " + i * j + "   ");
                    }
                    Console.WriteLine();
                }
        }
    }
}
