﻿using System;

namespace 第一次c_sharp_作业
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("请输入一个半径值：");
            int r = int.Parse(Console.ReadLine());

            const double Π = 3.14;
            Console.WriteLine("圆的面积：");
            double s = Π * r * r;

            Console.WriteLine(s);

            Console.ReadKey();
        }
    }
}
